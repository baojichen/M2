#!/usr/bin/env python

#------------------------------------------------------------------------------
#                 PyuEye example - gui application modul
#
# Copyright (c) 2017 by IDS Imaging Development Systems GmbH.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#------------------------------------------------------------------------------

from PyQt5.QtWidgets import QGraphicsScene, QApplication
from PyQt5.QtWidgets import QGraphicsView
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QSlider, QWidget
from PyQt5 import QtCore, QtGui,QtWidgets
import pyqtgraph
from PyQt5.QtCore import Qt
import PyQt5
import copy
import numpy as np

from pyueye import ueye
import logging


logger = logging.getLogger('mainlogger')

logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to logger
if not len(logger.handlers):
    logger.addHandler(ch)

class MainWarning(Exception):

    def __init__(self, value):

        self.value = value
        logger.error(f'warning: {value}')

class MainError(Exception):

    def __init__(self, value):

        self.value = value
        logger.error(f'error: {value}')


def get_qt_format(ueye_color_format):
    return { ueye.IS_CM_SENSOR_RAW8: QtGui.QImage.Format_Mono,
             ueye.IS_CM_MONO8: QtGui.QImage.Format_Mono,
             ueye.IS_CM_MONO12: QtGui.QImage.Format_Mono,
             ueye.IS_CM_RGB8_PACKED: QtGui.QImage.Format_RGB888,
             ueye.IS_CM_BGR8_PACKED: QtGui.QImage.Format_RGB888,
             ueye.IS_CM_RGBA8_PACKED: QtGui.QImage.Format_RGB32,
             ueye.IS_CM_BGRA8_PACKED: QtGui.QImage.Format_RGB32
    } [ueye_color_format]


class PyuEyeQtView(QWidget):

    update_signal = QtCore.pyqtSignal(np.ndarray, name="update_signal")
    stageSlider_signal = QtCore.pyqtSignal()
    showBlockLaserMessageBox_signal = QtCore.pyqtSignal()
    crossHair_signal = QtCore.pyqtSignal()
    takeBackgroundImage_signal = QtCore.pyqtSignal()
    startTakingBackgroundImage_signal = QtCore.pyqtSignal()
    deleteFit_signal = QtCore.pyqtSignal()
    runThrough_signal = QtCore.pyqtSignal()
    stageStart_signal = QtCore.pyqtSignal()
    stageEnd_signal = QtCore.pyqtSignal()
    abortMeasurement_signal = QtCore.pyqtSignal()
    automaticLaser_signal = QtCore.pyqtSignal()


    def __init__(self, parent=None):

        super(self.__class__, self).__init__(parent)
        self.setWindowTitle('Pythonic M2')
        #Setup the UI
        #Pyqtgraph handles the imageview
        self.image = None
        pyqtgraph.setConfigOption('imageAxisOrder','row-major')
        # pyqtgraph.setConfigOption('useWeave','True')
        pyqtgraph.setConfigOption('useOpenGL','True')
        pyqtgraph.setConfigOption('leftButtonPan','False')

        plot_view = pyqtgraph.PlotItem()
        plot_view.enableAutoRange()
        self.image_view = pyqtgraph.ImageView(view=plot_view)
        self.v_layout = QtWidgets.QVBoxLayout(self)
        self.v_layout.addWidget(self.image_view)

        #Setup the text elements, sliders and buttons
        CameraSettingsLayout = QtWidgets.QHBoxLayout()

        self.crossHairCheckBox = QtWidgets.QCheckBox("Crosshair")
        self.crossHairCheckBox.stateChanged.connect(self.crossHair_signal.emit)
        CameraSettingsLayout.addWidget(self.crossHairCheckBox)

        self.automaticLaserFocusCheckBox = QtWidgets.QCheckBox("Automatically follow laser")
        self.automaticLaserFocusCheckBox.stateChanged.connect(self.automaticLaser_signal.emit)
        CameraSettingsLayout.addWidget(self.automaticLaserFocusCheckBox)

        self.takeBackgroundImageCheckBox = QtWidgets.QCheckBox("Take background image")
        self.takeBackgroundImageCheckBox.stateChanged.connect(self.takeBackgroundImage_signal.emit)
        CameraSettingsLayout.addWidget(self.takeBackgroundImageCheckBox)

        self.zoomSlider = QtWidgets.QSlider(Qt.Horizontal)
        self.zoomSlider.setTickInterval(1)
        self.zoomSlider.setTickPosition(QSlider.TicksBelow)
        self.zoomSlider.setMinimum(1)
        self.zoomSlider.setMaximum(10)
        self.zoomSlider.setValue(1)
        CameraSettingsLayout.addWidget(self.zoomSlider)

        self.v_layout.addLayout(CameraSettingsLayout)

        self.exposureTimeDesc = QtWidgets.QLabel()
        self.exposureTimeDesc.setText('Exposure time is: NaN')
        self.v_layout.addWidget(self.exposureTimeDesc)

        NameLayout = QtWidgets.QHBoxLayout()

        self.additionalComments = QtWidgets.QLineEdit()
        self.additionalComments.setText('')
        self.additionalComments.setPlaceholderText('Additional comments')
        NameLayout.addWidget(self.additionalComments)

        self.measurementNameField = QtWidgets.QLineEdit()
        self.measurementNameField.setText('')
        self.measurementNameField.setPlaceholderText('Name of measurement')
        self.measurementNameField.setMaxLength(100)
        NameLayout.addWidget(self.measurementNameField)

        self.v_layout.addLayout(NameLayout)

        StageRunThroughLayout = QtWidgets.QHBoxLayout()
        self.stageRunThroughButton = QtWidgets.QPushButton('            Start measurement            ')
        StageRunThroughLayout.addWidget(self.stageRunThroughButton)
        self.stageRunThroughButton.clicked.connect(self.runThrough_signal.emit)

        self.repeatSequence = QtWidgets.QLineEdit()
        self.repeatSequence.setPlaceholderText('Repeat sequence')
        StageRunThroughLayout.addWidget(self.repeatSequence)
        self.repeatSequence.setValidator(QtGui.QIntValidator())
        self.repeatSequence.setMaxLength(3)

        self.StageStart = QtWidgets.QLineEdit()
        self.StageStart.setText('0')
        self.StageStart.setPlaceholderText('Stage position at Start (mm)')
        StageRunThroughLayout.addWidget(self.StageStart)
        self.StageStart.setValidator(QtGui.QDoubleValidator())
        self.StageStart.setMaxLength(6)

        self.StageEnd = QtWidgets.QLineEdit()
        self.StageEnd.setText('300')
        self.StageEnd.setPlaceholderText('Stage position at End (mm)')
        StageRunThroughLayout.addWidget(self.StageEnd)
        self.StageEnd.setValidator(QtGui.QDoubleValidator())
        self.StageEnd.setMaxLength(6)

        self.StageStepSize = QtWidgets.QLineEdit()
        self.StageStepSize.setText('1')
        self.StageStepSize.setPlaceholderText('Step Size (mm)')
        StageRunThroughLayout.addWidget(self.StageStepSize)
        self.StageStepSize.setValidator(QtGui.QDoubleValidator())
        self.StageStepSize.setMaxLength(6)
        self.v_layout.addLayout(StageRunThroughLayout)

        self.abortMeasurement = QtWidgets.QPushButton('            Abort measurement            ')
        self.abortMeasurement.clicked.connect(self.abortMeasurement_signal.emit)
        StageRunThroughLayout.addWidget(self.abortMeasurement)

        GouyPhaseRunTroughLayout = QtWidgets.QHBoxLayout()

        self.additionalStagePos = QtWidgets.QLineEdit()
        self.additionalStagePos.setText('')
        self.additionalStagePos.setPlaceholderText('Add additional points for measurement')
        GouyPhaseRunTroughLayout.addWidget(self.additionalStagePos)

        self.RayleighLength = QtWidgets.QLineEdit()
        self.RayleighLength.setText('')
        self.RayleighLength.setPlaceholderText('Rayleighlength (mm)')
        GouyPhaseRunTroughLayout.addWidget(self.RayleighLength)
        self.RayleighLength.setValidator(QtGui.QDoubleValidator())
        self.RayleighLength.setMaxLength(4)

        self.RayleighLengthSteps = QtWidgets.QLineEdit()
        self.RayleighLengthSteps.setText('')
        self.RayleighLengthSteps.setPlaceholderText('Rayleighlengths to measure around focus')
        GouyPhaseRunTroughLayout.addWidget(self.RayleighLengthSteps)
        self.RayleighLengthSteps.setValidator(QtGui.QDoubleValidator())
        self.RayleighLengthSteps.setMaxLength(2)

        self.GouyPhaseStepsize = QtWidgets.QLineEdit()
        self.GouyPhaseStepsize.setText('')
        self.GouyPhaseStepsize.setPlaceholderText('Gouy phase step size')

        GouyPhaseRunTroughLayout.addWidget(self.GouyPhaseStepsize)

        self.GouyPhaseStepsize.setValidator(QtGui.QDoubleValidator())
        self.GouyPhaseStepsize.setMaxLength(5)

        self.focusPoint = QtWidgets.QLineEdit()
        self.focusPoint.setText('')
        self.focusPoint.setPlaceholderText('Focuspoint on stage')
        GouyPhaseRunTroughLayout.addWidget(self.focusPoint)
        self.focusPoint.setValidator(QtGui.QDoubleValidator())
        self.focusPoint.setMaxLength(5)
        self.v_layout.addLayout(GouyPhaseRunTroughLayout)

        self.stagePositionDesc = QtWidgets.QLabel()
        self.stagePositionDesc.setText('Stage position is: NaN')
        self.v_layout.addWidget(self.stagePositionDesc)

        StagePosLayout = QtWidgets.QHBoxLayout()

        self.stageBeginningButton = QtWidgets.QPushButton('0 mm')
        self.stageEndButton = QtWidgets.QPushButton('300 mm')
        self.stagePositionSlider = QtWidgets.QSlider(Qt.Horizontal)
        self.stagePositionSlider.setMinimum(0)
        self.stagePositionSlider.setMaximum(300)
        self.stagePositionSlider.setValue(0)
        self.stagePositionSlider.setTickPosition(QSlider.TicksBothSides)
        self.stagePositionSlider.setTickInterval(0.5)
        self.stagePositionSlider.setTracking(True)
        self.stagePositionSlider.valueChanged.connect(self.stageSlider_signal.emit)
        self.stageBeginningButton.clicked.connect(self.stageStart_signal.emit)
        self.stageEndButton.clicked.connect(self.stageEnd_signal.emit)

        StagePosLayout.addWidget(self.stageBeginningButton)
        StagePosLayout.addWidget(self.stagePositionSlider)
        StagePosLayout.addWidget(self.stageEndButton)

        self.v_layout.addLayout(StagePosLayout)

        self.update_signal.connect(self.update_image)

        self.processors = []
        self.resize(1920, 1080)

    def showBlockLaserMessageBox(self):

        self.msg = QtWidgets.QMessageBox(parent=self)
        acceptButton = self.msg.addButton('Take background images',QtWidgets.QMessageBox.AcceptRole)
        self.msg.setIcon(QtWidgets.QMessageBox.Information)
        self.msg.setText("Block laser for background image")
        self.msg.setInformativeText("Block laser for background image")
        self.msg.setWindowTitle("Block laser for background image")
        self.msg.exec()

        if self.msg.clickedButton() == acceptButton:
            self.startTakingBackgroundImage_signal.emit()

    def update_image(self, image):
        #cast image as float to prevent crashes from pyqtgraph
        imh = image.astype(float)
        try:
            self.image_view.setImage(imh)
        except Exception as exp:
            logger.info('exp')

    def user_callback(self, image_data):
        return image_data.as_cv_image()

    def handle(self, image_data):

        image_data_copy =  copy.deepcopy(image_data)
        self.image = self.user_callback(image_data_copy)
        self.update_signal.emit(self.image)

    def shutdown(self):

        self.close()

    def add_processor(self, callback):

        self.processors.append(callback)


class PyuEyeQtApp:

    def __init__(self, args=[]):

        self.qt_app = QApplication(args)

    def exec_(self):

        self.qt_app.exec_()

    def exit_connect(self, method):

        self.qt_app.aboutToQuit.connect(method)
