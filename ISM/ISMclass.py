# -*- coding: utf-8 -*-
"""
Created on Mon Sep 19 17:42:16 2016

@author: Lamb
"""

import numpy as np
import ctypes
import _ctypes
import time
import os
import integrated_stepper_motors as ism_defs

import logging

logger = logging.getLogger('mainlogger')

logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to logger
if not len(logger.handlers):
    logger.addHandler(ch)

class ISMwarning(Exception):
    def __init__(self, value):
        self.value = value
        logger.error(value)

class ISMerror(Exception):
    def __init__(self, value):
        self.value = value
        logger.error(value)

# Define Stepper device info structure

class ISM:

    # Constants
    TIMEOUT = 10 # Timeout for device commands, in s
    #the internal device unit conversion factor, can be found on thorlabs website
    STEPS_PER_MM_CHECK_DEFAULT = 409600
    POSITION_ACCURACY_DEFAULT = 1e-3
    STEP_TOL_DEFAULT = 0.001
    FINAL_WAIT_DEFAULT = 0.1
    POLLING_INTERVAL = 500 # DLL device polling interval, in ms
    MM_INDEVUNIT = 409600
    MMperS_INDEVUNIS = 21990233
    MMperS2_INDEVUNIT = 4506

    # Set default values
    DEFAULT_MOTOR_PARAMETERS = {
            'Homing_Velocity': 5 # in mm/s
            ,'Jogging_Velocity': 50 #in mm/s
            ,'Jogging_Distance': 5 #in mm
            ,'Jogging_Acceleration': 50 #in mm/s^2
            ,'Velocity': 50 #in mm/s
            ,'Acceleration': 50 #in mm/s^2
            ,'Backlash': 0.05 # in mm
            ,'Potentiometer_Velocity':50 #in mm/s
            ,'Potentiometer_Acceleration':50 #in mm/s
            }

    #Inits
    def __init__(self, serialNo, parent=None, copy=True):
        self.dll_filename = 'Thorlabs.MotionControl.IntegratedStepperMotors.dll'
        dll_path = 'ISM\\'
        # When DLL is not located in current working directory, add DLL location temporarily to system search path
        if len(dll_path):
            os.environ['PATH'] = os.getcwd() + '\\' + dll_path + os.pathsep + os.environ['PATH']
        self.loadDLL()

        self.serialNo = serialNo
        serialNoEncoded = str(serialNo).encode('utf-8')
        self.serialNoEncoded = serialNoEncoded
        #Define the possible service settings

        self.deviceOpen = False
        self.deviceStatus = [np.nan]
        self.deviceInfo = [np.nan]
        self.hardwareInfo = [np.nan]
        self.stepsPermm = [np.nan]
        self.positionSigDigits = [np.nan]
        self.axisLimitsHardware = np.zeros((1,2))
        self.axisLimitsUser = np.zeros((1,2))
        self.axisLimitsUser[:] = np.nan
        self.motorStopped = False
        self.stageInitialized = False

        #init the Stage
        self.initStage(serialNo)

    def initStage(self, serialNo = 0):
        #set custom motor parameters
        motor_parameters = {
                'Homing_Velocity': 5 # in mm/s
                ,'Velocity': 50 #in mm/s
                ,'Acceleration': 50 #in mm/s^2
                }

        self.getDeviceList()

        if serialNo == 0:
             msg = 'Device you want to initialize has no serial number'
             raise ISMerror(msg)
        else:
            self.serialNo = serialNo
        #open the device connection and home the stage
        try:
            self.openDevice(serialNo,axisLimitsUser_mm=[np.nan,np.nan],motor_parameters=motor_parameters)
            self.homeMotor()
            self.stageInitialized = True
        except Exception as exp:
            logger.info(' Stage init failed with exception: '+str(exp))
            self.deviceOpen = False
            self.stageInitialized = False
            self.motor_parameters = 0
            logger.info('Initalization of device failed, trying again in 2 seconds')
            self.loadDLL()
            time.sleep(2)
            self.initStage(self.serialNo)

    def getDeviceList(self):

        #List of devices
        self.Stepper.TLI_BuildDeviceList()
        self.DeviceList = self.Stepper.TLI_GetDeviceListSize()

        #Built array for serial numbers size 100 and get device information
        serialNos = ctypes.create_string_buffer(100)
        self.Stepper.TLI_GetDeviceListByTypeExt(serialNos, ism_defs.c_dword(100), ism_defs.c_int(45))
        #Convert from char array to python string
        serialNos = serialNos.value.decode('utf-8').split(',')
        self.deviceSerialNos = [int(elem) for elem in serialNos if elem]
        logger.info('ISM: Number of devices in system: {:d} (serial number(s): {})'.format(len(self.deviceSerialNos),', '.join([str(elem) for elem in self.deviceSerialNos])))

        # Initialize internal variables
        self.deviceSerialNosEnc = np.array([str(elem).encode('utf-8') for elem in self.deviceSerialNos])

    def checkDeviceOpen(self):
        # Check if device is open
        if not self.deviceOpen:
            msg = 'Device with serial no. {:d} is not open.'.format(self.serialNo)
            raise ISMerror(msg)

    def openDevice(self, serialNo, calibration_file='', axisLimitsUser_mm=[np.nan, np.nan], motor_parameters={}, stepsPermmCheck=STEPS_PER_MM_CHECK_DEFAULT, positionAccuracy=POSITION_ACCURACY_DEFAULT):

        self.motor_parameters = {**self.DEFAULT_MOTOR_PARAMETERS, **motor_parameters}

        if not self.deviceOpen:

            logger.info('ISM: Opening device with serial no. {:d}'.format(self.serialNo))
            # Open device
            status = self.Stepper.ISC_Open(self.serialNoEncoded)
            # Check if device could be opened, otherwise raise error
            if status == 0:
                self.deviceOpen = True
                #device is open, but not confirgured
                # Start internal loop, requesting position and status continuously
                time.sleep(1)
                self.Stepper.ISC_StartPolling(self.serialNoEncoded, ctypes.c_int(self.POLLING_INTERVAL))
                time.sleep(1)
                #calibrate device with the provided file from thorlabs
                if not self.Stepper.ISC_IsCalibrationActive(self.serialNoEncoded) and (calibration_file!=''):
                   self.Stepper.ISC_SetCalibrationFile(self.serialNoEncoded,calibration_file.encode('utf-8'),ctypes.c_bool(True))
                #instead it assigns wrong random values to all parameters of the device, which we have to set manually to ensure a correct behavior of the stage
                #here we manually set all the settings of the device, see the documentation (Thorlabs.MotionControl.C_API) for more info
                my_MOT_HomingParameters = ism_defs.MOT_HomingParameters()
                my_MOT_HomingParameters.limitSwitch = ism_defs.MOT_ReverseLimitSwitch
                my_MOT_HomingParameters.direction = ism_defs.MOT_Reverse
                my_MOT_HomingParameters.velocity = ctypes.c_uint(self.convertToDevUnits(self.motor_parameters['Homing_Velocity'],1))
                my_MOT_HomingParameters.offsetDistance = ctypes.c_uint(self.convertToDevUnits(0.1,0))
                self.Stepper.ISC_SetHomingParamsBlock(self.serialNoEncoded, ctypes.byref(my_MOT_HomingParameters))
                self.Stepper.ISC_SetVelParams(self.serialNoEncoded,ctypes.c_int(self.convertToDevUnits(self.motor_parameters['Acceleration'], 2)),ctypes.c_int(self.convertToDevUnits(self.motor_parameters['Velocity'], 1)))
                self.Stepper.ISC_SetBacklash(self.serialNoEncoded,ctypes.c_int(self.convertToDevUnits(self.motor_parameters['Backlash'], 0)))
                self.Stepper.ISC_SetDirection(self.serialNoEncoded,ctypes.c_bool(True))
                self.Stepper.ISC_SetButtonParams(self.serialNoEncoded,ism_defs.MOT_JogMode,ctypes.c_int(0),ctypes.c_int(0))
                self.Stepper.ISC_SetMotorParamsExt(self.serialNoEncoded, ctypes.c_double(200), ctypes.c_double(1), ctypes.c_double(1))
                self.Stepper.ISC_SetLimitsSoftwareApproachPolicy(self.serialNoEncoded,ism_defs.AllowPartialMoves)
                self.Stepper.ISC_SetJogMode(self.serialNoEncoded,ism_defs.MOT_SingleStep,ism_defs.MOT_Profiled)
                self.Stepper.ISC_SetJogStepSize(self.serialNoEncoded,ctypes.c_uint(self.convertToDevUnits(self.motor_parameters['Jogging_Distance'], 0)))
                self.Stepper.ISC_SetJogVelParams(self.serialNoEncoded,ctypes.c_int(self.convertToDevUnits(self.motor_parameters['Jogging_Acceleration'], 2)),ctypes.c_int(self.convertToDevUnits(self.motor_parameters['Jogging_Velocity'], 1)))
                self.Stepper.ISC_SetStageAxisLimits(self.serialNoEncoded,ctypes.c_int(self.convertToDevUnits(0, 0)),ctypes.c_int(self.convertToDevUnits(300, 0)))
                self.Stepper.ISC_SetMotorTravelLimits(self.serialNoEncoded,ctypes.c_double(0),ctypes.c_double(300))
                for x in range(0,127):
                    self.Stepper.ISC_SetPotentiometerParams(self.serialNoEncoded,ctypes.c_short(0),ism_defs.c_word(x),ism_defs.c_dword(self.convertToDevUnits(x/127* self.motor_parameters['Potentiometer_Velocity'], 1)))

                # Get device and hardware info
                self.deviceInfo = ism_defs.TLI_DeviceInfo()
                status = self.Stepper.TLI_GetDeviceInfo(self.serialNoEncoded, ctypes.byref(self.deviceInfo))
                self.hardwareInfo = ism_defs.TLI_HardwareInformation()
                status = self.Stepper.ISC_GetHardwareInfoBlock(self.serialNoEncoded, ctypes.byref(self.hardwareInfo))

                # Get conversion factor from device units to mm
                if status!=0:
                    msg = 'Could not get motor parameters for device with serial no. %i, error code %i.'%(int(serialNo),status)
                    raise ISMerror(msg)

                self.stepsPermm = self.convertToDevUnits(1, 0)

                if self.stepsPermm!=self.STEPS_PER_MM_CHECK_DEFAULT:
                    msg = 'Steps per mm for device with serial no. %i is %i, not %i as expected.'%(int(serialNo),self.stepsPermm,self.STEPS_PER_MM_CHECK_DEFAULT)
                    raise ISMerror(msg)

                self.positionSigDigits = int(-np.log10(positionAccuracy))
                # Get hardware axis limits
                axisMinPos = self.Stepper.ISC_GetStageAxisMinPos(self.serialNoEncoded)
                axisMaxPos = self.Stepper.ISC_GetStageAxisMaxPos(self.serialNoEncoded)
                self.axisLimitsHardware = [axisMinPos, axisMaxPos]
                # Set user axis limits (implemented in software)
                self.setAxisLimitsUser(axisLimitsUser_mm)

            else:
                msg = 'Could not open device with serial no. %i, error code %i.'%(self.serialNo,status)
                raise ISMerror(msg)
        else:
            msg = 'ISM: Device (serial no. %i) is already open.'%(self.seriaNo)
            logger.debug(msg)

    def closeDevice(self):

        logger.info('ISM: Closing device with serial no. %i'%(self.serialNo))
        self.Stepper.ISC_StopPolling(self.serialNoEncoded)
        self.Stepper.ISC_Close(self.serialNoEncoded)
        self.deviceOpen = False

    def setAxisLimitsUser(self, axisLimitsUser_mm):

        self.checkDeviceOpen()
        self.axisLimitsUser = [self.convertToDevUnits(elem,0) if ~np.isnan(elem) else np.nan for elem in axisLimitsUser_mm]

    def getDeviceStatus(self):

        self.checkDeviceOpen()
        self.Stepper.ISC_RequestStatusBits(self.serialNoEncoded)
        statusBits = self.Stepper.ISC_GetStatusBits(self.serialNoEncoded)
        statusBits = [int(i) for i in bin(statusBits).split('b')[1].zfill(32)]
        statusBits.reverse()
        statusDict = {'CWHardwareLimitSwitch' : bool(statusBits[0]), \
              'CCWHardwareLimitSwitch' : bool(statusBits[1]), \
              'CCWSoftwareLimitSwitch' : bool(statusBits[3]), \
              'MotorShaftMovingClockwise' : bool(statusBits[4]),\
              'MotorShaftMovingCounterclockwise' : bool(statusBits[5]),\
              "ShaftJoggingClockwise" : bool(statusBits[6]),\
              "ShaftJoggingCounterclockwise" : bool(statusBits[7]),\
              'MotorConnected' : bool(statusBits[8]),\
              "MotorHoming" : bool(statusBits[9]), \
              "MotorHomed" : bool(statusBits[10]),\
              "DigitalInput1" : statusBits[20], \
              "DigitalInput2" : statusBits[21],\
              "DigitalInput3" : statusBits[22], \
              "DigitalInput4" : statusBits[23],\
              "DigitalInput5": statusBits[24],\
              "DigitalInput6": statusBits[25],\
              "Active": bool(statusBits[29]),\
              "ChannelEnabled" : bool(statusBits[31])}
        self.deviceStatus = statusDict
        return statusDict

    # Convert position from device units to real units
    def convertToRealUnits(self, position, vartype):

        self.checkDeviceOpen()
        #conversion values from device units into real units
        if vartype == 0:
            #Distance
            steps = self.MM_INDEVUNIT
            conv_distance = float(position/steps)
            return conv_distance
        if vartype == 1:
            #Velocity
            steps = self.MMperS_INDEVUNIS
            conv_velocity = float(position/steps)
            return conv_velocity
        if vartype == 2:
            #Acceleration
            steps = self.MMperS2_INDEVUNIT
            conv_acceleration = float(position/steps)
            return conv_acceleration
        else:
            raise ISMerror("Wrong vartype provided")

    #converts the real units into the deviceunits using conversion factors provided by Thorlabs
    def convertToDevUnits(self, position, vartype):

        self.checkDeviceOpen()
        #conversion values from real units into device units
        if vartype == 0:
            #Distance
            steps = self.MM_INDEVUNIT
            conv_distance = int(steps*position)
            return conv_distance
        if vartype == 1:
            #Velocity
            steps = self.MMperS_INDEVUNIS
            conv_velocity = int(steps*position)
            return conv_velocity
        if vartype == 2:
            #Acceleration
            steps = self.MMperS2_INDEVUNIT
            conv_acceleration = int(steps*position)
            return conv_acceleration
        else:
            raise ISMerror("Wrong vartype provided")

    # Get current motor position (in mm)
    def getPosition(self):

        self.checkDeviceOpen()
        position = self.getPositionDevUnits()
        return self.convertToRealUnits(position,0)

    # Get current motor position (in device units)
    def getPositionDevUnits(self):

        self.checkDeviceOpen()
        self.Stepper.ISC_RequestPosition(self.serialNoEncoded)
        position = self.Stepper.ISC_GetPosition(self.serialNoEncoded)
        return position

    # Move motor to new position (in mm)
    def moveToPosition(self, position_mm):

        position = self.convertToDevUnits(position_mm,0)
        self.Stepper.ISC_ClearMessageQueue(self.serialNoEncoded)
        status, position_mm = self.moveToPositionDevUnits(position)
        return status, position

    # Move motor to new position (in device units)
    def moveToPositionDevUnits(self, position):

        self.checkDeviceOpen()
        position_mm = self.convertToRealUnits(position,0)

        # Check whether requested position is in hardware and software limits
        minLim = int(np.nanmax([self.axisLimitsHardware[0],self.axisLimitsUser[0]]))
        maxLim = int(np.nanmin([self.axisLimitsHardware[1],self.axisLimitsUser[1]]))

        if not minLim <= position <= maxLim:
            msg = 'Requested position position %i dev. u./%.6f mm outside axis limits ([%i, %i] dev. u.) for device serial no. %i'%(position,position_mm,minLim,maxLim,self.serialNo)
            raise ISMerror(msg)

        # Moves motor to given position

        self.motorStopped = False
        logger.info('ISM: Moving device (serial no. {:d}) to position {:d} dev. u./{:.6f} mm'.format(self.serialNo,position,self.convertToRealUnits(position,0)))
        status = self.Stepper.ISC_MoveToPosition(self.serialNoEncoded,position)
        return status, position_mm

    def waitTillPositionReached(self, callback,position_mm, stepTol_mm=STEP_TOL_DEFAULT, finalWait=FINAL_WAIT_DEFAULT):

        self.checkDeviceOpen()
        position = self.convertToDevUnits(position_mm,0)
        stepTolerance = self.convertToDevUnits(stepTol_mm,0)
        return self.waitTillPositionReachedDevUnits(callback,position, stepTolerance, finalWait)

    def waitTillPositionReachedDevUnits(self,callback,position, stepTol, finalWait=FINAL_WAIT_DEFAULT):

        self.checkDeviceOpen()
        startTime = time.time()
        messageType = ism_defs.c_word()
        messageId = ism_defs.c_word()
        messageData = ism_defs.c_dword()
        self.Stepper.ISC_WaitForMessage(self.serialNoEncoded, ctypes.byref(messageType), ctypes.byref(messageId), ctypes.byref(messageData))
        while messageType.value != 2 or messageId.value != 1:
            self.Stepper.ISC_WaitForMessage(self.serialNoEncoded, ctypes.byref(messageType), ctypes.byref(messageId), ctypes.byref(messageData))
            # Check if device was stopped manually while this loop is executing
            if self.motorStopped:
                # Release device stop
                self.motorStopped = False
                msg = 'Device stopped manually while waiting for device (serial no. %i) to reach position %i dev. u./%.6f mm'%(self.serialNo,position,self.convertToRealUnits(position,0))
                raise ISMerror(msg)
            if time.time()-startTime > self.TIMEOUT:
                # Operation timed out
                # Stop motor
                self.stopMotor()
                # Release device stop
                self.motorStopped = False
                msg = 'Timeout while waiting for device (serial no. %i) to reach position %i dev. u./%.6f mm, device stopped'%(self.serialNo,position,self.convertToRealUnits(position,0))
                raise ISMerror(msg)
        # Check whether device has reached target position within tolerance (stepTol)
        if position+stepTol >= self.getPositionDevUnits() >= position-stepTol:
            if callback:
                callback.stageMessage(messageType.value,messageId.value,messageData.value)
            # Device has reached target position from different position in previous cycle
            callback.stageMessage(messageType.value,messageId.value,messageData.value)
            logger.info('ISM: Device (serial no. {:d}) has reached position {:d} dev. u./{:.6f} mm after {:.2f} s'.format(self.serialNo,self.getPositionDevUnits(),self.convertToRealUnits(self.getPositionDevUnits(),0),time.time()-startTime))
        else:
            msg = 'Motor stopped unexpectedly while waiting for motor (device serial no. %i) to reach position %i dev. u./%.6f mm'%(self.serialNo,position,self.convertToRealUnits(position,0))
            raise ISMerror(msg)

    def checkIfPositionReached(self,position_mm, stepTol_mm=STEP_TOL_DEFAULT):

        self.checkDeviceOpen()
        position = self.convertToDevUnits(position_mm,0)
        stepTolerance = self.convertToDevUnits(stepTol_mm,0)
        return self.checkIfPositionReachedDevUnits(position, stepTolerance)

    def checkIfPositionReachedDevUnits(self,position, stepTol):

        self.checkDeviceOpen()
        messageType = ism_defs.c_word()
        messageId = ism_defs.c_word()
        messageData = ism_defs.c_dword()
        while True:
            #wait from messages from the stage if it arrived
            self.Stepper.ISC_GetNextMessage(self.serialNoEncoded, ctypes.byref(messageType), ctypes.byref(messageId), ctypes.byref(messageData))
            if messageType.value == 2 and messageId.value == 1:
                 # Check whether device has reached target position within tolerance (stepTol)
                print('position target ',position,'real pos',self.getPositionDevUnits())
                if position+stepTol >= self.getPositionDevUnits() >= position-stepTol:
                        # Device has reached target position from different position in previous cycle
                    logger.info('ISM: Device (serial no. {:d}) has reached position {:d} dev. u./{:.6f} mm'.format(self.serialNo,self.getPositionDevUnits(),self.convertToRealUnits(self.getPositionDevUnits(),0)))
                    return True
                else:
                    msg = 'Motor stopped unexpectedly while waiting for motor (device serial no. %i) to reach position %i dev. u./%.6f mm, being at /%.6f mm'%(self.serialNo,position,self.convertToRealUnits(position,0),self.convertToRealUnits(self.getPositionDevUnits(),0))
                    raise ISMerror(msg)
                if self.motorStopped:
                    # Release device stop
                    self.motorStopped = False
                    msg = 'Device stopped manually while waiting for device (serial no. %i) to reach position %i dev. u./%.6f mm'%(self.serialNo,position,self.convertToRealUnits(position,0))
                    raise ISMerror(msg)
            if self.Stepper.ISC_MessageQueueSize(self.serialNoEncoded) <= 0:
               # print('message queue empty')
               if position+stepTol >= self.getPositionDevUnits() >= position-stepTol:
                        # Device has reached target position from different position in previous cycle
                    logger.info('ISM: Device (serial no. {:d}) has reached position {:d} dev. u./{:.6f} mm'.format(self.serialNo,self.getPositionDevUnits(),self.convertToRealUnits(self.getPositionDevUnits(),0)))
                    return True
               break
        return False

    def homeMotor(self):
        self.checkDeviceOpen()
        self.homingTimeStart = time.time()
        logger.info('ISM: Homing device (serial no. {:d})'.format(self.serialNo))
        self.Stepper.ISC_Home(self.serialNoEncoded)

    def getLatestMessage(self):

        messageType = ism_defs.c_word()
        messageId = ism_defs.c_word()
        messageData = ism_defs.c_dword()
        self.Stepper.ISC_GetNextMessage(self.serialNoEncoded, ctypes.byref(messageType), ctypes.byref(messageId), ctypes.byref(messageData))
        return messageType.value,messageId.value,messageData.value

    def waitTillMotorHomed(self):

        time.sleep(0.1)
        startTime = time.time()
        self.checkDeviceOpen()
        messageType = ism_defs.c_word()
        messageId = ism_defs.c_word()
        messageData = ism_defs.c_dword()
        self.Stepper.ISC_GetNextMessage(self.serialNoEncoded, ctypes.byref(messageType), ctypes.byref(messageId), ctypes.byref(messageData))
        while not (messageType.value == 2 and messageId.value == 0):
            self.Stepper.ISC_GetNextMessage(self.serialNoEncoded, ctypes.byref(messageType), ctypes.byref(messageId), ctypes.byref(messageData))
            if time.time()-startTime > 1.25*300/self.motor_parameters['Homing_Velocity']:
                # Operation timed out
                msg = 'Timeout while waiting for device to home (serial no. %i).'%(self.serialNo)
                raise ISMerror(msg)
            if self.motorStopped:
                # Release device stop
                self.motorStopped = False
                msg = 'Device stopped manually while waiting for device (serial no. %i) to home.'%(self.serialNo)
                raise ISMerror(msg)
        logger.info('ISM: Device (serial no. {:d}) homed after {:.3f} s'.format(self.serialNo,time.time()-startTime))

    def stopMotor(self):

        self.checkDeviceOpen()
        # Signal to other functions that motor has been stopped manually
        self.motorStopped = True
        self.Stepper.ISC_StopProfiled(self.serialNoEncoded)

    def loadDLL(self):

        ## Load Thorlabs library for controller

        self.Stepper = ctypes.CDLL(self.dll_filename)
        time.sleep(2)

    def close(self):

        self.closeDevice()
        _ctypes.FreeLibrary(self.Stepper._handle)

    def stop(self):

        self.close()
        self.running = False
