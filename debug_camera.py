# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 07:55:06 2023

@author: Balmer
"""
import sys
sys.path.insert(0, 'pyueye')
sys.path.insert(0, 'ISM')
sys.path.insert(0, 'Analysis')
# When DLL is not located in current working directory, add DLL location temporarily to system search path
from pyueye_camera import Camera
from pyueye_utils import *
from gui import PyuEyeQtApp, PyuEyeQtView
from PyQt5 import QtCore, QtGui, QtWidgets
import math
from ISMclass import ISM
#from ISMclassNET import ISM
import copy

from pyueye import ueye

import cv2
import numpy as np
import time
import datetime
import os
import random
import logging
import pandas as pd

# logger = logging.getLogger('mainlogger')
logger = logging.getLogger()

logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to logger
if not len(logger.handlers):
    logger.addHandler(ch)

view = None

logging.debug("M2.initCamera")
#init the camera and set the area of interest (AOI) to the maximium
cam = Camera()
cam.init()
cam.set_colormode(ueye.IS_CM_MONO12)
cam.set_aoi(0,0,3088,2076)
cam.alloc()
#Sets the color depth of the camera depending on the FPS that we want. Higher FPS can only be displayed if the color depth is reduced (due to limited bandwidth)
cam.set_imaging_mode('quality')
#Sets the biggest exposure time possible
cam.set_max_exposure_time()

# ueye.is_SetPixelClock(cam.handle(), 237)

# cam.get_max_exposure_time()
# cam.get_exposure_time()

# cam.set_imaging_mode('fast')

# logging.debug('first image')
ret = ueye.is_FreezeVideo(cam.handle(), ueye.IS_WAIT)

if ret == ueye.IS_SUCCESS:
    img = ImageData(cam.handle(), cam.buff,True)
else:
    ueye.is_ForceTrigger(cam.handle())
    print("pyueye_utils.FrameThread:", ret)

# cam.set_imaging_mode('quality')
# logging.debug('second image')
# ret2 = ueye.is_FreezeVideo(cam.handle(), ueye.IS_WAIT)
