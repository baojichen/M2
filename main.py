#!/usr/bin/env python

#------------------------------------------------------------------------------
#                 PyuEye example - main modul
#
# Copyright (c) 2017 by IDS Imaging Development Systems GmbH.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#------------------------------------------------------------------------------
import sys
sys.path.insert(0, 'pyueye')
sys.path.insert(0, 'ISM')
sys.path.insert(0, 'Analysis')
# When DLL is not located in current working directory, add DLL location temporarily to system search path
from pyueye_camera import Camera
from pyueye_utils import FrameThread
from gui import PyuEyeQtApp, PyuEyeQtView
from PyQt5 import QtCore, QtGui, QtWidgets
import math
from ISMclass import ISM
#from ISMclassNET import ISM
import copy

from pyueye import ueye

import cv2
import numpy as np
import time
import datetime
import os
import random
import logging
import pandas as pd

# logger = logging.getLogger('mainlogger')
logger = logging.getLogger()

logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to logger
if not len(logger.handlers):
    logger.addHandler(ch)

class MainWarning(Exception):

    def __init__(self, value):

        self.value = value
        logger.error(f'warning code {value}')

class MainError(Exception):

    def __init__(self, value):

        self.value = value
        logger.error(f'error code {value}')


class M2:
    
    def __init__(self, parent=None):
        self.repeatCount:np.uint16=0  
        self.repeatDatas=[]
        self.followLaserBeam = False
        self.crossHair = False
        self.takeBackgroundImage = False
        #sets the stage serialNumber
        self.stageSerialNo = 45876508
        #set up dataframe to store the data
        self.dataFrame = pd.DataFrame(columns=['Timestamp', 'imgName', 'ExposureTime', 'StagePos', 'MeasName', 'FolderName', 'MeasTimestamp','Comment','Status','Type'])
        #sets all the required camera exposure parameters
        self.exposure_parameters = {'current_step_size':0,'previous_step':'under','skip_frames':2,'frames_since_stagemov':0,'consecutive_readjustments':0,'max_readjustments':20,'hysteresis':0.2,'is_adjusted':False}
      
    def process_image(self, image_data):
        #self.cam.set_gain()
        timestamp = datetime.datetime.utcnow()
        stagePos = round(self.Stage.getPosition(),5)
        # reshape the image data as 1dimensional array
        im = image_data.as_2d_numpy_array()

        im_ysize = np.size(im,0)
        im_xsize = np.size(im,1)

        exposure_time = float(round(self.cam.get_exposure_time(),6))
        self.setStagePosSlider(stagePos)

        self.view.exposureTimeDesc.setText('Camera exposure time is: '+str(exposure_time)+' ms')

        if self.dataFrame.query('Status ==\'active\' and Type == \'Background\'').shape[0] == 1:
            #if the exposure time and the stage position is the for background as it was for the laser image then take the background image
            aaa=self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and '+'Type == \'Background\'').head(1) .index.values[0],'ExposureTime']
            if exposure_time ==aaa:
            #if True:
                if  self.Stage.checkIfPositionReached(self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and Type ==\'Background\'').head(1).index.values[0],'StagePos']):
                    #create directory
                    directory = self.checkAndCreateDir("Measurements"+os.path.sep+self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and Type ==\'Background\'').head(1).index.values[0],'FolderName'])
                    imgName = "Bkg-"+str(self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and Type ==\'Background\'').head(1).index.values[0],'StagePos'])+"mm"+".tif"
                    #write image
                    cv2.imwrite(directory+os.path.sep+imgName, im)
                    self.writeComment(directory)
                    activeObj = self.dataFrame.query('Status ==\'active\' and Type ==\'Background\'').head(1).index.values[0]
                    self.dataFrame.loc[activeObj,'imgName'] = imgName
                    self.dataFrame.loc[activeObj,'Timestamp'] = timestamp
                    self.dataFrame.loc[activeObj,'Status'] = 'done'
                    self.checkAndExecuteRemainingPos('Background')

            else:
                self.cam.set_exposure_time(self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and '+'Type == \'Background\'').head(1).index.values[0],'ExposureTime'])

        else:

            if self.exposure_parameters['is_adjusted']:
                self.exposure_parameters['current_step_size'] = self.cam.get_exposure_step()/2
            else:
                if self.exposure_parameters['consecutive_readjustments'] > self.exposure_parameters['max_readjustments']:
                    self.exposure_parameters['current_step_size'] = int(round(self.cam.get_possible_steps_exposure()/2))
                    self.exposure_parameters['consecutive_readjustments'] = 0
                self.view.exposureTimeDesc.setText('Camera exposure time is: '+str(exposure_time)+' ms'+' and is adjusting')

            if self.autoExposureCam(im, exposure_time):

                   self.exposure_parameters['consecutive_readjustments'] = 0
                   self.exposure_parameters['is_adjusted'] = True

                   if self.dataFrame.query('Status ==\'active\' and Type ==\'Laser\'').shape[0] == 1 and self.Stage.checkIfPositionReached(self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and Type ==\'Laser\'').head(1).index.values[0],'StagePos']):
                       self.exposure_parameters['frames_since_stagemov'] += 1

                       if self.exposure_parameters['frames_since_stagemov'] >= self.exposure_parameters['skip_frames']:
                           self.exposure_parameters['frames_since_stagemov'] = 0

                           if self.dataFrame.query('Status ==\'active\' and Type ==\'Laser\'').shape[0] == 1:
                               #create dir, write image to it
                               self.repeatDatas.append(im)
                               imgName = str(stagePos)+"mm"+".tif"
                                    

                               if self.dataFrame.query('Status ==\'active\' and Type ==\'Laser\'').shape[0] == 1:
                                   activeObj = self.dataFrame.query('Status ==\'active\' and Type ==\'Laser\'').head(1).index.values[0]
                                   self.dataFrame.loc[activeObj,'imgName'] = imgName
                                   self.dataFrame.loc[activeObj,'Timestamp'] = timestamp
                                   self.dataFrame.loc[activeObj,'ExposureTime'] = exposure_time
                                   activeObj0 = activeObj+1
                                   self.dataFrame.loc[activeObj0,'ExposureTime']=exposure_time
                                   #print(self.dataFrame.loc[activeObj0,'StagePos'])
                                   #print(self.dataFrame.loc[activeObj0,'Timestamp'])
                                   #print(self.dataFrame.loc[activeObj0,'ExposureTime'])
                                   #if taking a background image is requested, record the exposure time for it as well
                                   #print(self.dataFrame.loc[activeObj,'MeasName'])
                                   #print(str(self.dataFrame.loc[activeObj,'StagePos']))
                                   #aaaa=self.dataFrame.query('MeasName ==\''+self.dataFrame.loc[activeObj,'MeasName']+'\' and StagePos ==\''+str(self.dataFrame.loc[activeObj,'StagePos'])+'\' and Type ==\'Background\'')
                                   #aaac=self.dataFrame.query('MeasName ==\''+self.dataFrame.loc[activeObj,'MeasName']+'\' and StagePos ==\''+str(self.dataFrame.loc[activeObj,'StagePos'])+'\'')
                                   #aaav=self.dataFrame.query('MeasName ==\''+self.dataFrame.loc[activeObj,'MeasName']+'\'')
                                   if self.dataFrame.query('MeasName ==\''+self.dataFrame.loc[activeObj,'MeasName']+'\' and StagePos ==\''+str(self.dataFrame.loc[activeObj,'StagePos'])+'\' and Type ==\'Background\'').shape[0]:
                                       #print(114514)
                                       self.dataFrame.loc[self.dataFrame.query('MeasName ==\''+self.dataFrame.loc[activeObj,'MeasName']+'\' and StagePos ==\''+str(self.dataFrame.loc[activeObj,'StagePos'])+'\' and Type ==\'Background\'').head(1).index.values[0],'ExposureTime'] = exposure_time
                                   if(self.repeatCount<100):
                                    self.repeatCount+=1
                                   else:
                                    imm=self.repeatDatas[0].astype(np.uint32)
                                    for imIndex in range(1,self.repeatCount):
                                        imm+=self.repeatDatas[imIndex]
                                    imm=imm/np.uint16(self.repeatCount)
                                    directory = self.checkAndCreateDir("Measurements"+os.path.sep+self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and Type ==\'Laser\'').head(1).index.values[0],'FolderName'])
                                    imgName = str(stagePos)+"mm"+".tif"
                                    cv2.imwrite(directory+os.path.sep+imgName, imm.astype(np.uint16))
                                    self.writeComment(directory)
                                   
                                    self.dataFrame.loc[activeObj,'Status'] = 'done'
                                    self.checkAndExecuteRemainingPos('Laser')
                                    self.repeatCount=0
                                    self.repeatDatas=[]
                               
                               else:
                                   logger.info("Error: dataframe does not have any active measurement, no data was written")

        #this adds a crosshair to the image for better laser adjustment
        if self.crossHair:
            im = self.addCrosshair(im)
        #this is the zoom of the image. We either zoom into the image with or without tracking the laser beam
        try:
             if self.view.zoomSlider.value() != 1 or self.followLaserBeam:
                 if self.view.automaticLaserFocusCheckBox.isChecked():
                    #create 8bit version for OpenCV
                    im8 = copy.deepcopy(im.astype(np.uint8))
                    (xx, yy), (best_ma, beam_width), best_angle = self.find_ellipses(im8)
                    beam_width = int(round(beam_width*2/self.view.zoomSlider.value()))
                 elif self.view.zoomSlider.value() != 1:
                    beam_width = int(round(im_xsize*0.75/self.view.zoomSlider.value()))
                    xx = im_xsize/2
                    yy = im_ysize/2
                 return self.cutImage(im,xx,yy,beam_width,beam_width)
        except Exception as exp:
               logger.info(exp)
        return im


    def autoExposureCam(self, image, exposure_time):
        #determines the best exposure time for the camera to ensure that the image not over or underexposed
        #it takes the image and looks at the maximal pixel value
        #if the exposure is too high or low it adjusts the exposure time and then return False to indicate that the camera should take another image for a new try
        #if the adjustment takes too long (more than self.exposure_parameters['max_readjustments']))  then it resets itself and tries again
        maxVal = image.max()
        if (maxVal>=4095*(1-self.exposure_parameters['hysteresis']) and self.exposure_parameters['is_adjusted']) or (maxVal>=4095 and not self.exposure_parameters['is_adjusted']):
            if self.exposure_parameters['previous_step'] == 'under':
                self.exposure_parameters['current_step_size'] = int(round(self.exposure_parameters['current_step_size']/2))
                if self.exposure_parameters['current_step_size'] < 1:
                    self.exposure_parameters['current_step_size'] = 1
            # prevent that the exposure time is stuck at the lowest exposure time
            if self.cam.get_exposure_step() <= 2:
                self.view.exposureTimeDesc.setText('Camera exposure time is: '+str(exposure_time)+' ms'+' and is minimal, decrease the beam power')
                logger.info('Exposure time is minimal, decrease the beam power')
            self.cam.change_exposure_step(-self.exposure_parameters['current_step_size'])
            self.exposure_parameters['previous_step'] = 'over'
            self.exposure_parameters['is_adjusted'] = False
            self.exposure_parameters['consecutive_readjustments'] = self.exposure_parameters['consecutive_readjustments']+1
            return False

        elif (maxVal<3072*(1-self.exposure_parameters['hysteresis']) and self.exposure_parameters['is_adjusted']) or (maxVal < 3072 and not self.exposure_parameters['is_adjusted']):
            if self.exposure_parameters['previous_step'] == 'over':
                self.exposure_parameters['current_step_size']= int(round(self.exposure_parameters['current_step_size']/2))
                if self.exposure_parameters['current_step_size'] < 1:
                    self.exposure_parameters['current_step_size'] = 1
            self.cam.change_exposure_step(self.exposure_parameters['current_step_size'])
            self.exposure_parameters['previous_step'] = 'under'
            self.exposure_parameters['is_adjusted'] = False
            self.exposure_parameters['consecutive_readjustments'] = self.exposure_parameters['consecutive_readjustments']+1
            return False
        else:
            return True
        return False

    def checkAndExecuteRemainingPos(self, Type):
        #checks if the next image that should be taken is background or laser
        if Type == 'Laser':
            if self.dataFrame.query('Status ==\'active\' and '+'Type == \''+Type+'\'').shape[0] == 0:
                if self.dataFrame.query('Status ==\'open\' and '+'Type == \''+Type+'\'').shape[0] > 0:
                    #if there is no active position then pick the next open position and take an image there
                    self.dataFrame.loc[self.dataFrame.query('Status ==\'open\' and '+'Type == \''+Type+'\'').head(1).index.values[0],'Status'] = 'active'
                    self.Stage.moveToPosition(self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and '+'Type == \''+Type+'\'').head(1).index.values[0],'StagePos'])
                else:
                    #if all positions are done then look if background images are requested
                     if self.dataFrame.query('Status ==\'open\' and '+'Type == \'Background\'').shape[0] > 0:
                         self.view.showBlockLaserMessageBox_signal.emit()

            elif self.dataFrame.query('Status ==\'active\' and '+'Type == \''+Type+'\'').shape[0] > 1:
                    raise MainError()
        # if the requested next position is a background image, then mark the next open one as active
        if Type == 'Background':
           if self.dataFrame.query('Status ==\'active\' and '+'Type == \''+Type+'\'').shape[0] == 0:
                if self.dataFrame.query('Status ==\'open\' and '+'Type == \''+Type+'\'').shape[0] > 0:
                    self.dataFrame.loc[self.dataFrame.query('Status ==\'open\' and '+'Type == \''+Type+'\'').head(1).index.values[0],'Status'] = 'active'
                    self.Stage.moveToPosition(self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and '+'Type == \''+Type+'\'').head(1).index.values[0],'StagePos'])

        #check for completed datasets and save them
        self.saveAndCleanDataFrame()

    def startTakingBackgroundImages(self):
        #callback of the message box requesting the user to block the laser
        if self.dataFrame.query('Status ==\'open\' and '+'Type == \'Background\'').shape[0] > 0:
            self.dataFrame.loc[self.dataFrame.query('Status ==\'open\' and '+'Type == \'Background\'').head(1).index.values[0],'Status'] = 'active'
            self.Stage.moveToPosition(self.dataFrame.loc[self.dataFrame.query('Status ==\'active\' and Type == \'Background\'').head(1).index.values[0],'StagePos'])

    def checkAndCreateDir(self, name):
        #checks if directory exists and if not create it
        directory = os.getcwd()+os.path.sep+name
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory

    def writeComment(self, directory):
        #writes a comment into a txt and into the dataframe if wanted
        comment = self.view.additionalComments.text()
        file = open(directory+os.path.sep+"comment.txt","w")
        file.write(comment)
        if self.dataFrame.loc[self.dataFrame['Status'] == 'active'].shape[0] == 1:
            self.dataFrame.loc[self.dataFrame.query('Status ==\'active\'').head(1).index.values[0],'Comment'] = comment

    def setStagePosSlider(self, stagePos):
        #sets the stage position slider to see where the stage is graphically
        self.view.stagePositionDesc.setText('Stage position is: '+str(stagePos)+' mm')
        self.view.stagePositionSlider.blockSignals(True)
        self.view.stagePositionSlider.setValue(int(stagePos))
        self.view.stagePositionSlider.blockSignals(False)

    def cutImage(self, im, x, y, x_width, y_width):
        im_xsize,im_ysize,maxVal = self.imageParams(im)
        x1lim = int(round(x-x_width))
        x2lim = int(round(x+x_width))
        y1lim = int(round(y-y_width))
        y2lim = int(round(y+y_width))
        if x1lim<0:
          x1lim=0
        if y1lim<0:
          y1lim=0
        if x2lim > im_xsize:
          x2lim=im_xsize-1
        if y2lim > im_ysize:
          y2lim=im_ysize-1
        ims = np.array(im[(y1lim):(y2lim),(x1lim):(x2lim)],copy = True)
        return ims

    def imageParams(self, im):
      #return the image parameters (x,y,max pixel value)
      return np.size(im,1),np.size(im,0),im.max()

    def addCrosshair(self, im):
      #adds a crosshair in the middle of the image
      im_ysize = np.size(im,0)
      im_xsize = np.size(im,1)
      maxVal = im.max()
      cv2.line(im,(int(im_xsize/2)-1,0),(int(im_xsize/2)-1,im_ysize-1),int(maxVal),2)
      cv2.line(im,(0,int(im_ysize/2)-1),(im_xsize-1,int(im_ysize/2)-1),int(maxVal),2)
      return im

    def start(self):
        app = PyuEyeQtApp()

        #initialze the Thorlabs LTS 300 with Serial Number 45876508
        self.Stage = ISM(self.stageSerialNo,self)

        # a basic qt window
        self.view = PyuEyeQtView()
        self.view.show()
        #link the image callback
        self.view.user_callback = self.process_image

        #connect the signals from ui
        self.view.stageSlider_signal.connect(self.setStagePostionSlider)
        self.view.runThrough_signal.connect(self.runStageThrough)
        self.view.stageStart_signal.connect(self.setStagePositionStart)
        self.view.stageEnd_signal.connect(self.setStagePositionEnd)
        self.view.crossHair_signal.connect(self.setCrossHair)
        self.view.takeBackgroundImage_signal.connect(self.setTakeBackgroundImages)
        self.view.automaticLaser_signal.connect(self.followLaserBeamAutomatically)
        self.view.abortMeasurement_signal.connect(self.abortMeasurement)
        self.view.showBlockLaserMessageBox_signal.connect(self.view.showBlockLaserMessageBox)
        self.view.startTakingBackgroundImage_signal.connect(self.startTakingBackgroundImages)

        # camera class to simplify uEye API access
        self.initCamera()
        self.exposure_parameters['current_step_size'] = int(round(self.cam.get_possible_steps_exposure()))/2

        # a thread that waits for new images and processes all connected views

        self.thread1 = FrameThread(self.cam, self.view)
        self.thread1.start()
        app.exit_connect(self.thread1.stop)
        app.exec_()

        # cleanup after window exit
        self.thread1.stop()
        self.thread1.join()
        self.cam.exit()
        self.Stage.close()

    def runStageThrough(self):

        try:
            #collect the stage positions from the linear positions
            stepsarray = []
            if self.view.StageStart.text() != '' and self.view.StageStepSize.text() != '' and self.view.StageEnd.text() != '':
                start = float(self.view.StageStart.text())
                end = float(self.view.StageEnd.text())
                stepsize = float(self.view.StageStepSize.text())
                logger.info(stepsize)
                if stepsize > 0:
                     for x in range(0,round(abs((end-start)/stepsize))+1):
                         position_mm = round(x/(abs((end-start)/stepsize))*(end-start)+start,5)
                         stepsarray.append(position_mm)

            #add the extra positions
            extra_pos = self.view.additionalStagePos.text()
            if extra_pos != '':
                for pos in extra_pos.split(","):
                  stepsarray.append(float(pos))
            #add the gouyphase positions
            if self.view.RayleighLength.text() != '' and self.view.focusPoint.text() != '' and self.view.GouyPhaseStepsize.text() != '' and self.view.RayleighLengthSteps.text() != '':
                try:
                  GouyPhaseArray = []
                  GouyPhaseStep = float(self.view.GouyPhaseStepsize.text())*math.pi
                  zR = float(self.view.RayleighLength.text())
                  z0 = float(self.view.focusPoint.text())
                  GouyPhaseMin = math.atan(-float(self.view.RayleighLengthSteps.text()))
                  GouyPhaseMax = math.atan(float(self.view.RayleighLengthSteps.text()))
                  for x in range(0,round(abs((GouyPhaseMax-GouyPhaseMin)/GouyPhaseStep))+1):
                    z = round(math.tan(GouyPhaseMin+x*GouyPhaseStep)*zR+z0,5)
                    if z < 0:
                      z = 0
                    if z > 300:
                      z = 300
                    GouyPhaseArray.append(z)
                    stepsarray.extend(GouyPhaseArray)
                except Exception as exp:
                    logger.info(exp)
            #sort the stage positions to measure
            stepsarray.sort()
            #discard the stage positions which are over the limit
            stepsarray = [step for step in stepsarray if not (step > 300 or step < 0)]
            timestamp = datetime.datetime.utcnow()

            # if no measurement name is provided a random 6 digit hex string is created instead
            if self.view.measurementNameField.text() == '':
                self.view.measurementNameField.setText(random.choice('0123456789abcdef')+random.choice('0123456789abcdef')+random.choice('0123456789abcdef')+random.choice('0123456789abcdef')+random.choice('0123456789abcdef')+random.choice('0123456789abcdef'))

            measurementName = self.view.measurementNameField.text()

            # Add data to loop and append to a dataframe
            dataFrameAdd = {}
            if self.view.repeatSequence.text() != '':
                try:
                    nr_meas = int(self.view.repeatSequence.text())
                except Exception as exp:
                    print(exp)
                for i in range (1,nr_meas+1):
                    for stagePos in stepsarray:
                        print(measurementName+'-'+str(i))
                        #add the stage position the dataframe
                        dataFrameAdd[len(dataFrameAdd)]={'MeasName':measurementName+'-'+str(i),'FolderName':timestamp.strftime("%Y%m%d-%H%M-")+measurementName+'-'+str(i),'StagePos':stagePos,'MeasTimestamp':timestamp,'Status':'open','Type':'Laser'}
                        #if wanted also add a request for a background image
                        if self.takeBackgroundImage:
                            dataFrameAdd[len(dataFrameAdd)]={'MeasName':measurementName+'-'+str(i),'FolderName':timestamp.strftime("%Y%m%d-%H%M-")+measurementName+'-'+str(i),'StagePos':stagePos,'MeasTimestamp':timestamp,'Status':'open','Type':'Background'}
            else:
                for stagePos in stepsarray:
                    #add the stage position the dataframe
                    dataFrameAdd[len(dataFrameAdd)]={'MeasName':measurementName,'FolderName':timestamp.strftime("%Y%m%d-%H%M-")+measurementName,'StagePos':stagePos,'MeasTimestamp':timestamp,'Status':'open','Type':'Laser'}
                    #if wanted also add a request for a background image
                    if self.takeBackgroundImage:
                        dataFrameAdd[len(dataFrameAdd)]={'MeasName':measurementName,'FolderName':timestamp.strftime("%Y%m%d-%H%M-")+measurementName,'StagePos':stagePos,'MeasTimestamp':timestamp,'Status':'open','Type':'Background'}

            self.dataFrame = self.dataFrame.append(pd.DataFrame.from_dict(dataFrameAdd, "index"),ignore_index=True,sort=True)
            self.checkAndExecuteRemainingPos('Laser')

        except Exception as exp:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setText("Wrong stage parameters")
            msg.setInformativeText("Missing or wrong parameters for stage with error message: "+str(exp))
            msg.setWindowTitle("Missing or wrong parameters for stage")
            msg.exec_()

    def initCamera(self):
        #init the camera and set the area of interest (AOI) to the maximium
        self.cam = Camera()
        self.cam.init()
        self.cam.set_colormode(ueye.IS_CM_MONO12)
        self.cam.set_aoi(0,0,3088,2076)
        self.cam.alloc()
        self.cam.set_gain()
        #Sets the color depth of the camera depending on the FPS that we want. Higher FPS can only be displayed if the color depth is reduced (due to limited bandwidth)
        self.cam.set_imaging_mode('quality')

        #Sets the biggest exposure time possible
        self.cam.set_max_exposure_time()

    def find_ellipses(self, im):

        # Otsu's threshbesting after Gaussian filtering
        blur = cv2.GaussianBlur(im,(5,5),0)
        ret,thresh = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        #find the contours
        _,contours,hierarchy = cv2.findContours(thresh, 1, 2)
        best_x, best_y, best_ma, best_MA, best_angle = 0, 0, 0 , 0 ,0
        #check if contours exist and the try to fit an ellipse to the controur if it has enough points
        if len(contours) != 0:
            for cont in contours:
                if len(cont) < 5:
                    continue
                (x,y),(ma,MA),angle = cv2.fitEllipse(cont)
                if ma > best_ma and MA > best_MA:
                    (best_x, best_y), (best_ma, best_MA), best_angle = (x,y),(ma,MA),angle
        #if no contour can be found return none
        if best_x == 0 and best_y == 0 and best_ma == 0 and best_MA == 0 and best_angle == 0:
            return None
        else:
            return (best_x, best_y), (best_ma, best_MA), best_angle

    def followLaserBeamAutomatically(self):
        self.followLaserBeam = self.view.automaticLaserFocusCheckBox.isChecked()

    def setCrossHair(self):
        self.crossHair = self.view.crossHairCheckBox.isChecked()

    def setTakeBackgroundImages(self):
        self.takeBackgroundImage = self.view.takeBackgroundImageCheckBox.isChecked()


    def setStagePositionStart(self):
        if self.Stage.stageInitialized:
            self.abortMeasurement()
            self.Stage.moveToPosition(0)

    def setStagePositionEnd(self):
        if self.Stage.stageInitialized:
            self.abortMeasurement()
            self.Stage.moveToPosition(300)

    def setStagePostionSlider(self):
        if self.Stage.stageInitialized:
            self.abortMeasurement()
            self.Stage.moveToPosition(self.view.stagePositionSlider.value())

    def saveAndCleanDataFrame(self):
        #check for completed datasets and save them, completed datasets are automatically deleted from the dataframe
        uniqueMeas = self.dataFrame['MeasName'].unique()
        for Meas in uniqueMeas:
            dataFrameMeas =  self.dataFrame.loc[self.dataFrame['MeasName'] == Meas]
            if dataFrameMeas.loc[dataFrameMeas['Status'] != 'done'].shape[0] == 0 or dataFrameMeas.loc[dataFrameMeas['Status'] == 'aborted'].shape[0] > 0:
                directory = "Measurements"+os.path.sep+dataFrameMeas['FolderName'].unique()[0]+os.path.sep
                self.checkAndCreateDir(directory)
                dataFrameMeas.to_csv(path_or_buf = directory+'data.csv' ,sep=';')
                self.dataFrame =  self.dataFrame.loc[self.dataFrame['MeasName'] != Meas]

    def abortMeasurement(self):
        #aborts the measurement and cleans the dataframe
        logger.info('Request abort measurement')
        if self.dataFrame.loc[self.dataFrame['Status'] == 'active'].shape[0] == 1:
            self.dataFrame['Status'] = 'aborted'
            self.saveAndCleanDataFrame()
        elif self.dataFrame.loc[self.dataFrame['Status'] == 'active'].shape[0] > 1:
            raise Exception

p = M2()
p.start()