#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  2 13:35:52 2022

@author: Cotton
"""

from PIL import Image
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime

directories = ['/Users/Cotton/Downloads/20220803']

hists = []
Timestamp = []
WaistX = []
WaistY = []
FolderName = []

w_maj_1d = []
w_min_1d = []

w_x_2d = []
w_y_2d = []

w_x_mom = []
w_y_mom = []

pos = 280

for directory in directories:
    folders = [d for d in os.listdir(directory) if os.path.isdir(os.path.join(directory, d))]
    folders.sort(key=lambda s: int(s.split('-')[-1]))
    
    for folder in folders:
        if folder.startswith('202208'): 
            #if not os.path.isfile(os.path.join(directory, folder, 'WxPlot.png')):
            print('doing ' + folder)
            
            metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')
            
            Timestamp.append(datetime.strptime(metadata['Timestamp'][0], '%Y-%m-%d %H:%M:%S.%f'))
            FolderName.append(folder)
            
            g1d = pd.read_csv(os.path.join(directory, folder, 'Gauss1DAnalysis/analysisGauss1D.csv'))
            w_maj_1d.append(g1d[g1d['StagePos_mm'] == pos]['w_px_maj'])
            w_min_1d.append(g1d[g1d['StagePos_mm'] == pos]['w_px_min'])
            
            g2d = pd.read_csv(os.path.join(directory, folder, 'Gauss2DAnalysis/analysisGauss2D.csv'))
            w_x_2d.append(g2d[g2d['StagePos_mm'] == pos]['wx_px'])
            w_y_2d.append(g2d[g2d['StagePos_mm'] == pos]['wy_px'])
            
            moments = pd.read_csv(os.path.join(directory, folder, 'SecondMomentsAnalysis/analysisSecondMoments.csv'))
            w_x_mom.append(moments[moments['StagePos_mm'] == pos]['wx_px'])
            w_y_mom.append(moments[moments['StagePos_mm'] == pos]['wy_px'])
            
            fitdf = pd.read_csv(os.path.join(directory, folder, 'WxFit.csv'))
            fitdf = fitdf.set_index('Method')
            WaistX.append(fitdf.loc['Gauss1D']['w0_mm'])
            
            fitdf = pd.read_csv(os.path.join(directory, folder, 'WyFit.csv'))
            fitdf = fitdf.set_index('Method')
            WaistY.append(fitdf.loc['Gauss1D']['w0_mm'])
            

Ellipticity = np.array(WaistX) / np.array(WaistY)


fig, ax = plt.subplots()
plt.plot(Timestamp, w_maj_1d, 'o', label='1D Gauss')
plt.plot(Timestamp, w_x_2d, 'o', label='2D Gauss')
plt.plot(Timestamp, w_x_mom, 'o', label='Second Moments')

fig.autofmt_xdate()
plt.legend()
plt.xlabel('Timestamp')
plt.ylabel('Beam Width [px]')
plt.title('Beam Width over Time for 20220803-P5-405BPM-FC2-PCSide \n Position ' + str(pos) + ' mm')

# plt.ylabel('Beam Ellipticity (waist_x / waist_y)')
# plt.title('Beam Ellipticity for 20220808-P3-405BPM-FC5')
            
            