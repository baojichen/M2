# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 09:54:19 2021

@author: Florian Egli
"""

import os
import sys
import numpy as np
import matplotlib.pyplot as plt

import scipy.special as special
import scipy.optimize
import scipy.integrate
import scipy.interpolate
import ImageFit
import multiprocessing as mp
import fitFunctions.Fit2DGaussian as fitGauss

import random

from PIL import Image

import time

import logging
import traceback #debug

def EFieldFiberMode(r,a,kT,gamma,s):
    r=abs(r)
    core = lambda x: FiberModeCore(x, kT)
    cladding = lambda x: FiberModeCladding(x, gamma, s)
    return np.piecewise(r, [np.where(r<=a, True, False),np.where(r>a, True, False)], [core, cladding])

def FiberModeCore(r, kT):
    return special.j0(kT*r)

def FiberModeCladding(r, gamma, s):
    return s*special.k0(gamma*r)

def BoundaryConditions(X, V):
    Y = np.sqrt(V**2-X**2)
    #the following has to be zero from boundary conditions
    return X*special.j1(X)/special.j0(X)-Y*special.k1(Y)/special.k0(Y)

def DetermFiberMode(lam, a, NA):
    V=NA*2*np.pi*a/lam
    fun = lambda x: BoundaryConditions(x,V)
    #scipy.optimize.root finds zero of the specified function, ensures boundary conditions
    sol= scipy.optimize.root(fun, 1)
    X=sol.x[0]
    Y=np.sqrt(V**2-X**2) 
    kT=X/a
    gamma=Y/a
    s=special.j0(kT*a)/special.k0(gamma*a)
    return (kT,gamma,s)

# BilligInt means "shabby integral" because this is a very brute force approach
def BilligInt(func,xmin,xmax,N):
    dx=(xmax-xmin)/N
    x=xmin    
    intval=0
    while x <= xmax:
        x+=dx
        intval += func(x)*dx
    return intval


# diffracted E-Field
def EBeug(E0,r,intlimit,z,N,lam):
    if z==0:
        return E0(r)
    k=2*np.pi/lam
    intfunc = lambda x: E0(x)*np.exp(-1j*k*x**2/2/z)*x*special.jv(0,k*x*r/z)
    return 2*np.pi*1j/lam/z*np.exp(-1j*k*r**2/2/z)*BilligInt(intfunc,0,intlimit,N)


def createImages(z, lam=405e-9, a=1.5e-6, NA=0.09974, theoryFolder="./theory/"):
    outputFolder = theoryFolder

    print("V=", 2*np.pi*a*NA/lam)
    if(2*np.pi*a*NA/lam>2.405): # should be V < 2.405
        print("V too large for",a,NA)
    
    intlim = 50*a
    #N = 1000
    N=500
    kT, gamma, s = DetermFiberMode(lam, a, NA)
    
    # camera is 11.3 mm by 11.3 mm, and 2048 pixels
    dx = 2.4e-6
    #dx = 11.3e-3/256
    x = np.arange(-3705.6e-6,3705.6e-6,dx)
    #x = np.arange(-2457.6e-6,2457.6e-6,dx)
    y = np.arange(-2491.2e-6,2491.2e-6,dx)
    #y = np.arange(-2457.6e-6,2457.6e-6,dx)
    #z = np.arange(5e-3,51e-3,1e-3)

    #imArray = np.zeros((x.size, y.size), dtype=int)
    xx, yy = np.meshgrid(x,y,sparse=False)
    t_tot = time.time()
    
    r = np.linspace(0, np.sqrt(2)*3705.6e-6, 2048*5)
    #r = np.linspace(0, np.sqrt(2)*11.3e-3/2, 256*5)
    
    #Fit Gauss to Fiber Mode
    fiberMode = lambda r: EFieldFiberMode(r, a, kT, gamma, s)
    grid = np.linspace(-intlim, intlim, 500)
    w0find = lambda r: np.abs(fiberMode(r))-np.abs(fiberMode(0))*np.exp(-2)
    sol = scipy.optimize.root(w0find, a/2)
    w0 = sol.x[0]/np.sqrt(2.0)

    # calculate initial gaussian
    # popt, pcov = scipy.optimize.curve_fit(lambda x,A,w: fitGauss.gauss1d(x,0,A,w,0), grid, np.abs((fiberMode(grid))**2), p0=[1,a/2])
    # A = popt[0]
    # w0 = popt[1]
    # print(A,w0)
    
    # plt.plot(grid, fitGauss.gauss1d(grid,0,1,w0,0), label="fitted gauss")
    # plt.plot(grid, np.abs(fiberMode(grid)), label="fiber mode")
    # plt.plot(grid, np.ones_like(grid)*np.exp(-2), label="1/e", color="green")
    # plt.axvline(w0, label="w0", color="green")

    # plt.title("Intensity Plot\nNA="+str(NA))
    # plt.legend()
    #plt.show()
    
    # create compare images
    # compare = str(outputFolder)+"Gauss/"+f'lam{lam*10**9:.0f}/'+f'a{a*10**6:.3f}/compare/'
    # if not os.path.exists(compare):
    #     os.makedirs(compare)
    # plt.savefig(str(outputFolder)+"Gauss/"+f'lam{lam*10**9:.0f}/'+f'a{a*10**6:.3f}/compare/'+f'NA{NA:.5f}.png')
   
    pool = mp.Pool(2)
    args = [[position, r, intlim, N, lam, a, NA, kT, gamma, s, 1, w0, xx, yy, outputFolder] for position in z]

    pool.starmap(createImage, args)

    # for position in z:
    #     createImage(position, r, intlim, N, lam, a, NA, kT, gamma, s, A, w0, xx, yy, outputFolder)
    
    print("NA", NA, "took", time.time()-t_tot, "s")
    

    imageFitter = ImageFit.ImageFitHandler()
    imageFitter.wavelength = lam
    imageFitter.pixelSize = dx
    imageFitter.dataFolder = str(outputFolder)+"FiberMode/"+f'lam{lam*10**9:.0f}/'+f'a{a*10**6:.3f}/'+f'NA{NA:.5f}/'
    #imageFitter.AnalyzeFolder()
    imageFitter.AnalyzeFolder1DAndD4S()

    #imageFitter.dataFolder = str(outputFolder)+"Gauss/"+f'lam{lam*10**9:.0f}/'+f'a{a*10**6:.3f}/'+f'NA{NA:.5f}/'
    #imageFitter.AnalyzeFolder()
    

    return outputFolder

def createImage(z, r, intlim, N, lam, a, NA, kT, gamma, s, A_Gauss, w0_Gauss, xx, yy, outputFolder):
    # model the nonlinearity of the sensor
    nonlin = 0.0
    power = 0 # order of the nonlinearity (x^power)
    
    path = f'lam{lam*10**9:.0f}/'+f'a{a*10**6:.3f}/'+f'NA{NA:.5f}/'
    
    FiberModeFolder = str(outputFolder)+"FiberMode/"+path
    if not os.path.exists(FiberModeFolder):
        os.makedirs(FiberModeFolder)

    '''    
    GaussFolder = str(outputFolder)+"Gauss/"+path
    if not os.path.exists(GaussFolder):
        os.makedirs(GaussFolder)
    '''
    
    position = z
    
    
    #Create Fiber Mode Image
    E0 = lambda r: EFieldFiberMode(r, a, kT, gamma, s)

    print(f"Working on position {position*1000:.3f}")
    E = EBeug(E0, r,intlim, position, N, lam)
    E2 = np.abs(E)**2
    I = scipy.interpolate.interp1d(r, E2, "linear")
    
    imArray = I(np.sqrt(xx**2+yy**2))
    imArray = imArray*(1-nonlin*(imArray/imArray.max())**power) # apply the nonlinearity
    imArray = imArray/imArray.max()*2047 # 8 bits

    '''
    #Add noise
    bkgSampleList = os.listdir("./bkgSamples/")
    file = random.choice(bkgSampleList)
    bkgSampleObj = Image.open("./bkgSamples/"+file)
    bkgSample = np.array(bkgSampleObj)
    #print(np.max(imArray), np.max(bkgSample))
    tmpArray = imArray
    #print(np.count_nonzero(bkgSample))
    imArray = imArray+bkgSample
    #print(np.count_nonzero(tmpArray != imArray))
    imArray[np.where(imArray>255)] = np.ones_like(imArray[np.where(imArray>255)])*255
    '''
    
    imArray = imArray.astype(np.uint16)
    imObj = Image.fromarray(imArray)

    
    imObj.save(FiberModeFolder+"{:.3f}mm.tif".format(position*1e3))
    

    bkgArray = np.zeros((2048,2048)) 
    bkgObj = Image.fromarray(bkgArray)
    bkgObj.save(FiberModeFolder+"Bkg-{:.3f}mm.tif".format(position*1e3))
    
    '''
    #Create Gauss Propagation Image
    zr = np.pi*w0_Gauss**2/lam
    w = lambda z: w0_Gauss*np.sqrt(1+(z/zr)**2)
    gauss = lambda r, z: A_Gauss*(w0_Gauss/w(z))**2*np.exp(-2*r**2/(w(z))**2)
    
    im2Array = gauss(np.sqrt(xx**2+yy**2), position)
    im2Array = im2Array/im2Array.max()*255
    im2Array = im2Array.astype(np.uint8)
    im2Obj = Image.fromarray(im2Array)
    
    im2Obj.save(GaussFolder+"{:.3f}mm.tif".format(position*1e3))
    bkgObj.save(GaussFolder+"Bkg-{:.3f}mm.tif".format(position*1e3))
    '''
    
    
if __name__=="__main__":
    '''
    logging.basicConfig(filename="BStheory.log", level=logging.DEBUG)
    
    tot = time.time()
    
    lam = 405e-9 # wavelength
    #aArray = np.linspace(1.0e-6, 3.0e-6,9)
    #aArray = [1e-6,1.25e-6,1.4e-6,1.45e-6,1.5e-6,1.55e-6,1.6e-6]
    #aArray = [1.25e-6,1.3e-6,1.35e-6,1.4e-6,1.45e-6,1.5e-6,1.55e-6,1.6e-6,1.65e-6,1.7e-6,1.75e-6]
    aArray = [1.32e-6,1.33e-6,1.34e-6,1.35e-6,1.36e-6,1.37e-6,1.38e-6,1.39e-6,1.40e-6,1.41e-6,1.42e-6]
    #NAArray = np.linspace(0.08, 0.13, num=3, endpoint=False)
    #NAArray = np.linspace(7/75, 0.12, num=4, endpoint=False) # 7/75 = 0.09333
    NAArray = [0.105,0.106,0.107,0.108,0.109,0.110,0.111,0.112,0.113]
    
    for a in aArray:
        print("Working on a=",a)

        theoryFolder = "./theory/"
        z = np.linspace(5e-3, 17e-3, 13)
        #z = np.linspace(5e-3, 25e-3, 11)
        #z = np.linspace(15e-3, 45e-3, 7)
        #z = np.linspace(19e-3, 49e-3, 16)
        #z = np.linspace(19e-3, 49e-3, 7)
        for NA in NAArray:
            try:
                createImages(z, lam, a, NA, theoryFolder)
            except Exception as e:
                print("NA",NA,"failed")
                print(e)
                print(traceback.format_exc())
                logging.warning(theoryFolder+f"FiberMode/lam{lam*10**9:.0f}/a{a*10**6:.3f}/NA{NA:.5f}")
                logging.warning(theoryFolder+f"Gauss/lam{lam*10**9:.0f}/a{a*10**6:.3f}/NA{NA:.5f}")
                #os.rmdir(theoryFolder+f"FiberMode/lam{lam*10**9:.0f}/a{a*10**6:.3f}/NA{NA:.5f}")
                #os.rmdir(theoryFolder+f"Gauss/lam{lam*10**9:.0f}/a{a*10**6:.3f}/NA{NA:.5f}")
    '''
    d = np.loadtxt(r'C:\Users\baojichen\Desktop\BeamSimulator\BeamSimulator\theory\FiberMode\lam405\b.txt', usecols=(0,1,3,7))
    dictWithA={}
    lastab=(0,0)
    for data in d:
        a=data[0]
        b=data[1]
        if(a==lastab[0] and b==lastab[1]):
            continue
        else:
            lastab=(a,b)    
        if(a in dictWithA):
            subdictW:dict=dictWithA[a][0]
            subdictW[data[1]]=data[2]
            subdictWErr:dict=dictWithA[a][1]
            subdictWErr[data[1]]=data[3]
        else:
            dictWithA[a]=({},{})
            subdictW:dict=dictWithA[a][0]
            subdictW[data[1]]=data[2]
            subdictWErr:dict=dictWithA[a][1]
            subdictWErr[data[1]]=data[3]
    fig=plt.figure(figsize=(7,13))
    axis1=fig.add_subplot(211)
    axis2=fig.add_subplot(212)
    for a in dictWithA:
        WDic=dictWithA[a][0]
        WErrDic=dictWithA[a][1]
        NAs=[]
        Ws=[]
        WErrs=[]
        for NA in WDic:
            NAs.append(NA)
            Ws.append(WDic[NA])
            WErrs.append(WErrDic[NA])
        axis1.plot(NAs, Ws,label=f"w0@a={a}")
        axis2.plot(NAs, WErrs,label=f"w0.Error@a={a}")
    fig.legend(title="a value/m")
    fig.savefig(r'C:\Users\baojichen\Desktop\BeamSimulator\BeamSimulator\theory\FiberMode\lam405\bc.png')
    input()    
    d = np.loadtxt(r'C:\Users\baojichen\Desktop\BeamSimulator\BeamSimulator\theory\FiberMode\lam405\a.txt', usecols=(0,1,2,6))
    dictWithA={}
    lastab=(0,0)
    for data in d:
        a=data[0]
        b=data[1]
        if(a==lastab[0] and b==lastab[1]):
            continue
        else:
            lastab=(a,b)    
        if(a in dictWithA):
            subdictW:dict=dictWithA[a][0]
            subdictW[data[1]]=data[2]
            subdictWErr:dict=dictWithA[a][1]
            subdictWErr[data[1]]=data[3]
        else:
            dictWithA[a]=({},{})
            subdictW:dict=dictWithA[a][0]
            subdictW[data[1]]=data[2]
            subdictWErr:dict=dictWithA[a][1]
            subdictWErr[data[1]]=data[3]
    fig=plt.figure(figsize=(7,13))
    axis1=fig.add_subplot(211)
    axis2=fig.add_subplot(212)
    for a in dictWithA:
        WDic=dictWithA[a][0]
        WErrDic=dictWithA[a][1]
        NAs=[]
        Ws=[]
        WErrs=[]
        for NA in WDic:
            NAs.append(NA)
            Ws.append(WDic[NA])
            WErrs.append(WErrDic[NA])
        axis1.plot(NAs, Ws,label=f"w0@a={a}")
        axis2.plot(NAs, WErrs,label=f"w0.Error@a={a}")
    fig.legend(title="a value/m")
    fig.savefig(r'C:\Users\baojichen\Desktop\BeamSimulator\BeamSimulator\theory\FiberMode\lam405\aa.png')
    #print(f"This whole operation took {(time.time()-tot)/60:.4f}min")