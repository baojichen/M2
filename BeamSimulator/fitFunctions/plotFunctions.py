# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 12:27:23 2022

@author: Florian Egli
"""

import os
import matplotlib.pyplot as plt
from fitFunctions.Fit2DGaussian import gauss1d
import numpy as np

# makes plots of fits of every picture, in fits/maj and fits/min
# makes plots of fit error of every picture
def plotFits(file, xvals_maj, yvals_maj, xvals_min, yvals_min, g1d, g2d, moments):
    folder_maj = "/".join(file.split("/")[0:-1])+"/fits/maj/"
    if not os.path.exists(folder_maj):
        os.makedirs(folder_maj)
    
    folder_min = "/".join(file.split("/")[0:-1])+"/fits/min/"
    if not os.path.exists(folder_min):
        os.makedirs(folder_min)
    
    file_maj = folder_maj + (file.split("/")[-1]).split(".tif")[0]+"_maj.png"
    file_min = folder_min + (file.split("/")[-1]).split(".tif")[0]+"_min.png"
    
    #plot maj fit
    plt.clf()
    plt.plot(xvals_maj, yvals_maj, label="data")
    
    #gauss1d(r,r0,A,w,c)
    f2d = lambda x: gauss1d(x, g1d["r0_px_maj"], g2d["PeakValue"], g2d["wx_px"], g2d["offset"])
    plt.plot(xvals_maj, f2d(xvals_maj), label="g2d_fit")
    
    f1d = lambda x: gauss1d(x, g1d["r0_px_maj"], g1d["PeakValue_maj"], g1d["w_px_maj"], g1d["c_px_maj"])
    plt.plot(xvals_maj, f1d(xvals_maj), label="g1d_fit")
    
    #mom = lambda x: gauss1d(x, g1d["r0_px_maj"], g2d["PeakValue"], moments["wx_px"], 0)
    #plt.plot(xvals_maj, mom(xvals_maj), label="mom_fit")
    
    plt.legend()
    plt.title("Maj")
    plt.savefig(file_maj)
    
    #plot min fit
    plt.clf()
    plt.plot(xvals_min, yvals_min, label="data")
    
    #gauss1d(r,r0,A,w,c)
    f2d = lambda x: gauss1d(x, g1d["r0_px_min"], g2d["PeakValue"], g2d["wy_px"], g2d["offset"])
    plt.plot(xvals_min, f2d(xvals_min), label="g2d_fit")
    
    f1d = lambda x: gauss1d(x, g1d["r0_px_min"], g1d["PeakValue_min"], g1d["w_px_min"], g1d["c_px_min"])
    plt.plot(xvals_min, f1d(xvals_min), label="g1d_fit")
    
    #mom = lambda x: gauss1d(x, g1d["r0_px_min"], g2d["PeakValue"], moments["wy_px"], 0)
    #plt.plot(xvals_min, mom(xvals_min), label="mom_fit")
    
    plt.legend()
    plt.title("Min")
    plt.savefig(file_min)
    

    
    # PLOTTING ERRORS IN FIT
    #plot error in maj fit
    plt.clf()
    
    folder_maj = "/".join(file.split("/")[0:-1])+"/fits_err/maj/"
    if not os.path.exists(folder_maj):
        os.makedirs(folder_maj)
    
    folder_min = "/".join(file.split("/")[0:-1])+"/fits_err/min/"
    if not os.path.exists(folder_min):
        os.makedirs(folder_min)
        
    
    file_maj = folder_maj + (file.split("/")[-1]).split(".tif")[0]+"_maj.png"
    file_min = folder_min + (file.split("/")[-1]).split(".tif")[0]+"_min.png"
    
    
    #smooth errors using rolling average
    kernel_size = 5
    kernel = np.ones(kernel_size)/kernel_size
    
    f2d = lambda x: gauss1d(x, g1d["r0_px_maj"], g2d["PeakValue"], g2d["wx_px"], g2d["offset"])
    #plt.plot(xvals_maj, yvals_maj-f2d(xvals_maj), label="g2d_fit")
    f2derr = np.convolve(yvals_maj-f2d(xvals_maj), kernel, mode='same')
    plt.plot(xvals_maj, f2derr, label="g2d_fit")
    
    f1d = lambda x: gauss1d(x, g1d["r0_px_maj"], g1d["PeakValue_maj"], g1d["w_px_maj"], g1d["c_px_maj"])
    #plt.plot(xvals_maj, yvals_maj-f1d(xvals_maj), label="g1d_fit")
    f1derr = np.convolve(yvals_maj-f1d(xvals_maj), kernel, mode='same')
    plt.plot(xvals_maj, f1derr, label="g1d_fit")
    
    #mom = lambda x: gauss1d(x, g1d["r0_px_maj"], g2d["PeakValue"], moments["wx_px"], 0)
    #plt.plot(xvals_maj, yvals_maj-mom(xvals_maj), label="mom_fit")
    
    plt.legend()
    plt.title("Maj")
    plt.savefig(file_maj)
    
    #plot error in min fit
    plt.clf()
    
    #gauss1d(r,r0,A,w,c)
    f2d = lambda x: gauss1d(x, g1d["r0_px_min"], g2d["PeakValue"], g2d["wy_px"], g2d["offset"])
    #plt.plot(xvals_min, yvals_min-f2d(xvals_min), label="g2d_fit")
    f2derr = np.convolve(yvals_min-f2d(xvals_min), kernel, mode='same')
    plt.plot(xvals_min, f2derr, label="g2d_fit")
    
    f1d = lambda x: gauss1d(x, g1d["r0_px_min"], g1d["PeakValue_min"], g1d["w_px_min"], g1d["c_px_min"])
    #plt.plot(xvals_min, yvals_min-f1d(xvals_min), label="g1d_fit")
    f1derr = np.convolve(yvals_min-f1d(xvals_min), kernel, mode='same')
    plt.plot(xvals_min, f1derr, label="g1d_fit")
    
    #mom = lambda x: gauss1d(x, g1d["r0_px_min"], g2d["PeakValue"], moments["wy_px"], 0)
    #plt.plot(xvals_min, yvals_min-mom(xvals_min), label="mom_fit")
    
    plt.legend()
    plt.title("Min")
    plt.savefig(file_min)

    
    
    
        
def plotFits1D(file, xvals_maj, yvals_maj, xvals_min, yvals_min, g1d):
    folder_maj = "/".join(file.split("/")[0:-1])+"/fits/maj/"
    if not os.path.exists(folder_maj):
        os.makedirs(folder_maj)
    
    folder_min = "/".join(file.split("/")[0:-1])+"/fits/min/"
    if not os.path.exists(folder_min):
        os.makedirs(folder_min)
    
    file_maj = folder_maj + (file.split("/")[-1]).split(".tif")[0]+"_maj.png"
    file_min = folder_min + (file.split("/")[-1]).split(".tif")[0]+"_min.png"
    
    #plot data
    plt.clf()
    plt.plot(xvals_maj, yvals_maj, label="data")
    
    #gauss1d(r,r0,A,w,c)
    # f2d = lambda x: gauss1d(x, g1d["r0_px_maj"], g2d["PeakValue"], g2d["wx_px"], g2d["offset"])
    # plt.plot(xvals_maj, f2d(xvals_maj), label="g2d_fit")
    
    f1d = lambda x: gauss1d(x, g1d["r0_px_maj"], g1d["PeakValue_maj"], g1d["w_px_maj"], g1d["c_px_maj"])
    plt.plot(xvals_maj, f1d(xvals_maj), label="g1d_fit")
    
    #mom = lambda x: gauss1d(x, g1d["r0_px_maj"], g2d["PeakValue"], moments["wx_px"], 0)
    #plt.plot(xvals_maj, mom(xvals_maj), label="mom_fit")
    
    plt.legend()
    plt.title("Maj")
    plt.savefig(file_maj)
    
    #plot min fit
    plt.clf()
    plt.plot(xvals_min, yvals_min, label="data")
    
    #gauss1d(r,r0,A,w,c)
    # f2d = lambda x: gauss1d(x, g1d["r0_px_min"], g2d["PeakValue"], g2d["wy_px"], g2d["offset"])
    # plt.plot(xvals_min, f2d(xvals_min), label="g2d_fit")
    
    f1d = lambda x: gauss1d(x, g1d["r0_px_min"], g1d["PeakValue_min"], g1d["w_px_min"], g1d["c_px_min"])
    plt.plot(xvals_min, f1d(xvals_min), label="g1d_fit")
    
    #mom = lambda x: gauss1d(x, g1d["r0_px_min"], g2d["PeakValue"], moments["wy_px"], 0)
    #plt.plot(xvals_min, mom(xvals_min), label="mom_fit")
    
    plt.legend()
    plt.title("Min")
    plt.savefig(file_min)
    
    # PLOTTING ERRORS IN FIT
    #plot error in maj fit
    plt.clf()
    
    folder_err = "/".join(file.split("/")[0:-1])+"/fits_err/"
    if not os.path.exists(folder_err):
        os.makedirs(folder_err)

    
    file_err = folder_err + (file.split("/")[-1]).split(".tif")[0]+"_err.png"

    f1d = lambda x: gauss1d(x, g1d["r0_px_maj"], g1d["PeakValue_maj"], g1d["w_px_maj"], g1d["c_px_maj"])
    plt.plot(xvals_maj, yvals_maj-f1d(xvals_maj), '.', label="g1d_fit maj")

    
    f1d = lambda x: gauss1d(x, g1d["r0_px_min"], g1d["PeakValue_min"], g1d["w_px_min"], g1d["c_px_min"])
    plt.plot(xvals_min, yvals_min-f1d(xvals_min), '.', label="g1d_fit min")
    
    ax = plt.gca()
    ax.set_ylim([-200, 200])

    plt.legend()
    plt.title("Maj and Min fit errors")
    plt.savefig(file_err)