# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 16:25:28 2021

@author: Florian Egli
"""

import sys
sys.path.append('/Users/Cotton/Documents/fiberanalysis/fitFunctions/') # so spyder can find the module when debugging
import traceback #debug
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import scipy.interpolate
import fitFunctions.Fit2DGaussian as Fit2DGauss
import fitFunctions.plotFunctions as pltFun
import copy
import cv2
from PIL import Image

def runAnalysis(filePair, selfconfwidth=2):
    sys.stdout.flush()
    file = filePair[0]
    bkgFile = filePair[1]
    try:
        imObj = Image.open(file)
        #this line fails if tif is LZW-compressed
        im = np.array(imObj)
        im = im.astype(np.int64)
        
        try:
            bkgObj = Image.open(bkgFile)
            #this line fails if tif is LZW-compressed
            bkgIm = np.array(bkgObj)
            bkgIm = bkgIm.astype(np.int64)

        except Exception as e:
            print("Opening BkgFile fail\nFile: ", bkgFile)
            print(e)
            bkgIm = None    
            
    except Exception as e:
        print("Opening file fail\nFile: ", file)
        print(e)
        #return None
    
    try:
        print("Running analysis", file.split("/")[-1])
        print("g2D")
        g2d = fitImageGauss2D(im, bkgIm)
        print("g1D")
        g1d = fitImageGauss1DCuts_zeroRot(im, bkgIm)
        #g1d = fitImageGauss1DCuts_fixedRot(im, bkgIm, g2d)
        #g1d = fitImageGauss1DCuts(im, bkgIm, g2d)
        #moments = calcSecondMoments(im, selfconfwidth)
        print("d4S")
        moments = d4sigma(im, bkgIm, selfconfwidth)
        print("done")
        # dummy moments fit for faster fitting
        # moments = {
        #     'PeakValue': 3100,
        #     'x_px': 1024,
        #     'y_px': 1024,
        #     'wx_px': 500,
        #     'wy_px': 500,
        #     'alpha': 80,
        #     'Background_Mean': 0,
        #     'Background_SD': 0,
        #     'Background_SEM': 0,
        # }
        
        #plot the fit and remove pic data from g1d
        xvals_maj = g1d.pop("Xvals_maj")
        yvals_maj = g1d.pop("Yvals_maj")
        xvals_min = g1d.pop("Xvals_min")
        yvals_min = g1d.pop("Yvals_min")
        # pltFun.plotFits(file, xvals_maj, yvals_maj, xvals_min, yvals_min, g1d, g2d, moments)
        
        return {"Gauss2D": g2d,
                "Gauss1D": g1d,
                "Moments": moments}
    except Exception as e:
        print("Image Fit Fail\nFile:", file)
        print(e)
        print(traceback.format_exc())
        # or
        #print(sys.exc_info()[2])
        sys.exit()
def runAnalysis1DAndD4S(filePair, selfconfwidth=2):
    sys.stdout.flush()
    file = filePair[0]
    bkgFile = filePair[1]
    try:
        imObj = Image.open(file)
        #this line fails if tif is LZW-compressed
        im = np.array(imObj)
        im = im.astype(np.int64)
        
        try:
            bkgObj = Image.open(bkgFile)
            #this line fails if tif is LZW-compressed
            bkgIm = np.array(bkgObj)
            bkgIm = bkgIm.astype(np.int64)

        except Exception as e:
            print("Opening BkgFile fail\nFile: ", bkgFile)
            print(e)
            bkgIm = None    
            
    except Exception as e:
        print("Opening file fail\nFile: ", file)
        print(e)
        #return None
    
    try:
        print("Running analysis", file.split("/")[-1])
        print("g1D")
        g1d = fitImageGauss1DCuts_zeroRot(im, bkgIm)
        #g1d = fitImageGauss1DCuts_fixedRot(im, bkgIm, g2d)
        #g1d = fitImageGauss1DCuts(im, bkgIm, g2d)
        #moments = calcSecondMoments(im, selfconfwidth)
        print("d4S")
        moments = d4sigma(im, bkgIm, selfconfwidth)
        print("done")
        # dummy moments fit for faster fitting
        # moments = {
        #     'PeakValue': 3100,
        #     'x_px': 1024,
        #     'y_px': 1024,
        #     'wx_px': 500,
        #     'wy_px': 500,
        #     'alpha': 80,
        #     'Background_Mean': 0,
        #     'Background_SD': 0,
        #     'Background_SEM': 0,
        # }
        
        #plot the fit and remove pic data from g1d
        xvals_maj = g1d.pop("Xvals_maj")
        yvals_maj = g1d.pop("Yvals_maj")
        xvals_min = g1d.pop("Xvals_min")
        yvals_min = g1d.pop("Yvals_min")
        # pltFun.plotFits(file, xvals_maj, yvals_maj, xvals_min, yvals_min, g1d, g2d, moments)
        
        return {
                "Gauss1D": g1d,
                "Moments": moments}
    except Exception as e:
        print("Image Fit Fail\nFile:", file)
        print(e)
        print(traceback.format_exc())
        # or
        #print(sys.exc_info()[2])
        sys.exit()
def runAnalysis1D(filePair, selfconfwidth=2):
    sys.stdout.flush()
    file = filePair[0]
    bkgFile = filePair[1]
    try:
        imObj = Image.open(file)
        #this line fails if tif is LZW-compressed
        im = np.array(imObj)
        im = im.astype(np.int64)
        
        try:
            bkgObj = Image.open(bkgFile)
            #this line fails if tif is LZW-compressed
            bkgIm = np.array(bkgObj)
            bkgIm = bkgIm.astype(np.int64)

        except Exception as e:
            print("Opening BkgFile fail\nFile: ", bkgFile)
            print(e)
            bkgIm = None    
            
    except Exception as e:
        print("Opening file fail\nFile: ", file)
        print(e)
        #return None
    
    try:
        print("Running 1D analysis", file.split("/")[-1])

        # alpha = 0.15
        # n = 2
        # im = applyNonlinearity(im, alpha, n)
        g1d = fitImageGauss1DCuts_zeroRot(im, bkgIm)
        
        #plot the fit and remove pic data from g1d
        xvals_maj = g1d.pop("Xvals_maj")
        yvals_maj = g1d.pop("Yvals_maj")
        xvals_min = g1d.pop("Xvals_min")
        yvals_min = g1d.pop("Yvals_min")
        pltFun.plotFits1D(file, xvals_maj, yvals_maj, xvals_min, yvals_min, g1d)
        
        return {"Gauss1D": g1d
            }
    except Exception as e:
        print("Image Fit Fail\nFile:", file)
        print(e)
        print(traceback.format_exc())
        # or
        #print(sys.exc_info()[2])
        sys.exit()

def applyNonlinearity(im, alpha, n):
    # model sensor nonlinearity on top of image
    im = im * (1-alpha*np.power(im/4096,n) )
    im = np.rint(im) # round to nearest integer
    return im
        
def calcSecondMoments(im, selfconfwidth=2):
    # data = precut image
    # pos is position of
    # comimg is complete image
    #deepcopy precut image
    imCalcMoments = copy.deepcopy(im)
    # Size of image
    x_size = np.size(imCalcMoments,1)
    y_size = np.size(imCalcMoments,0)
    
    #Convert image to uint8 for ellipse fit
    im8 = imCalcMoments.astype(np.uint8)
    #fit ellipse for first guess of moments and for background determination
    (xx, yy), (best_ma, beam_widthEllipse), best_angle = find_ellipses(im8)
    
    ######################## Background determination and substraction START ########################################
    
    # Width of beam excluded for background determination:
    # beam_widthBkg is NwidthBkg times the fitted width from ellipse to which the image is cut below for background determination
    NwidthBkg=4
    beam_widthBkg,xx,yy = int(beam_widthEllipse*NwidthBkg),int(xx),int(yy)
        
    # Borders for masking image to get background information when calculatign moments
    x1lim = xx-beam_widthBkg
    x2lim = xx+beam_widthBkg
    y1lim = yy-beam_widthBkg
    y2lim = yy+beam_widthBkg
    if x1lim<0:
        x1lim=0
    if y1lim<0:
        y1lim=0
    if x2lim > np.size(im8,1)-1:
        x2lim=np.size(im8,1)-1
    if y2lim > np.size(im8,0)-1:
        y2lim=np.size(im8,0)-1

    bkg_avg = 0
    bkg_std = 0
    bkg_err = 0
    masked_img = 0
    #masked_max = 0

    # Determine and substract background from the area NwidthEllipse times the fitted ellipse width:
    img = copy.deepcopy(imCalcMoments)
    mask1 = np.full(imCalcMoments.shape,False)
    mask1[y1lim:y2lim,x1lim:x2lim] = True
    masked_img = np.ma.array(img,mask = mask1)
    bkg_avg = np.ma.mean(masked_img)
    bkg_std = np.ma.std(masked_img)
    bkg_err = np.ma.std(masked_img)/np.sqrt(len(masked_img.compressed()))
    #masked_max = (masked_img.compressed()).max()
    #print('Bkg data (mean, std, err, max):',bkg_avg,bkg_std,bkg_err,masked_max)
    #print('Max of image:',im.max())
    #print('Rel bkg:',bkg_avg/im.max())
    # substracting background without setting negative values to zero:
    imCalcMoments = imCalcMoments-abs(bkg_avg)
    # Substracting average value of background and setting negative values to zero:
    # im = cv2.subtract(im,abs(bkg_avg))

   ######################## Background determination and substraction END ########################################

    #Starting values from Ellipse fit
    x, y, width_x, width_y = xx, yy, beam_widthEllipse/4, beam_widthEllipse/4
    #Setting variables for iterative determination of beam center and width
    x_p, y_p, width_x_p,width_y_p,im2 = 0,0,0,0,0
    max_iterations= 100
    iterations = 0
    while not (x_p==x and y_p==y and width_x_p==width_x and width_y_p==width_y):
        x1lim = int(round(x-width_x/2*selfconfwidth,0))
        x2lim = int(round(x+width_x/2*selfconfwidth,0))
        y1lim = int(round(y-width_y/2*selfconfwidth,0))
        y2lim = int(round(y+width_y/2*selfconfwidth,0))
        warningmessage='Warning: image is too small for full integration area of moments calculation'
        if x1lim<0:
            x1lim=0
            print(warningmessage)
            break
        if y1lim<0:
            y1lim=0
            print(warningmessage)
            break
        if x2lim > x_size-1:
            x2lim=x_size-1
            print(warningmessage)
            break
        if y2lim > y_size-1:
            y2lim=y_size-1
            print(warningmessage)
            break

        im2 = imCalcMoments[y1lim:y2lim,x1lim:x2lim]
        x_p = copy.deepcopy(x)
        y_p = copy.deepcopy(y)
        width_x_p = copy.deepcopy(width_x)
        width_y_p = copy.deepcopy(width_y)
        x, y, width_x, width_y, phi = beam_size(copy.deepcopy(im2))
        #x, y, width_x, width_y, phi = beam_size_rot(copy.deepcopy(im2))
        x += x1lim
        y += y1lim
        iterations += 1
        if iterations > max_iterations:
            break

    # Beam size adjusted by factor of 2 to fit definition of a
    # transversal gaussian beam profile
    # (1/e^2 intensity radius instead of 4 sigma width of a probability distribution)
    
    #Peak Value does not really make sense here: For Gaussian Fits it is the maximum of the fitfunction, not the maximum pixel value of the image
    results = {
        'PeakValue': im2.max(),
        'x_px': x,
        'y_px': y,
        'wx_px': max(width_x/2,width_y/2),
        'wy_px': min(width_x/2,width_y/2),
        'alpha': np.abs(phi)*360/(2*np.pi),
        'Background_Mean': bkg_avg,
        'Background_SD': bkg_std,
        'Background_SEM': bkg_err,
    }
    return results 
  

def d4sigma(im, bkgIm, selfconfwidth=2):
    imD4Sigma = copy.deepcopy(im)
    if bkgIm is not None:
        imD4Sigma = imD4Sigma - bkgIm
        imD4Sigma = imD4Sigma.clip(min=0)
    else:
        print("WARNING: No valid bkgFile provided")
        
    y_size, x_size = imD4Sigma.shape  
    #Starting values
    #x, y, width_x, width_y, phi = beam_size_rot(imD4Sigma)
    x, y, width_x, width_y, phi = beam_size(imD4Sigma)
    #Setting variables for iterative determination of beam center and width
    x_p, y_p, width_x_p,width_y_p = 0,0,0,0
    im2 = copy.deepcopy(imD4Sigma)
    max_iterations= 100
    iterations = 0
    while not (x_p==x and y_p==y and width_x_p==width_x and width_y_p==width_y):
        x1lim = int(round(x-width_x/2*selfconfwidth,0))
        x2lim = int(round(x+width_x/2*selfconfwidth,0))
        y1lim = int(round(y-width_y/2*selfconfwidth,0))
        y2lim = int(round(y+width_y/2*selfconfwidth,0))
        print((x1lim,x2lim,y1lim,y2lim,x_size,y_size))
        warningmessage='Warning: image is too small for full integration area of moments calculation'
        if x1lim<0:
            x1lim=0
            print(warningmessage)
            #break
        if y1lim<0:
            y1lim=0
            print(warningmessage)
            #break
        if x2lim > x_size-1:
            x2lim=x_size-1
            print(warningmessage)
            #break
        if y2lim > y_size-1:
            y2lim=y_size-1
            print(warningmessage)
            #break
        
        im2 = imD4Sigma[y1lim:y2lim,x1lim:x2lim]
        x_p = copy.deepcopy(x)
        y_p = copy.deepcopy(y)
        width_x_p = copy.deepcopy(width_x)
        width_y_p = copy.deepcopy(width_y)
        
        #set phi = 0 in calculations of width_x and width_y
        x, y, width_x, width_y, phi = beam_size(copy.deepcopy(im2))
        #x, y, width_x, width_y, phi = beam_size_rot(copy.deepcopy(im2))
        
        x += x1lim
        y += y1lim
        iterations += 1
        if iterations > max_iterations:
            break

    # Beam size adjusted by factor of 2 to fit definition of a
    # transversal gaussian beam profile
    # (1/e^2 intensity radius instead of 4 sigma width of a probability distribution)
    
    #Peak Value does not really make sense here: For Gaussian Fits it is the maximum of the fitfunction, not the maximum pixel value of the image
    results = {
        'PeakValue': im2.max(),
        'x_px': x,
        'y_px': y,
        'wx_px': np.abs(width_x/2),
        'wy_px': np.abs(width_y/2),
        'alpha': np.abs(phi)*360/(2*np.pi),
        'Background_Mean': 0,
        'Background_SD': 0,
        'Background_SEM': 0,
    }
    
    return results 
    

def fitImageGauss2D(im, bkgIm=None):
    if bkgIm is not None:
        imGauss2D = im - bkgIm
        imGauss2D = imGauss2D.clip(min=0)
    else:
        print("WARNING: No valid bkgFile provided")
        imGauss2D = im
    try:
        imGauss2D = reduceImageSize(imGauss2D)
    except RuntimeError as e:
        print(e)
        imGauss2D=imGauss2D[int(0.15*imGauss2D.shape[0]):int(0.85*imGauss2D.shape[0]),int(0.15*imGauss2D.shape[1]):int(0.85*imGauss2D.shape[1])]
    # free rotation axes, fit alpha as well
    #result, error = Fit2DGauss.fitGaussian2D(imGauss2D)

    # fixed rotation axes
    result, error = Fit2DGauss.fitGaussian2DRotFixed(imGauss2D)
    result = np.insert(result, 5, 0.1) # insert rotation angle alpha back into result
    
    return {"PeakValue": result[0],
            "x_px": result[1],
            "y_px": result[2],
            "wx_px": np.abs(result[3]),
            "wy_px": np.abs(result[4]),
            "alpha": result[5],
            "alpha_err": 0,
            "offset": result[6]}
    '''
    "alpha": result[5],
    "alpha_err": error[5],
    '''
    '''
    "wx_px": max(result[3],result[4]),
    "wy_px": min(result[3],result[4]),
    '''
    
def fitImageGauss1DCuts(im, bkgIm, fitResultsPx):
    if fitResultsPx is None:
        print("Need successful fitImageGauss2D run first!")
        return None
    
    if bkgIm is not None:
        imGauss1D = im-bkgIm
        imGauss1D = imGauss1D.clip(min=0)
    else:
        imGauss1D = im
        
    try:
        imGauss1D=reduceImageSize(imGauss1D)
    except RuntimeError:
        print('fitImageGauss1DCuts runtime error')
        #im=im[int(0.1*im.shape[0]):int(0.8*im.shape[0]),int(0.1*im.shape[1]):int(0.9*im.shape[1])]        
        imGauss1D=imGauss1D[int(0.15*imGauss1D.shape[0]):int(0.85*imGauss1D.shape[0]),int(0.15*imGauss1D.shape[1]):int(0.85*imGauss1D.shape[1])]     
        imGauss1D=reduceImageSize(imGauss1D)
          
    # Load current fit results
    p = fitResultsPx

    x = np.arange(0,imGauss1D.shape[0])
    y = np.arange(0,imGauss1D.shape[1])
    
    A=p['PeakValue']
    x0=p['x_px']
    y0=p['y_px']
    w1=p['wx_px']
    w2=p['wy_px']
    alpha=p['alpha']%90
    alpha_err = p['alpha_err']
    
    #theory images are very round, tan diverges for alpha=0
    if alpha_err>360:
        #print("Info: Theory Image or Beam is very Round")
        alpha = 15
    
    if np.abs(alpha)< 0.1:
        print("Warning: Calculated angle is too close to 0°, set it to 0.1°")
        alpha = 0.1
        
    if np.abs(alpha-90.0)<0.1:
        print("Warning: Calculated angle is too close to 90°, set it to 90.1°")
        alpha = 90.1
    
    #print('Rot. angle: %.3f'%alpha)
      
    inter2d = scipy.interpolate.RectBivariateSpline(x,y,imGauss1D)

    ## Major cut
    m=np.tan(alpha/360.0*2*np.pi)
    c=x0-m*y0
    
    xvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[0]/np.cos(alpha/360.0*2*np.pi)))))
    yvals=m*xvals+c
    if m>1: mask=(yvals>0) & (yvals<imGauss1D.shape[0])
    else: mask=(yvals>0) & (yvals<imGauss1D.shape[1])
    cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*np.pi))
    cutXvals=cutXvals-cutXvals[0]
    cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])
    popt, pcov = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w2,0)) 
    cut1 = popt
    cutXvals1 = cutXvals
    cutYvals1 = cutYvals
    
    ## Minor cut
    alpha=alpha+90
    m=np.tan(alpha/360.0*2*np.pi)
    c=x0-m*y0
    
    xvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[1]/np.cos(alpha/360.0*2*np.pi)))))
    yvals=m*xvals+c
    if m>1: mask=(yvals>0) & (yvals<imGauss1D.shape[0])
    else: mask=(yvals>0) & (yvals<imGauss1D.shape[1])
    cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*np.pi))
    cutXvals=cutXvals-cutXvals[0]
    cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])
    popt, pcov = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w1,0))
    cut2 = popt
    cutXvals2 = cutXvals
    cutYvals2 = cutYvals
    
    #return major cut fit as first entry of the tuple
    #if cut1[2] >= cut2[2]:
    if True:
        r0_maj, A_maj, w_maj, c_maj = cut1
        cutXvals_maj = cutXvals1
        cutYvals_maj = cutYvals1
        r0_min, A_min, w_min, c_min = cut2
        cutXvals_min = cutXvals2
        cutYvals_min = cutYvals2
    else:
        r0_maj, A_maj, w_maj, c_maj = cut2
        cutXvals_maj = cutXvals2
        cutYvals_maj = cutYvals2
        r0_min, A_min, w_min, c_min = cut1
        cutXvals_min = cutXvals1
        cutYvals_min = cutYvals1
    
    return {'PeakValue_maj':A_maj,
            'r0_px_maj': r0_maj,
            'w_px_maj':w_maj,
            'c_px_maj': c_maj,
            'Xvals_maj': cutXvals_maj,
            'Yvals_maj': cutYvals_maj,
            'PeakValue_min': A_min,
            'r0_px_min': r0_min,
            'w_px_min': w_min,
            'c_px_min': c_min,
            'Xvals_min': cutXvals_min,
            'Yvals_min': cutYvals_min}

def fitImageGauss1DCuts_fixedRot(im, bkgIm, fitResultsPx):
    if fitResultsPx is None:
        print("Need successful fitImageGauss2D run first!")
        return None
    
    if bkgIm is not None:
        imGauss1D = im-bkgIm
        imGauss1D = imGauss1D.clip(min=0)
        
    else:
        imGauss1D = im
        
    try:
        imGauss1D=reduceImageSize(imGauss1D)
    except RuntimeError:
        print('fitImageGauss1DCuts_fixedRot runtime error')
        #im=im[int(0.1*im.shape[0]):int(0.8*im.shape[0]),int(0.1*im.shape[1]):int(0.9*im.shape[1])]        
        imGauss1D=imGauss1D[int(0.15*imGauss1D.shape[0]):int(0.85*imGauss1D.shape[0]),int(0.15*imGauss1D.shape[1]):int(0.85*imGauss1D.shape[1])]     
        imGauss1D=reduceImageSize(imGauss1D)
          
    # Load current fit results
    p = fitResultsPx

    x = np.arange(0,imGauss1D.shape[0])
    y = np.arange(0,imGauss1D.shape[1])
    
    A=p['PeakValue']
    x0=p['x_px']
    y0=p['y_px']
    w1=p['wx_px']
    w2=p['wy_px']
    alpha=0.1 # fix rotation angle to be 0 degrees
    #print('Rot. angle: %.3f'%alpha)
      
    inter2d = scipy.interpolate.RectBivariateSpline(x,y,imGauss1D)

    ## x cut
    m=np.tan(alpha/360.0*2*np.pi) 
    c=x0-m*y0
    
    xvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[0]/np.cos(alpha/360.0*2*np.pi)))))
    yvals=m*xvals+c
    if m>1: mask=(yvals>0) & (yvals<imGauss1D.shape[0])
    else: mask=(yvals>0) & (yvals<imGauss1D.shape[1])
    cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*np.pi))
    cutXvals=cutXvals-cutXvals[0]
    cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])
    popt, pcov = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w2,0)) 
    cut1 = popt
    cutXvals1 = cutXvals
    cutYvals1 = cutYvals
    
    ## Minor cut
    alpha=alpha+90
    m=np.tan(alpha/360.0*2*np.pi)
    c=x0-m*y0
    
    xvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[1]/np.cos(alpha/360.0*2*np.pi)))))
    yvals=m*xvals+c
    if m>1: mask=(yvals>0) & (yvals<imGauss1D.shape[0])
    else: mask=(yvals>0) & (yvals<imGauss1D.shape[1])
    cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*np.pi))
    cutXvals=cutXvals-cutXvals[0]
    cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])

    popt, pcov = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w1,0))
    cut2 = popt
    cutXvals2 = cutXvals
    cutYvals2 = cutYvals
    
    # yvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[1]/np.cos(alpha/360.0*2*np.pi)))))
    # xvals=m*yvals+c
    # if m>1: mask=(xvals>0) & (xvals<imGauss1D.shape[1])
    # else: mask=(xvals>0) & (xvals<imGauss1D.shape[0])
    # cutXvals=abs(yvals[mask]/np.cos(alpha/360.0*2*np.pi))
    # cutXvals=cutYvals-cutYvals[0]
    # cutYvals=inter2d.ev(m*yvals[mask]+c,yvals[mask])
    # #
    # popt, pcov = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w1,0))
    # cut2 = popt
    # cutXvals2 = cutXvals
    # cutYvals2 = cutYvals
    
    #return major cut fit as first entry of the tuple
    #if cut1[2] >= cut2[2]:
    if True:
        r0_maj, A_maj, w_maj, c_maj = cut1
        cutXvals_maj = cutXvals1
        cutYvals_maj = cutYvals1
        r0_min, A_min, w_min, c_min = cut2
        cutXvals_min = cutXvals2
        cutYvals_min = cutYvals2
    else:
        r0_maj, A_maj, w_maj, c_maj = cut2
        cutXvals_maj = cutXvals2
        cutYvals_maj = cutYvals2
        r0_min, A_min, w_min, c_min = cut1
        cutXvals_min = cutXvals1
        cutYvals_min = cutYvals1
    
    return {'PeakValue_maj':A_maj,
            'r0_px_maj': r0_maj,
            'w_px_maj':w_maj,
            'c_px_maj': c_maj,
            'Xvals_maj': cutXvals_maj,
            'Yvals_maj': cutYvals_maj,
            'PeakValue_min': A_min,
            'r0_px_min': r0_min,
            'w_px_min': w_min,
            'c_px_min': c_min,
            'Xvals_min': cutXvals_min,
            'Yvals_min': cutYvals_min}

    """
    Hopefully a simpler, faster analysis that only considers 1D cuts along x and y
    """
def fitImageGauss1DCuts_zeroRot(im, bkgIm):
   
    if bkgIm is not None:
        imGauss1D = im-bkgIm
        #imGauss1D = im
        imGauss1D = imGauss1D.clip(min=0)
        
    else:
        imGauss1D = im
        
    try:
        imGauss1D=reduceImageSize(imGauss1D)
    except RuntimeError:
        print('fitImageGauss1DCuts_fixedRot runtime error')
        #im=im[int(0.1*im.shape[0]):int(0.8*im.shape[0]),int(0.1*im.shape[1]):int(0.9*im.shape[1])]        
        imGauss1D=imGauss1D[int(0.15*imGauss1D.shape[0]):int(0.85*imGauss1D.shape[0]),int(0.15*imGauss1D.shape[1]):int(0.85*imGauss1D.shape[1])]     
        imGauss1D=reduceImageSize(imGauss1D)
          
    # calculate moments (height, x, y, width_x, width_y) to get center
    p = Fit2DGauss.moments(imGauss1D)

    # x = np.arange(0,imGauss1D.shape[0])
    # y = np.arange(0,imGauss1D.shape[1])
    
    A=p[0]
    x0=p[1] # actually y0 (vertical peak)
    y0=p[2] # actually x0 (horizontal peak)
    w1=p[3]
    w2=p[4]
    # print('x0',x0)
    # print('y0',y0)
    # alpha=0.1 # fix rotation angle to be 0 degrees
    #print('Rot. angle: %.3f'%alpha)
      
    # inter2d = scipy.interpolate.RectBivariateSpline(x,y,imGauss1D)

    ## x cut
    # m=np.tan(alpha/360.0*2*np.pi) 
    # c=x0-m*y0
    
    # xvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[0]/np.cos(alpha/360.0*2*np.pi)))))
    # yvals=m*xvals+c
    # if m>1: mask=(yvals>0) & (yvals<imGauss1D.shape[0])
    # else: mask=(yvals>0) & (yvals<imGauss1D.shape[1])
    # cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*np.pi))
    # cutXvals=cutXvals-cutXvals[0]
    # cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])

    ## Major (x cut)
    cutXvals1 = np.arange(0, imGauss1D.shape[1])
    cutYvals1 = imGauss1D[int(x0), :]
    cut1, pcov1 = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals1, cutYvals1,(max(cutXvals1)/2,A,w2,0)) 

    plt.plot(cutXvals1,cutYvals1)
    plt.plot(cutXvals1,Fit2DGauss.gauss1d(cutXvals1,cut1[0],cut1[1],cut1[2],cut1[3]))
    plt.show()
    
    
    ## Minor cut
    # alpha=alpha+90
    # m=np.tan(alpha/360.0*2*np.pi)
    # c=x0-m*y0
    
    # xvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[1]/np.cos(alpha/360.0*2*np.pi)))))
    # yvals=m*xvals+c
    # if m>1: mask=(yvals>0) & (yvals<imGauss1D.shape[0])
    # else: mask=(yvals>0) & (yvals<imGauss1D.shape[1])
    # cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*np.pi))
    # cutXvals=cutXvals-cutXvals[0]
    # cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])
    
    cutXvals2 = np.arange(0, imGauss1D.shape[0])
    cutYvals2 = imGauss1D[:, int(y0)]
    cut2, pcov2 = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals2, cutYvals2,(max(cutXvals2)/2,A,w1,0))
    
    #plt.plot(cutXvals2,cutYvals2)
    #plt.show()
    # yvals = abs(np.cos(alpha/360.0*2*np.pi)*np.arange(0,abs(int(imGauss1D.shape[1]/np.cos(alpha/360.0*2*np.pi)))))
    # xvals=m*yvals+c
    # if m>1: mask=(xvals>0) & (xvals<imGauss1D.shape[1])
    # else: mask=(xvals>0) & (xvals<imGauss1D.shape[0])
    # cutXvals=abs(yvals[mask]/np.cos(alpha/360.0*2*np.pi))
    # cutXvals=cutYvals-cutYvals[0]
    # cutYvals=inter2d.ev(m*yvals[mask]+c,yvals[mask])
    # #
    # popt, pcov = scipy.optimize.curve_fit(Fit2DGauss.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w1,0))
    # cut2 = popt
    # cutXvals2 = cutXvals
    # cutYvals2 = cutYvals
    
    #return major cut fit as first entry of the tuple
    #if cut1[2] >= cut2[2]:
    # if True:
    r0_maj, A_maj, w_maj, c_maj = cut1
    cutXvals_maj = cutXvals1
    cutYvals_maj = cutYvals1
    r0_min, A_min, w_min, c_min = cut2
    cutXvals_min = cutXvals2
    cutYvals_min = cutYvals2
    # else:
    #     r0_maj, A_maj, w_maj, c_maj = cut2
    #     cutXvals_maj = cutXvals2
    #     cutYvals_maj = cutYvals2
    #     r0_min, A_min, w_min, c_min = cut1
    #     cutXvals_min = cutXvals1
    #     cutYvals_min = cutYvals1
    
    return {'PeakValue_maj':A_maj,
            'r0_px_maj': r0_maj,
            'w_px_maj':w_maj,
            'c_px_maj': c_maj,
            'Xvals_maj': cutXvals_maj,
            'Yvals_maj': cutYvals_maj,
            'PeakValue_min': A_min,
            'r0_px_min': r0_min,
            'w_px_min': w_min,
            'c_px_min': c_min,
            'Xvals_min': cutXvals_min,
            'Yvals_min': cutYvals_min,
            'wErr_maj':pcov1[2][2]*1000,
            'wErr_min':pcov2[2][2]*1000}


def reduceImageSize(data):
    # crops image if beam width takes up less than 1/6 of the image width
    data = data.astype(np.int64)
    X = np.arange(0, data.shape[0])
    Y = np.arange(0, data.shape[1])   
    xint = data.sum(axis=0) # sum each column
    yint = data.sum(axis=1) # sum each row
    x = np.sum(X*yint)/np.sum(yint)
    y = np.sum(Y*xint)/np.sum(yint)   
    print(Y)
    print(yint)
    print(y)
    
    if int(y)<len(data):
        col = data[:, int(y)]
    else:
        col = data[:,len(data)-1]
        
    width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/abs(col.sum()))/2.2
    height = data.max()
    maxrow=data[:,int(y)].argmax()
    maxcol=data[maxrow,:].argmax()
    xvals = np.arange(0,data.shape[1])
    
    gauss1d = lambda r,r0,A,w,c: A*np.exp(-2*(r-r0)**2/w**2)+c
    popt, pcov = scipy.optimize.curve_fit(gauss1d, xvals, data[maxrow,:],(x,height,width_x,data[0][0]),maxfev=10000)
    
    width=popt[2]  
    width=np.abs(width)
    if width<data.shape[1]/6:
        
        x1lim=maxrow-3*width
        x2lim=maxrow+3*width
        y1lim=maxcol-3*width
        y2lim=maxcol+3*width
        
        if x1lim<0:
            x1lim=0
        if y1lim<0:
            y1lim=0
        if x2lim > data.shape[0]:
            x2lim=data.shape[0]
        if y2lim > data.shape[1]:
            y2lim=data.shape[1]
        
        data=data[int(x1lim):int(x2lim),int(y1lim):int(y2lim)]
        print('image size has been reduced')
    
    return data
    


def find_ellipses(im):
    # Otsu's threshbesting after Gaussian filtering
    blur = cv2.GaussianBlur(im,(5,5),0)
    ret, thresh = cv2.threshold(
        blur, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(thresh, 1, 2)
    best_x, best_y, best_ma, best_MA, best_angle = 0, 0, 0 , 0 ,0
    if len(contours) != 0:
        for cont in contours:
            if len(cont) < 5:
                continue
            (x,y),(ma,MA),angle = cv2.fitEllipse(cont)
            if ma > best_ma and MA > best_MA:
                (best_x, best_y), (best_ma, best_MA), best_angle = (x,y),(ma,MA),angle

    if best_x == 0 and best_y == 0 and best_ma == 0 and best_MA == 0 and best_angle == 0:
        return None
    else:
        return (best_x, best_y), (best_ma, best_MA), best_angle


def beam_size(image):
        """
        Determines laser beam center, diameters, and tilt of beam according to ISO 11146 standard.

        Parameters
        ----------
        image : array_like
            should be a monochrome two-dimensional array

        Returns
        -------
        xc : int
            horizontal center of beam
        yc : int
            vertical center of beam
        dx : float
            horizontal diameter of beam
        dy : float
            vertical diameter of beam
        phi: float
            angle that beam is rotated (about center) from the horizontal axis
        """

        # force negative values to 0
        image = image.clip(min=0)

        #vertical and horizontal dimensions of beam
        v,h = image.shape
        # total of all pixels
        p  = np.sum(image,dtype=float) # float avoids integer overflow

        # find the centroid
        hh = np.arange(h,dtype=float) # float avoids integer overflow
        vv = np.arange(v,dtype=float) # ditto
        xc = np.sum(np.dot(image,hh))/p
        yc = np.sum(np.dot(image.T,vv))/p

        # find the variances
        hs = hh-xc
        vs = vv-yc
        xx = np.sum(np.dot(image,hs**2))/p
        xy = np.dot(np.dot(image.T,vs),hs)/p
        yy = np.sum(np.dot(image.T,vs**2))/p

        # the ISO measures
        diff = xx-yy
        summ = xx+yy
 
        disc = np.sign(diff)*np.sqrt(diff**2)

        dx = 2.0*np.sqrt(2)*np.sqrt(summ+disc)
        dy = 2.0*np.sqrt(2)*np.sqrt(summ-disc)

        phi = 0.5 * np.arctan2(2*xy,diff) # negative because top of matrix is zero
        return xc, yc, dx, dy, phi
    
def beam_size_rot(image):
        """
        Determines laser beam center, diameters, and tilt of beam according to ISO 11146 standard.

        Parameters
        ----------
        image : array_like
            should be a monochrome two-dimensional array

        Returns
        -------
        xc : int
            horizontal center of beam
        yc : int
            vertical center of beam
        dx : float
            horizontal diameter of beam
        dy : float
            vertical diameter of beam
        phi: float
            angle that beam is rotated (about center) from the horizontal axis
        """

        # force negative values to 0
        image = image.clip(min=0)

        #vertical and horizontal dimensions of beam
        v,h = image.shape
        # total of all pixels
        p  = np.sum(image,dtype=float) # float avoids integer overflow

        # find the centroid
        hh = np.arange(h,dtype=float) # float avoids integer overflow
        vv = np.arange(v,dtype=float) # ditto
        xc = np.sum(np.dot(image,hh))/p
        yc = np.sum(np.dot(image.T,vv))/p
        print('xc is ' + str(xc))
        print('yc is ' + str(yc))

        # find the variances
        hs = hh-xc # horizontal axis
        vs = vv-yc # vertical axis
        print('hs is ' + str(hs))
        print('vs is ' + str(vs))
        xx = np.sum(np.dot(image,hs**2))/p # variance in x
        #xx = np.dot(np.dot(image,hs),hs)/p
        
        print(np.dot(image,hs**2))
        xy = np.dot(np.dot(image.T,vs),hs)/p
        yy = np.sum(np.dot(image.T,vs**2))/p
        print('xx is' + str(xx))
        print('xy is' + str(xy))
        print('yy is' + str(yy))
        # the ISO measures
        diff = xx-yy
        summ = xx+yy


        # somehow different from beam_size()
        disc = np.sign(diff)*np.sqrt(diff**2 + 4*(xy)**2)
        print('diff is' + str(diff))
        print('summ is' + str(summ))
        print('disc is' + str(disc))
        dx = 2.0*np.sqrt(2)*np.sqrt(summ+disc)
        dy = 2.0*np.sqrt(2)*np.sqrt(summ-disc)
        print('dx is' + str(dx))
        print('dy is' + str(dy))
        phi = 0.5 * np.arctan2(2*xy,diff) # negative because top of matrix is zero
        return xc, yc, dx, dy, phi
    
    
    
#runAnalysis(['/Volumes/KINGSTON/20220719/20220718-1617-P5-405BPM-FC2-PCSide-0/270.0mm.tif', '/Volumes/KINGSTON/20220719/20220718-1617-P5-405BPM-FC2-PCSide-0/Bkg-270.0mm.tif'])