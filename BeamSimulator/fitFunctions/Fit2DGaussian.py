# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:46:18 2015

@author: Lothar
"""

import scipy.optimize
import numpy as np

def gauss1d(r,r0,A,w,c):
    return A*np.exp(-2*(r-r0)**2/w**2)+c

def gaussian2D(height, center_x, center_y, width_x, width_y, rotation, offset):
    """Returns a gaussian function with the given parameters"""
    width_x = float(width_x)
    width_y = float(width_y)

    rotation = np.deg2rad(rotation)
    center_x_tmp = center_x * np.cos(rotation) - center_y * np.sin(rotation)
    center_y_tmp = center_x * np.sin(rotation) + center_y * np.cos(rotation)

    center_x=center_x_tmp
    center_y=center_y_tmp        
        
    def rotgauss(x,y):
        xp = x * np.cos(rotation) - y * np.sin(rotation)
        yp = x * np.sin(rotation) + y * np.cos(rotation)
        g = height*np.exp(
            -2*(((center_x-xp)/width_x)**2+
                ((center_y-yp)/width_y)**2)) + offset
        return g
    return rotgauss
        
def gaussian2DRotFixed(height, center_x, center_y, width_x, width_y, offset):
    # rotation angle is a fixed 0 degrees
    return gaussian2D(height, center_x, center_y, width_x, width_y, 0.1, offset)
    #return gaussian2D(height, center_x, center_y, width_x, width_y, 23.58, offset)
    
def moments(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    X = np.arange(0, data.shape[0])
    Y = np.arange(0, data.shape[1])   
    xint = data.sum(axis=0)
    yint = data.sum(axis=1)
    x = np.sum(X*yint)/np.sum(yint)
    y = np.sum(Y*xint)/np.sum(yint)
    col = data[:, int(y)]
    width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())
    row = data[int(x), :]
    width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())
    height = data.max()
    return height, x, y, width_x, width_y, 0.0, 0.0

def momentsRotFixed(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    X = np.arange(0, data.shape[0])
    Y = np.arange(0, data.shape[1])   
    xint = data.sum(axis=0)
    yint = data.sum(axis=1)
    x = np.sum(X*yint)/np.sum(yint)
    y = np.sum(Y*xint)/np.sum(yint)
    col = data[:, int(y)]
    width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())/2.2
    #row = data[int(x), :]
    #width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())/2.2
    height = data.max()
        
    maxrow=data[:,int(y)].argmax()
    maxcol=data[maxrow,:].argmax()
    xvals = np.arange(0,data.shape[1])
    popt, pcov = scipy.optimize.curve_fit(gauss1d, xvals, data[maxrow,:],(x,height,width_x,data[0][0]))
    
    return popt[1]/1.03, maxrow, maxcol,  1.05*popt[2], 1.05*popt[2], popt[3]
    
def fitGaussian2D(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction = lambda p: np.ravel(gaussian2D(*p)(*np.indices(data.shape)) - data)
    #p, pcov, tmp1, tmp2, tmp3 = scipy.optimize.leastsq(errorfunction, params, full_output=True)
    result = scipy.optimize.least_squares(errorfunction, params, bounds=([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, 0, -np.inf], [np.inf, np.inf, np.inf, np.inf, np.inf, 360, np.inf]))
    p = result.x
    J = result.jac
    pcov = np.linalg.inv(J.T.dot(J))
    
    #calculate errors
    s_sq = (errorfunction(p)**2).sum()/(len(data)-len(p))
    pcov = pcov*s_sq
    error = []
    for i in range(len(p)):
        error.append(np.abs(pcov[i][i])**0.5)
    
    #print("Param:", p)
    #print("Error:", error)
    
    # make rotation angle positive
    x = p[-2]%360
    if x<0: x=x+360
    
    #print(x)
    return p, error

def fitGaussian2DRotFixed(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = momentsRotFixed(data)
    errorfunction = lambda p: np.ravel(gaussian2DRotFixed(*p)(*np.indices(data.shape)) - data)
    # p, success = scipy.optimize.leastsq(errorfunction, params)

    result = scipy.optimize.least_squares(errorfunction, params, bounds=([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf], [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf]))
    p = result.x
    J = result.jac
    pcov = np.linalg.inv(J.T.dot(J))
    
    #calculate errors
    s_sq = (errorfunction(p)**2).sum()/(len(data)-len(p))
    pcov = pcov*s_sq
    error = []
    for i in range(len(p)):
        error.append(np.abs(pcov[i][i])**0.5)

    return p, error