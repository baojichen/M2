# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 14:52:14 2021

@author: Florian Egli
"""

from msilib.schema import File
import sys
import os
import matplotlib.pyplot as plt
import scipy.optimize
from math import pi
import numpy as np
import pandas as pd

def wz(z,z0,w0,lam):
    #return (z+z0)*lam/w0/pi    
    zR=pi*w0**2/lam
    return w0*np.sqrt(1+((z-z0)/zR)**2)

def wzFit(zList,wList, lam):
    wz_fit = lambda z, z0, w0: wz(z,z0,w0,lam)
    print(zList)
    print(wList)
    # maybe ~305 would be a better initial guess for z0
    #popt, pcov = scipy.optimize.curve_fit(wz_fit, zList, wList,(-15,1e-3))
    popt, pcov = scipy.optimize.curve_fit(wz_fit, zList, wList,(0,1e-3))
    perr = np.sqrt(np.diag(pcov))
    
    wfit = lambda x: wz(x, popt[0], popt[1], lam)
    w0=popt[1]
    w0Err=perr[1]
    z0=popt[0]
    z0Err=perr[0]
    
    return wfit, w0, w0Err, z0, z0Err
    
def wzFit2(zList,wList, lam):
    wz_fit = lambda z, z0, w0,a: w0*z*z+z0*z+a
    print(zList)
    print(wList)
    # maybe ~305 would be a better initial guess for z0
    #popt, pcov = scipy.optimize.curve_fit(wz_fit, zList, wList,(-15,1e-3))
    popt, pcov = scipy.optimize.curve_fit(wz_fit, zList, wList,(0.04,1e-3,2))
    perr = np.sqrt(np.diag(pcov))
    
    wfit = lambda z: popt[1]*z*z+popt[0]*z+popt[2]
    w0=popt[1]
    w0Err=perr[1]
    z0=popt[0]
    z0Err=perr[0]
    
    return wfit, w0, w0Err, z0, z0Err

def fitMFD(dataFolder, pixelSize, lam, title=""):
    gauss1DFile = dataFolder+"Gauss1DAnalysis/analysisGauss1D.csv"
    gauss2DFile = dataFolder+"Gauss2DAnalysis/analysisGauss2D.csv"
    secondMomentsFile = dataFolder+"SecondMomentsAnalysis/analysisSecondMoments.csv"
    
    mainTitle=title
    
    dfGauss1D = pd.read_csv(gauss1DFile)
    dfGauss2D = pd.read_csv(gauss2DFile)
    dfSecondMoments = pd.read_csv(secondMomentsFile)
    
    zList = dfGauss1D["StagePos_mm"]
    wxGauss1D = dfGauss1D["w_px_maj"]*pixelSize
    wyGauss1D = dfGauss1D["w_px_min"]*pixelSize
    wxGauss2D = dfGauss2D["wx_px"]*pixelSize
    wyGauss2D = dfGauss2D["wy_px"]*pixelSize
    angleGauss = dfGauss2D["alpha"]%90
    wxMoments = dfSecondMoments["wx_px"]*pixelSize
    wyMoments = dfSecondMoments["wy_px"]*pixelSize
    angleMoments = dfSecondMoments["alpha"]
    
    resultWx = plotW(zList, wxGauss1D, wxGauss2D, wxMoments, lam, "WxPlot", dataFolder)
    resultWx.index.name = "Method"
    resultWx.to_csv(dataFolder+"WxFit.csv", index=True)
    
    resultWy = plotW(zList, wyGauss1D, wyGauss2D, wyMoments, lam, "WyPlot", dataFolder)
    resultWy.index.name = "Method"
    resultWy.to_csv(dataFolder+"WyFit.csv", index=True)
    
    plotAngle(zList, angleGauss, angleMoments, "Rotation Angles", dataFolder)
def fitMFD1DAndD4S(dataFolder, pixelSize, lam, title=""):
    gauss1DFile = dataFolder+"Gauss1DAnalysis/analysisGauss1D.csv"
    secondMomentsFile = dataFolder+"SecondMomentsAnalysis/analysisSecondMoments.csv"
    
    mainTitle=title
    
    dfGauss1D = pd.read_csv(gauss1DFile)
    dfSecondMoments = pd.read_csv(secondMomentsFile)
    
    zList = dfGauss1D["StagePos_mm"]
    wxGauss1D = dfGauss1D["w_px_maj"]*pixelSize
    wyGauss1D = dfGauss1D["w_px_min"]*pixelSize
    wxGauss1DErr = dfGauss1D["wErr_maj"]*pixelSize
    wyGauss1DErr = dfGauss1D["wErr_min"]*pixelSize
    wxMoments = dfSecondMoments["wx_px"]*pixelSize
    wyMoments = dfSecondMoments["wy_px"]*pixelSize
    angleMoments = dfSecondMoments["alpha"]
    
    resultWx = plotW(zList, wxGauss1D, wxGauss1DErr, wxMoments, lam, "WxPlot", dataFolder)
    
    resultWx.index.name = "Method"
    resultWx.to_csv(dataFolder+"WxFit.csv", index=True)
    
    resultWy = plotW(zList, wyGauss1D, wyGauss1DErr, wyMoments, lam, "WyPlot", dataFolder)
    resultWy.index.name = "Method"
    resultWy.to_csv(dataFolder+"WyFit.csv", index=True)
    
    
def fitMFD1D(dataFolder, pixelSize, lam, title=""):
    gauss1DFile = dataFolder+"Gauss1DAnalysis/analysisGauss1D.csv"
    
    mainTitle=title
    
    dfGauss1D = pd.read_csv(gauss1DFile)
   
    zList = dfGauss1D["StagePos_mm"]
    wxGauss1D = dfGauss1D["w_px_maj"]*pixelSize
    wyGauss1D = dfGauss1D["w_px_min"]*pixelSize
    
    resultWx = plotW1D(zList, wxGauss1D, lam, "WxPlot", dataFolder)
    resultWx.index.name = "Method"
    resultWx.to_csv(dataFolder+"WxFit.csv", index=True)
    
    resultWy = plotW1D(zList, wyGauss1D, lam, "WyPlot", dataFolder)
    resultWy.index.name = "Method"
    resultWy.to_csv(dataFolder+"WyFit.csv", index=True)
    
    # plotAngle(zList, angleGauss, angleMoments, "Rotation Angles", dataFolder)
    
# create plots of fitted width vs z
def plotW(zList, wGauss1D, wGauss2D, wMoments, lam, title, outputFolder):
    
    Gauss1Dfit, Gauss1Dw0, Gauss1Dw0Err, Gauss1Dz0, Gauss1Dz0Err = wzFit(zList, wGauss1D, lam)
    Gauss2Dfit, Gauss2Dw0, Gauss2Dw0Err, Gauss2Dz0, Gauss2Dz0Err = wzFit2(zList, wGauss2D, lam)
    Momentsfit, Momentsw0, Momentsw0Err, Momentsz0, Momentsz0Err = wzFit(zList, wMoments, lam)
    
    result = pd.DataFrame()
    Gauss1DResults = {"w0_mm":Gauss1Dw0,
                      "w0Err_mm": Gauss1Dw0Err,
                      "z0_mm":Gauss1Dz0,
                      "z0Err_mm": Gauss1Dz0Err}
    
    Gauss2DResults = {"w0_mm":Gauss2Dw0,
                      "w0Err_mm": Gauss2Dw0Err,
                      "z0_mm":Gauss2Dz0,
                      "z0Err_mm": Gauss2Dz0Err}

    MomentsResults = {"w0_mm":Momentsw0,
                      "w0Err_mm": Momentsw0Err,
                      "z0_mm": Momentsz0,
                      "z0Err_mm": Momentsz0Err} 
    
    data = {"Gauss1D": Gauss1DResults,
            "Gauss2D": Gauss2DResults,
            "Moments": MomentsResults}
    
    result = pd.DataFrame.from_dict(data, orient="index")
    with open(r'C:\Users\baojichen\Desktop\BeamSimulator\BeamSimulator\theory\FiberMode\lam405\a.txt', 'a') as the_file:
        the_file.write(f'{outputFolder[27:32]}\t{outputFolder[35:42]}\t{Gauss1Dw0}\t{Gauss1Dw0Err}\t{Gauss1Dz0}\t{Gauss1Dz0Err}\t{Gauss2Dw0}\t{Gauss2Dw0Err}\t{Gauss2Dz0}\t{Gauss2Dz0Err}\t{Momentsw0}\t{Momentsw0Err}\t{Momentsz0}\t{Momentsz0Err}\n')
    with open(r'C:\Users\baojichen\Desktop\BeamSimulator\BeamSimulator\theory\FiberMode\lam405\b.txt', 'a') as the_file1:
        the_file1.write(f'{outputFolder[27:32]}\t{outputFolder[35:42]}\t')
        for a2 in wGauss2D:
            the_file1.write(f'{a2}\t')
        the_file1.write(f'\n')
        # plot actual data
    plt.plot(zList,wGauss1D, marker="x", linestyle='None',color='b', label="Gauss1D")
    plt.plot(zList,wGauss2D, marker="x", linestyle='None',color='r', label="Gauss2D")
    plt.plot(zList,wMoments, marker="x", linestyle='None',color='g', label="SecondMoments")
    ax = plt.gca()
    plt.text(0.5, 0.6,
             f"Gauss1D: $w = {Gauss1Dw0*1000:.3f} ({Gauss1Dw0Err*1e6:.0f}) \,\mu m$\n"+
             f"Gauss2D: $w = {Gauss2Dw0*1000:.3f} ({Gauss1Dw0Err*1e6:.0f})  \,\mu m$\n"+
             f"Moments: $w = {Momentsw0*1000:.3f} ({Momentsw0Err*1e6:.0f})  \,\mu m$",
             transform=ax.transAxes, fontsize=12)
    
    xvals = np.linspace(min(zList)-1, max(zList)+2, 500)

    # plot fitted data
    plt.plot(xvals,Gauss1Dfit(xvals), color='b')
    plt.plot(xvals,Gauss2Dfit(xvals), color='r')
    plt.plot(xvals,Momentsfit(xvals), color='g')
    

    plt.xlabel('$z$ [mm]')
    plt.ylabel('Fitted width $w(z)=w_0 \sqrt{1+((z-z_0)/z_R)^2}$ [mm]')
    
    plt.title(title)
    plt.legend()
    
    plt.xlim([min(zList)-5, max(zList)+5])
    plt.savefig(outputFolder+title+".png")
    
    plt.close()


    # plot residuals on separate plot
    residuals1D = wGauss1D - Gauss1Dfit(zList)
    residuals2D = wGauss2D - Gauss2Dfit(zList)
    residualsMoments = wMoments - Momentsfit(zList)

    plt.plot(zList, residuals1D, "bo", label="Gauss1D")
    plt.plot(zList, residuals2D, "ro", label="Gauss2D")
    plt.plot(zList, residualsMoments, "go", label="SecondMoments")

    plt.xlabel('$z$ [mm]')
    plt.ylabel('Residuals to linear fit for width [mm]')
    
    plt.title(title)
    plt.legend()
    
    plt.xlim([min(zList)-5, max(zList)+5])
    plt.savefig(outputFolder+title+"_residuals.png")
    
    plt.close()
    
    #return Gauss1DResults, Gauss2DResults, MomentsResults
    return result

# create plots of fitted width vs z
def plotW1D(zList, wGauss1D, lam, title, outputFolder):
    
    Gauss1Dfit, Gauss1Dw0, Gauss1Dw0Err, Gauss1Dz0, Gauss1Dz0Err = wzFit(zList, wGauss1D, lam)
    
    result = pd.DataFrame()
    Gauss1DResults = {"w0_mm":Gauss1Dw0,
                      "w0Err_mm": Gauss1Dw0Err,
                      "z0_mm":Gauss1Dz0,
                      "z0Err_mm": Gauss1Dz0Err}
    
    data = {"Gauss1D": Gauss1DResults}
    
    result = pd.DataFrame.from_dict(data, orient="index")
    
    # plot actual data
    plt.plot(zList,wGauss1D, marker="x", linestyle='None',color='b', label="Gauss1D")
    ax = plt.gca()
    plt.text(0.5, 0.6,
             f"Gauss1D: $w = {Gauss1Dw0*1000:.3f} ({Gauss1Dw0Err*1e6:.0f}) \,\mu m$\n",
             transform=ax.transAxes, fontsize=12)
    
    xvals = np.linspace(min(zList)-1, max(zList)+2, 500)

    # plot fitted data
    plt.plot(xvals,Gauss1Dfit(xvals), color='b')
    

    plt.xlabel('$z$ [mm]')
    plt.ylabel('Fitted width $w(z)=w_0 \sqrt{1+((z-z_0)/z_R)^2}$ [mm]')
    
    plt.title(title)
    plt.legend()
    
    plt.xlim([min(zList)-5, max(zList)+5])
    plt.savefig(outputFolder+title+".png")
    
    plt.close()


    # plot residuals on separate plot
    residuals1D = wGauss1D - Gauss1Dfit(zList)

    plt.plot(zList, residuals1D, "bo", label="Gauss1D")

    plt.xlabel('$z$ [mm]')
    plt.ylabel('Residuals to linear fit for width [mm]')
    
    plt.title(title)
    plt.legend()
    
    plt.xlim([min(zList)-5, max(zList)+5])
    plt.savefig(outputFolder+title+"_residuals.png")
    
    plt.close()
    
    #return Gauss1DResults, Gauss2DResults, MomentsResults
    return result
    

def plotAngle(zList, angleGauss, angleMoments, title, outputFolder):
    
    plt.plot(zList, angleGauss, marker="x", linestyle='None',color='b', label="Gauss")
    plt.plot(zList, angleMoments, marker="x", linestyle='None',color='r', label="Moments")
    
    plt.xlabel('$z$ [mm]')
    plt.ylabel('Rotation angle of the major/minor axes [deg]')
    plt.title(title)
    plt.xlim([min(zList)-5, max(zList)+5])
    plt.ylim([min(min(angleGauss), min(angleMoments))-5,max(max(angleGauss), max(angleMoments))+5])
    ax = plt.gca()
    plt.text(0.5, 0.7, f'Gauss Mean: {(sum(angleGauss)/len(angleGauss)):.2f} deg\n'+
             f'Moments Mean: {(sum(angleMoments)/len(angleMoments)):.2f} deg',
             transform=ax.transAxes, fontsize=12)
    plt.legend()
    
    plt.savefig(outputFolder +'rotation angles.png')
    plt.close()


if __name__=="__main__":
    dataFolder = sys.argv[1]+"/"
    
    #both should be in mm so that the output is im mm as well
    lam = 405e-6
    pixelSize = 5.5e-3
    
    fitMFD(dataFolder, pixelSize, lam)
    
    #for refitting the theory folder
    #for folder in os.listdir(dataFolder):
    #    fitMFD(dataFolder+folder+"/", pixelSize, lam)