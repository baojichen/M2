#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 15:31:52 2022

@author: Cotton
"""


import os
directories = ['/Users/Cotton/Documents/fiberanalysis']


for directory in directories:
    folders = [d for d in os.listdir(directory) if os.path.isdir(os.path.join(directory, d))]
    for folder in folders:
        if folder.startswith('20220713'): 
            #if not os.path.isfile(os.path.join(directory, folder, 'WxPlot.png')):
            print('doing ' + folder)
            #os.system("python ImageFit.py " + str(os.path.join(directory,folder)))
            os.system("python compareToTheory.py " + str(os.path.join(directory,folder)))