# Fiber Analysis Project

The Fiber Analysis Project can be devided into two parts:

1. The Simulation
2. The Analysis and Comparison.

## The Simulation

The simulation is realized in the script ``theoryImageCreator.py``.
The parameters of the simulation can be adjusted in the section that follows ``if __name__ == ""__main__":``.

This Script creates images that show the cross section of the simulated beam at different distances from the fiber end. These have the same structure as the ones taken by the beam camera in the experimental setup.

It also call the analysis script for the simulated images and saves the necessary data for the w0-NA plots.

## The Analysis and Comparison

The Analysis is realized in ``ImageFit.py``. A folder of experimental data can be analyzed with
``python ImageFit.py path\to\folder``, which results in csv files with the analyzed data.

One can then call ``python compareToTheory.py path\to\folder`` or ``python compareToFiberMode.py path\to\folder`` to get a comparison between the experimental data and the simulation.
In those scripts also the w0-NA plots are produced.

The script ``compareToTheory.py`` corresponds to the method described up to and including chapter 4 of the "Fiber Report", ``compareToFiberMode.py`` is the script described in chapter 5.