#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 16:14:36 2022

@author: Tian
"""

import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

directory = '/Volumes/KINGSTON'

ExpTime = []
Gauss1D = []
Gauss2D = []
Moments = []
Gauss1Derr = []
Gauss2Derr = []
Momentserr = []

for folder in os.listdir(directory):
    if folder.startswith('20220713') and folder.endswith(('1', '2', '3', '4', '5')): 
        # plot exposure times
        metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')

        exptime = metadata['ExposureTime'][0] # get first exposure time
        ExpTime.append(exptime)
        
        xfit = pd.read_csv(os.path.join(directory, folder, 'WyFit.csv'), index_col=0)
        Gauss1D.append(xfit['w0_mm']['Gauss1D'])
        Gauss2D.append(xfit['w0_mm']['Gauss2D'])
        Moments.append(xfit['w0_mm']['Moments'])
        
        Gauss1Derr.append(xfit['w0Err_mm']['Gauss1D'])
        Gauss2Derr.append(xfit['w0Err_mm']['Gauss2D'])
        Momentserr.append(xfit['w0Err_mm']['Moments'])
        #plt.plot(metadata['StagePos'], metadata['ExposureTime'], label=label1, marker='x')
        

plt.errorbar(ExpTime, Gauss1D, fmt='o', yerr=Gauss1Derr, label='Gauss1D')
plt.errorbar(ExpTime, Gauss2D, fmt='o', yerr=Gauss2Derr, label='Gauss2D')
plt.errorbar(ExpTime, Moments, fmt='o', yerr=Momentserr, label='Moments')

plt.ylabel('Fitted $w0$ [mm]')
plt.xlabel('270 mm Exposure Time [ms]')

plt.title('20220713 P5-405BPM-FC2-PCSide WyFit')
plt.legend()
plt.show()