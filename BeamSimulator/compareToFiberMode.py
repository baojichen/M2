# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 13:50:31 2022

@author: Florian Egli
"""

import sys
import os
import time

import matplotlib.pyplot as plt
from fitFunctions.Fit2DGaussian import gauss1d
import numpy as np
import pandas as pd
from fitFunctions.Analysis import fitImageGauss2D, fitImageGauss1DCuts
from theoryImageCreator import *

from scipy.interpolate import interp1d
import scipy.optimize

from PIL import Image
    
def compareToFiberMode_Fit(dataFile, method, dataBkgFile, th_lam, th_core, th_NA, outputFolder):
    
    #theoryFile = f"./theory/FiberMode/lam{th_lam}/a{th_core}/NA{th_NA}/ThisPartDoesNotMatter.tif"
    
    try:
        imObj = Image.open(dataFile)
        #this line fails if tif is LZW-compressed
        im = np.array(imObj)
        im = im.astype(np.int64)
        
        bkgObj = Image.open(dataBkgFile)
        #this line fails if tif is LZW-compressed
        bkg = np.array(bkgObj)
        bkg = bkg.astype(np.int64)
        
    except Exception as e:
        print("Failed to open dataFile")
        print(e)
        return
    
    axis = "maj" #min/maj
    if axis == "maj":
        ax = "x"
    else:
        ax = "y"
    
    # get g2d fitting data: wx_px, wy_px, x_px, y_px etc
    g2d_data = fitImageGauss2D(im, bkg)
    #g2d_file = "/".join(dataFile.split("/")[:-1])+"/Gauss2DAnalysis/analysisGauss2D.csv"
    #g2d_file_data = pd.read_csv(g2d_file, index_col="Filename")
    #g2d_data = g2d_file_data.loc[dataFile.split("/")[-1]]

    g1d_data = fitImageGauss1DCuts(im, bkg, g2d_data)
    f1d = lambda x: gauss1d(x, g1d_data[f"r0_px_{axis}"], g1d_data[f"PeakValue_{axis}"], g1d_data[f"w_px_{axis}"], g1d_data[f"c_px_{axis}"])
    
    xvals_data = g1d_data.pop(f"Xvals_{axis}")
    yvals_data = g1d_data.pop(f"Yvals_{axis}")
    r0_data = g1d_data[f"r0_px_{axis}"]
    
    #Calculate Diffraction Integral for fit
    lam = float(th_lam)*10**-9
    a= float(th_core)*10**-6
    NA = float(th_NA)
    #Position: From z0 theory + data
    MFD_data = pd.read_csv("/".join(dataFile.split("/")[:-1])+f"/W{ax}Fit.csv")
    #MFD_theory = pd.read_csv("/".join(theoryFile.split("/")[:-1])+f"/W{ax}Fit.csv")
    
    #Data Says z0 is at 300-MFD_data["z0_mm"]
    #theory, where we know that z0=0 says z0=MFD_theory["z0_mm"]-> We need to correct for that
    #The real z0 for the data is at 300-MFD_data["z0_mm"] - MFD_theory["z0_mm"]
    #position = 300-MFD_data["z0_mm"]-MFD_theory["z0_mm"]
    
    #Different Fit Methods give different z0
    position = -(position.loc[method])
    stage_pos = 300-float((dataFile.split("/")[-1]).split("mm")[0])
    position = (position+stage_pos)*10**-3
    
    #position = 25.0e-3# (28.3 - (25.0-7.0))*1e-3
    #position = 28.3e-3
    position = position + 0.0e-3
    print("Position", position)
    
    intlim = 50*a
    #N = 1000
    N=500
    kT, gamma, s = DetermFiberMode(lam, a, NA)
        
    dx = 11.3e-3/2048
    r = np.linspace(-2*np.sqrt(2)*11.3e-3, 2*np.sqrt(2)*11.3e-3, 10*2048*5)
    
    E0 = lambda r: EFieldFiberMode(r, a, kT, gamma, s)
    E = EBeug(E0, r ,intlim, position, N, lam)
    E2 = np.abs(E)**2
    #For proper scale
    E2 = E2/E2.max()*255
    
    I = scipy.interpolate.interp1d(r, E2, "cubic")
    
    #x coordinates start at 0
    theorySpline = lambda x, a,d, c: a/255*I((x-d)*11.3e-3/2048)+c
    #theorySpline = lambda x, a,d, c: a/255*I((x-d)*11.3e-3/1024)+c #DataRay1024
    
    popt, pcov = scipy.optimize.curve_fit(theorySpline, xvals_data, yvals_data, p0=[g1d_data[f"PeakValue_{axis}"], g1d_data[f"r0_px_{axis}"], 0])
    theoryFit = lambda x: theorySpline(x, popt[0], popt[1], popt[2])
    #theoryFit = lambda x: theorySpline(x, g1d_data["PeakValue_maj"], popt[1])
    print(popt)
    
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    
    file = outputFolder + (dataFile.split("/")[-1]).split(".tif")[0]+"_diffInt.png"
    file_err = outputFolder + (dataFile.split("/")[-1]).split(".tif")[0]+"_diffInt_residuals.png"
    
    plt.clf()
    plt.plot(xvals_data, yvals_data, label="data")
    plt.plot(xvals_data, f1d(xvals_data), label="g1d_fit")
    plt.plot(xvals_data, theoryFit(xvals_data), label="TheoryFit")
    #plt.plot(xvals_theory[1:-1]+(r0_data-r0_theory), theorySpline(xvals_theory[1:-1]+(r0_data-r0_theory))*g1d_data[f"PeakValue_{axis}"]/255, label="theory spline")
    #even worse:
    #plt.plot(xvals_theory+(r0_data-r0_theory), yvals_theory*g2d_data["PeakValue"]/255, label="FiberMode")
    
    global methods
    plt.title(dataFile.split("/")[-1]+f"\n1D cuts for a={th_core}, NA={th_NA}, {methods[i]}")
    
    
    plt.legend()
    plt.savefig(file)
    #plt.show()
    
    plt.clf()
    plt.plot(xvals_data, yvals_data-f1d(xvals_data), "b", label="Fitted 1D Gaussian")
    plt.plot(xvals_data, yvals_data-theoryFit(xvals_data), "r", label="TheoryFit")
    plt.title((dataFile.split("/")[-1])+f"\nResiduals for a={th_core}, NA={th_NA}, {methods[i]}")
    plt.legend()
    plt.savefig(file_err)
    #plt.show()
    
if __name__ == "__main__":
    positions = [270.0, 280.0, 295.0]
    #pos = 25.0
    dataFolder = sys.argv[1]+"/"
    for pos in positions:
        dataFile = dataFolder + f"{pos:.1f}mm.tif"
        bkgFile = dataFolder + f'Bkg-{pos:.1f}mm.tif'
        core= "1.600"
        lam= "405"
        #      G1D      G2D    D4Sig
        NA = ["0.10000", "0.10000"]
        #NA = ["0.09581", "0.09647"]#, "0.09970"] #Snapshot
        #NA = ["0.09167", "0.08989"] #CinMeas
        methods = ["G1D", "G2D"]#, "D4Sig"] # MUST follow order in the WxFit.csv files
        #methods = ["G1D_Spec", "G2D_Spec", "D4Sig_Spec"]
        for i in range(len(methods)):
            t=time.time()
            #outputFolder = dataFolder+"CompareToFiberMode_CinogyMeas/"+methods[i]+"/"
            outputFolder = dataFolder+"CompareToFiberMode/"+methods[i]+"/"
            
            compareToFiberMode_Fit(dataFile, i, bkgFile, lam, core, NA[i], outputFolder)
            print("took", time.time()-t, "s")