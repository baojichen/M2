import sys
import os
import time

import matplotlib.pyplot as plt
from fitFunctions.Fit2DGaussian import gauss1d
import numpy as np
import pandas as pd
from fitFunctions.Analysis import fitImageGauss2D, fitImageGauss1DCuts
from theoryImageCreator import *

from scipy.interpolate import interp1d
import scipy.optimize

from PIL import Image

cache={}

def FiberMode(a,NA,position):
    global cache
    if((a, NA, position) in cache):
        return cache[(a, NA, position)]
    lam=405e-9
    intlim = 50*a
    N=500
    kT, gamma, s = DetermFiberMode(lam, a, NA)
        
    r = np.linspace(-2*np.sqrt(2)*3705.6e-6, 2*np.sqrt(2)*3705.6e-6, 10*2048*5)
    
    E0 = lambda r: EFieldFiberMode(r, a, kT, gamma, s)
    E = EBeug(E0, r ,intlim, position, N, lam)
    E2 = np.abs(E)**2
    #For proper scale
    E2 = E2/E2.max()*255
    I = scipy.interpolate.interp1d(r, E2)
    cache[(a, NA, position)]=I
    return I
