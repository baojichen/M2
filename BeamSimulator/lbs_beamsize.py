#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 13:51:57 2022

@author: Cotton
"""
import sys
sys.path.append('/Users/Cotton/Documents/fiberanalysis/fitFunctions/') # so spyder can find the module when debugging
import traceback #debug

import numpy as np
import scipy.optimize
import scipy.interpolate
import copy
import cv2
from PIL import Image
import laserbeamsize as lbs
import matplotlib.pyplot as plt

file = '/Users/Cotton/Documents/fiberanalysis/20220627-WinCam-S405-XP-Custom-Muc-084/300.0mm.tiff'
bkgFile = '/Users/Cotton/Documents/fiberanalysis/20220627-WinCam-S405-XP-Custom-Muc-084/Bkg.tiff'

imObj = Image.open(file)
#this line fails if tif is LZW-compressed
im = np.array(imObj)
im = im.astype(np.int64)

bkgObj = Image.open(bkgFile)
#this line fails if tif is LZW-compressed
bkgIm = np.array(bkgObj)
bkgIm = bkgIm.astype(np.int64)

image = im
# force negative values to 0
image = image.clip(min=0)

x, y, dx, dy, phi = lbs.beam_size(image)

print("The center of the beam ellipse is at (%.0f, %.0f)" % (x,y))
print("The ellipse diameter (closest to horizontal) is %.0f pixels" % dx)
print("The ellipse diameter (closest to   vertical) is %.0f pixels" % dy)
print("The ellipse is rotated %.0f° ccw from horizontal" % (phi*180/3.1416))

lbs.beam_size_plot(image)
plt.show()