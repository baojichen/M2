#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 11:04:29 2022

@author: Cotton
"""


import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from PIL import Image



def applyNonlinearity(im, alpha, n):
    im = im * (1-alpha*np.power(im/4096,n) )
    im = np.rint(im) # round to nearest integer
    return im

folder = '/Users/Cotton/Downloads/20220811-PMC400RGB-IDS/20220811-1537-PMC-400RGB-1'

wincam_file = '/Users/Cotton/Downloads/20220816-PMC400RGB-WinCam/20220816-WinCam-PMC-400RGB-1/280.0mm.tiff'
cinogy_file = '/Users/Cotton/Downloads/20220811-PMC400RGB-Cinogy/20220811-PMC-400RGB-1/280.0mm.tif'

# grab image
im = Image.open('/Users/Cotton/Downloads/20220811-PMC400RGB-IDS/20220811-1537-PMC-400RGB-1/280.0mm.tif')
im = np.asarray(im)

im_wincam = Image.open(wincam_file)
im_wincam = np.asarray(im_wincam)
im_cinogy = Image.open(cinogy_file)
im_cinogy = np.asarray(im_cinogy)

major_c, minor_c = 1078.216164, 1073.198116
major_w, minor_w = 1041.845075, 1018.152374

major = 959.6891706		# x coord of max, y cut
minor = 1042.68261 # y coord of max, x cut

plt.imshow(im)
plt.plot(major,minor, 'o')

ycut = im[:,int(major)]
xcut = im[int(minor),:]
x = np.arange(len(xcut))
plt.figure()

for a in [0, 0.02, 0.05, 0.08, 0.12, 0.15]:
    im_nonlin = applyNonlinearity(xcut, a, 5)
    lab = 'alpha = ' + str(a)
    
    plt.plot(x, im_nonlin, label=lab, marker='.', markersize=1)

#plt.plot(np.arange(2048), im_wincam[int(minor_w), :], marker='.', markersize=1, label='wincam')
#plt.plot(np.arange(2040), im_cinogy[int(minor_c), :], marker='.', markersize=1, label='cinogy')

plt.title('X cuts with nonlinearity for varying alpha \n PMC-400RGB IDS 280mm')
plt.legend()
plt.show()


plt.figure()

for a in [0, 0.02, 0.05, 0.08, 0.12, 0.15]:
    im_nonlin = applyNonlinearity(ycut, a, 5)
    lab = 'alpha = ' + str(a)
    
    plt.plot(x, im_nonlin, label=lab, marker='.', markersize=1)

#plt.plot(np.arange(2048), im_wincam[:, int(major_w)], marker='.', markersize=1, label='wincam')
#plt.plot(np.arange(2040), im_cinogy[:, int(major_c)], marker='.', markersize=1, label='cinogy')

plt.title('Y cuts with nonlinearity for varying alpha \n PMC-400RGB IDS 280mm')
plt.legend()
plt.show()

# plt.plot(wincam['StagePos'], wincam['ExposureTime'], label='WinCam', marker='o')
# plt.plot(cinogy['StagePos'], cinogy['ExposureTime'], label='Cinogy', marker='o')

# plt.xlabel('$z$ [mm]')
# plt.ylabel('Exposure Time [ms]')

# plt.title('Camera Exposure Times \n 20220811 PMC-400RGB-2.8-NA011-1-APC-150-P')
# plt.legend()
# plt.show()


