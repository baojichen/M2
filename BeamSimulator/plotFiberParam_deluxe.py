#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 16:00:58 2022

@author: Cotton
"""


import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from datetime import datetime

directories = ['/Users/Cotton/Downloads/20220818','/Users/Cotton/Downloads/20220819']

FolderName = []
Timestamp = []
ExpTime = []
Angle = []
NA = []
NA1D = []
NA2D= []
NA15a = []
NAErr = []
NADiff = []
Waist = []
WaistErr = []
Radius = []
Type = []
x0 = []
y0 = []
Camera = []




for directory in directories:
    # sort by last number
    folders = [d for d in os.listdir(directory) if os.path.isdir(os.path.join(directory, d))]
    folders.sort(key=lambda s: int(s.split('-')[-1]))
    
    # collect fiber rotated data
    for folder in folders:
        if folder.startswith('202208') and not folder.endswith('csv'): 
            
            FolderName.append(folder)
            
            # metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')
    
            # exptime = metadata['ExposureTime'][0] # get first exposure time
            # ExpTime.append(exptime)
            
            # Timestamp.append(datetime.strptime(metadata['Timestamp'][0], '%Y-%m-%d %H:%M:%S.%f'))
            
            fitdf = pd.read_csv(os.path.join(directory, folder, 'WyFit.csv'))
            fitdf = fitdf.set_index('Method')
            Waist.append(fitdf.loc['Gauss1D']['w0_mm'])
            WaistErr.append(fitdf.loc['Gauss1D']['w0Err_mm'])
            
            # ang = int(folder.split('-')[-2])
            # Angle.append(ang)
            
            # moments = pd.read_csv(os.path.join(directory, folder, 'SecondMomentsAnalysis/analysisSecondMoments.csv'))
            # x0.append(moments[moments['StagePos_mm'] == 270].x_px.values[0])
            # y0.append(moments[moments['StagePos_mm'] == 270].y_px.values[0])
            
            df = pd.read_csv(os.path.join(directory, folder, 'theory/NA_y.csv'))
            
            # get the smallest difference NA value
            # diff = abs(df['G1D_NA'] - df['G2D_NA'])
            # err = diff.min()
            # index = diff.idxmin()
            
            # NAavg = (df['G1D_NA'][index] + df['G2D_NA'][index])/2 # avg NA values
            
            # rad = df['a [um]'][index]
            
            # NA1D.append(df['G1D_NA'][index])
            # NA2D.append(df['G2D_NA'][index])
            NA15a.append(df[df['a [um]'] == 1.5].G1D_NA.values[0])
            NAErr.append(df[df['a [um]'] == 1.5].G1D_NAErr.values[0])
            # NA.append(NAavg)
            # NADiff.append(err)
            # Radius.append(rad)
            Type.append('FiberRotated')
            
            if folder.split('-')[-2] == 'IDS':
                Camera.append('IDS')
            elif folder.split('-')[-2] == 'WinCam':
                Camera.append('WinCam')
            elif folder.split('-')[-2] == 'Cinogy':
                Camera.append('Cinogy')
            
            
            #plt.plot(metadata['StagePos'], metadata['ExposureTime'], label=label1, marker='x')
        
            
            
            
# data = {'Folder Name': FolderName,
#         'Exposure Time': ExpTime,
#         'Timestamp': Timestamp,
#         'NA': NA,
#         'NA1D': NA1D,
#         'NA2D': NA2D,
#         'NA Diff': NADiff,
#         'NA15a': NA15a,
#         'Radius': Radius,
#         # 'Angle': Angle,
#         'x0': x0,
#         'y0': y0,
#         'Type':Type
#         }



# data = {'Folder Name': FolderName,
#         'Exposure Time': ExpTime,
#         'Timestamp': Timestamp,
#         'NA15a': NA15a,
#         'NAErr': NAErr,
#         'Waist': Waist,
#         'WaistErr': WaistErr,
#         #'Radius': Radius,
#         # 'Angle': Angle,
#         'Type':Type
#         }

data = {'Folder Name': FolderName,
        'NA15a': NA15a,
        'NAErr': NAErr,
        'Waist': Waist,
        'WaistErr': WaistErr,
        #'Radius': Radius,
        # 'Angle': Angle,
        'Type':Type,
        'Camera': Camera
        }

df = pd.DataFrame(data)

#fiberrot = df.loc[df['Type']=='FiberRotated']
#imagerot = df.loc[df['Type']=='ImageRotated']


ids = df.loc[df['Camera']=='IDS']
wincam = df.loc[df['Camera']=='WinCam']
cinogy = df.loc[df['Camera']=='Cinogy']

# normalize and scale
# markersizef = (np.array(fiberrot['NA Diff']) / max(fiberrot['NA Diff']) * 40)**2 + 100
#markersizei = (np.array(imagerot['NA Diff']) / max(imagerot['NA Diff']) * 40)**2 + 100

fig, ax = plt.subplots()
#fig.set_size_inches(12, 3)

# plot error accurately with scale
#ax.set_ylim([0.0019, 0.00196]) # must set limits for scaling
ax.set_ylim([0.097, 0.109])

M = ax.transData.get_matrix()
yscale = M[1,1]
#markersizef = yscale*fiberrot['NA Diff']
markersizef = yscale*ids['NAErr']
markersizef = np.clip(markersizef, 3, None) # minimum markersize
markersize2 = yscale*wincam['NAErr']
markersize2 = np.clip(markersize2, 3, None)
markersize3 = yscale*cinogy['NAErr']
markersize3 = np.clip(markersize3, 3, None)
# markersizei = yscale*imagerot['NA Diff']
# markersizei = np.clip(markersizei, 3, None)

#sc = plt.scatter(ExpTime, NA, s=markersize**2 + 100, c=Radius, alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.65)
#sc = plt.scatter(fiberrot['Angle'], fiberrot['NA15a'], s=(markersizef)**2, c=fiberrot['Radius'], alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.7)
#sc = plt.scatter(fiberrot['Angle'], fiberrot['NA15a'], s=(8)**2, alpha = 0.5)
#sc = plt.scatter(fiberrot['x0'], fiberrot['y0'], c=fiberrot['Angle'], alpha = 0.5)
# sc1 = plt.scatter(imagerot['Angle'], imagerot['NA'], s=markersizei**2, c=imagerot['Radius'], marker='s', alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.7)
sc = plt.scatter(np.arange(1, len(ids)+1), ids['NA15a'], s=(markersizef)**2, alpha = 0.5, label='IDS')
sc = plt.scatter(np.arange(1, len(wincam)+1), wincam['NA15a'], s=(markersize2)**2, alpha = 0.5, label='WinCam')
sc = plt.scatter(np.arange(1, len(cinogy)+1), cinogy['NA15a'], s=(markersize3)**2, alpha = 0.5, label='Cinogy')
#fig.autofmt_xdate()
#plt.errorbar(fiberrot['Angle'], fiberrot['NA'], fmt='none', yerr=fiberrot['NA Diff']/2, capsize=3, errorevery=3)
# plt.errorbar(ExpTime, Gauss2D, fmt='o', yerr=Gauss2Derr, label='Gauss2D')
# plt.errorbar(ExpTime, Moments, fmt='o', yerr=Momentserr, label='Moments')

#plt.colorbar(sc)
#plt.text(0, 0.0995, 'markersize = NA error from waist fit ')
#plt.text(14.1, 0.097, 'markersize = diff between NA \n Free rotation')
#plt.text(ax.get_xlim()[0], 0.111, 'markersize = Fit error')
#plt.text(ax.get_xlim()[0], 0.00188, 'markersize = Fit error')

#plt.ylabel('Avg of closest Gauss1D and Gauss2D NA')
plt.ylabel('Gauss1D NA for a=1.5 um')
#plt.ylabel('Gauss1D Fitted beam waist [mm]')
#plt.ylabel('Beam center y0 [px]')

plt.xlabel('Dataset')
#plt.xlabel('Beam center x0 [px]')
#plt.xlabel('270mm Exposure time [ms]')

plt.title('Fiber Parameters vs Cameras (Same Exp) \n Diamond-PM-S405-XP 2 Y Fit')
#plt.title('Beam center at 270 mm while rotating fiber\n 20220727-P5-405BPM-FC2-PCSide')

plt.legend()
plt.show()


df.to_csv(os.path.join(directory, "20220818-df-Diamond-y.csv"))















