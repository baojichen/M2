# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 14:43:20 2021

@author: Florian Egli
"""

import numpy as np
import scipy.interpolate
import matplotlib.pyplot as plt
import pandas as pd
import sys
import os

    
def compareToTheory(dataFolder, theoryFolder, lam, a, title, method):
    
    subtitle = f"a={a*10**6:.3f}um"

    #MomentsResults=np.genfromtxt(theoryFolder+"moments.csv", delimiter=",", skip_header=1)
    #GaussResults=np.genfromtxt(theoryFolder+"gauss.csv", delimiter=",", skip_header=1)
    theoryGauss1D = pd.DataFrame()
    theoryGauss2D = pd.DataFrame()
    theoryMoments = pd.DataFrame()
    theoryGauss1D.index.name = "Index"
    theoryGauss2D.index.name = "Index"
    theoryMoments.index.name = "Index"

    #TODO: Move this for loop to theoryImageCreator
    for folder in sorted(os.listdir(theoryFolder)):
        if folder.endswith("csv"):
            continue
        if "NA" not in folder:
            continue
            
        NA = float(folder.split("NA")[1])
        wxFit = pd.read_csv(theoryFolder+folder+"/WxFit.csv", index_col=0)
        wyFit = pd.read_csv(theoryFolder+folder+"/WyFit.csv", index_col=0)
        
        Gauss1D = {"NA": NA,
                   "w0x": wxFit.loc["Gauss1D"]["w0_mm"],
                   "w0xErr": wxFit.loc["Gauss1D"]["w0Err_mm"],
                   "z0x": wxFit.loc["Gauss1D"]["z0_mm"],
                   "z0xErr": wxFit.loc["Gauss1D"]["z0Err_mm"],
                   "w0y": wyFit.loc["Gauss1D"]["w0_mm"],
                   "w0yErr": wyFit.loc["Gauss1D"]["w0Err_mm"],
                   "z0y": wyFit.loc["Gauss1D"]["z0_mm"],
                   "z0yErr": wyFit.loc["Gauss1D"]["z0Err_mm"]}
        
        Gauss2D = {"NA": NA,
                   "w0x": wxFit.loc["Gauss2D"]["w0_mm"],
                   "w0xErr": wxFit.loc["Gauss2D"]["w0Err_mm"],
                   "z0x": wxFit.loc["Gauss2D"]["z0_mm"],
                   "z0xErr": wxFit.loc["Gauss2D"]["z0Err_mm"],
                   "w0y": wyFit.loc["Gauss2D"]["w0_mm"],
                   "w0yErr": wyFit.loc["Gauss2D"]["w0Err_mm"],
                   "z0y": wyFit.loc["Gauss2D"]["z0_mm"],
                   "z0yErr": wyFit.loc["Gauss2D"]["z0Err_mm"]}
        
        
        Moments = {"NA": NA,
                   "w0x": wxFit.loc["Moments"]["w0_mm"],
                   "w0xErr": wxFit.loc["Moments"]["w0Err_mm"],
                   "z0x": wxFit.loc["Moments"]["z0_mm"],
                   "z0xErr": wxFit.loc["Moments"]["z0Err_mm"],
                   "w0y": wyFit.loc["Moments"]["w0_mm"],
                   "w0yErr": wyFit.loc["Moments"]["w0Err_mm"],
                   "z0y": wyFit.loc["Moments"]["z0_mm"],
                   "z0yErr": wyFit.loc["Moments"]["z0Err_mm"]}
        
        
        theoryGauss1D = theoryGauss1D.append(pd.Series({**Gauss1D}), ignore_index=True)
        theoryGauss2D = theoryGauss2D.append(pd.Series({**Gauss2D}), ignore_index=True)
        theoryMoments = theoryMoments.append(pd.Series({**Moments}), ignore_index=True)
    
    #save this as csv to core radius Folder
    theoryGauss1D.to_csv(theoryFolder+"NAGauss1D.csv")
    theoryGauss2D.to_csv(theoryFolder+"NAGauss2D.csv")
    theoryMoments.to_csv(theoryFolder+"NAMoments.csv")
    
    
    # read the fitted data from measured beam
    wxData = pd.read_csv(dataFolder+"WxFit.csv", index_col=0)
    wyData = pd.read_csv(dataFolder+"WyFit.csv", index_col=0)

    # create fits from theory data
    # don't enforce cubic fit to avoid weird curves
    MomentsXFit = scipy.interpolate.interp1d(theoryMoments["w0x"], theoryMoments["NA"])
    MomentsYFit = scipy.interpolate.interp1d(theoryMoments["w0y"], theoryMoments["NA"])
    
    Gauss1DXFit = scipy.interpolate.interp1d(theoryGauss1D["w0x"], theoryGauss1D["NA"])
    Gauss1DYFit = scipy.interpolate.interp1d(theoryGauss1D["w0y"], theoryGauss1D["NA"])
    
    Gauss2DXFit = scipy.interpolate.interp1d(theoryGauss2D["w0x"], theoryGauss2D["NA"])
    Gauss2DYFit = scipy.interpolate.interp1d(theoryGauss2D["w0y"], theoryGauss2D["NA"])
    
    #print(theoryGauss1D["w0x"])
    #print(theoryGauss2D["w0x"])
    
    w0xArrM = np.linspace(theoryMoments["w0x"].min(), theoryMoments["w0x"].max(), 500)
    w0xArrG1D = np.linspace(theoryGauss1D["w0x"].min(), theoryGauss1D["w0x"].max(), 500)
    w0xArrG2D = np.linspace(theoryGauss2D["w0x"].min(), theoryGauss2D["w0x"].max(), 500)
    
    w0yArrM = np.linspace(theoryMoments["w0y"].min(), theoryMoments["w0y"].max(), 500)
    w0yArrG1D = np.linspace(theoryGauss1D["w0y"].min(), theoryGauss1D["w0y"].max(), 500)
    w0yArrG2D = np.linspace(theoryGauss2D["w0y"].min(), theoryGauss2D["w0y"].max(), 500)
    
    NAresults_x = NAPlot(w0xArrM, w0xArrG1D, w0xArrG2D, MomentsXFit, Gauss1DXFit, Gauss2DXFit, wxData, title, "x", method, 
        theoryGauss1D["w0x"], theoryGauss1D["NA"], theoryGauss2D["w0x"], theoryGauss2D["NA"], theoryMoments["w0x"], theoryMoments["NA"])
    NAresults_y = NAPlot(w0yArrM, w0yArrG1D, w0yArrG2D, MomentsYFit, Gauss1DYFit, Gauss2DYFit, wyData, title, "y", method, 
        theoryGauss1D["w0y"], theoryGauss1D["NA"], theoryGauss2D["w0x"], theoryGauss2D["NA"], theoryMoments["w0x"], theoryMoments["NA"])

    return NAresults_x, NAresults_y
    

def compareToTheory1D(dataFolder, theoryFolder, lam, a, title, method):
    
    subtitle = f"a={a*10**6:.3f}um"

    #MomentsResults=np.genfromtxt(theoryFolder+"moments.csv", delimiter=",", skip_header=1)
    #GaussResults=np.genfromtxt(theoryFolder+"gauss.csv", delimiter=",", skip_header=1)
    theoryGauss1D = pd.DataFrame()
    theoryGauss1D.index.name = "Index"

    #TODO: Move this for loop to theoryImageCreator
    for folder in sorted(os.listdir(theoryFolder)):
        if folder.endswith("csv"):
            continue
        if "NA" not in folder:
            continue
            
        NA = float(folder.split("NA")[1])
        wxFit = pd.read_csv(theoryFolder+folder+"/WxFit.csv", index_col=0)
        wyFit = pd.read_csv(theoryFolder+folder+"/WyFit.csv", index_col=0)
        
        Gauss1D = {"NA": NA,
                   "w0x": wxFit.loc["Gauss1D"]["w0_mm"],
                   "w0xErr": wxFit.loc["Gauss1D"]["w0Err_mm"],
                   "z0x": wxFit.loc["Gauss1D"]["z0_mm"],
                   "z0xErr": wxFit.loc["Gauss1D"]["z0Err_mm"],
                   "w0y": wyFit.loc["Gauss1D"]["w0_mm"],
                   "w0yErr": wyFit.loc["Gauss1D"]["w0Err_mm"],
                   "z0y": wyFit.loc["Gauss1D"]["z0_mm"],
                   "z0yErr": wyFit.loc["Gauss1D"]["z0Err_mm"]}
        
        
        theoryGauss1D = theoryGauss1D.append(pd.Series({**Gauss1D}), ignore_index=True)
    
    #save this as csv to core radius Folder
    theoryGauss1D.to_csv(theoryFolder+"NAGauss1D.csv")
    
    
    # read the fitted data from measured beam
    wxData = pd.read_csv(dataFolder+"WxFit.csv", index_col=0)
    wyData = pd.read_csv(dataFolder+"WyFit.csv", index_col=0)

    # create fits from theory data
    # don't enforce cubic fit to avoid weird curves
    
    Gauss1DXFit = scipy.interpolate.interp1d(theoryGauss1D["w0x"], theoryGauss1D["NA"])
    Gauss1DYFit = scipy.interpolate.interp1d(theoryGauss1D["w0y"], theoryGauss1D["NA"])

    
    #print(theoryGauss1D["w0x"])
    #print(theoryGauss2D["w0x"])
    
    w0xArrG1D = np.linspace(theoryGauss1D["w0x"].min(), theoryGauss1D["w0x"].max(), 500)
    

    w0yArrG1D = np.linspace(theoryGauss1D["w0y"].min(), theoryGauss1D["w0y"].max(), 500)
    
    NAresults_x = NAPlot1D(w0xArrG1D, Gauss1DXFit, wxData, title, "x", method, theoryGauss1D["w0x"], theoryGauss1D["NA"])
    NAresults_y = NAPlot1D(w0yArrG1D, Gauss1DYFit, wyData, title, "y", method, theoryGauss1D["w0y"], theoryGauss1D["NA"])

    return NAresults_x, NAresults_y

def NAPlot(w0ArrM, w0ArrG1D, w0ArrG2D, MomentsFit, Gauss1DFit, Gauss2DFit, wData, title, axis, method, data_1D_w, data_1D_NA, data_2D_w, data_2D_NA, data_m_w, data_m_NA):
    M_w0, M_w0Err = wData.loc['Moments']['w0_mm'], wData.loc['Moments']['w0Err_mm']
    M_NA = MomentsFit(M_w0)
    M_NAErr = abs(MomentsFit(M_w0+M_w0Err)-MomentsFit(M_w0-M_w0Err))/2.
    
    G1D_w0, G1D_w0Err = wData.loc['Gauss1D']['w0_mm'], wData.loc['Gauss1D']['w0Err_mm']
    G1D_NA = Gauss1DFit(G1D_w0)
    G1D_NAErr = abs(Gauss1DFit(G1D_w0+G1D_w0Err)-Gauss1DFit(G1D_w0-G1D_w0Err))/2.
    
    G2D_w0, G2D_w0Err = wData.loc['Gauss2D']['w0_mm'], wData.loc['Gauss2D']['w0Err_mm']
    G2D_NA = Gauss2DFit(G2D_w0)
    G2D_NAErr = abs(Gauss2DFit(G2D_w0+G2D_w0Err)-Gauss2DFit(G2D_w0-G2D_w0Err))/2.

    plt.plot(Gauss1DFit(w0ArrG1D), w0ArrG1D, color = "b", label="theory Gauss1D")
    plt.plot(data_1D_NA, data_1D_w, 'bo', markersize=3) # plot theory points
    plt.plot(Gauss2DFit(w0ArrG2D), w0ArrG2D, color = "r", label="theory Gauss2D")
    plt.plot(data_2D_NA, data_2D_w, 'ro', markersize=3)
    plt.plot(MomentsFit(w0ArrM), w0ArrM, color = "g", label="theory Moments")
    plt.plot(data_m_NA, data_m_w, 'go', markersize=3)
    
    subtitle = (f"a={a:.3f}: {method} $w_{axis}$ vs NA Plot")
    #subtitle = (f"non-linear pixel response n=5: {method} $w_{axis}$ vs NA Plot")
    
    ax = plt.gca()
    wMin, wMax = ax.get_ylim()
    NAmin, NAmax = ax.get_xlim()
    
    plt.axvline(M_NA, ymax=(M_w0-wMin)/(wMax-wMin),color='g', linestyle='dashed', linewidth=2)
    plt.axhline(M_w0, xmax=(M_NA-NAmin)/(NAmax-NAmin),color='g', linestyle='dashed', linewidth=2)
    
    plt.axvline(G1D_NA, ymax=(G1D_w0-wMin)/(wMax-wMin),color='b', linestyle='dashed', linewidth=2)
    plt.axhline(G1D_w0, xmax=(G1D_NA-NAmin)/(NAmax-NAmin),color='b', linestyle='dashed', linewidth=2)
    plt.axvline(G2D_NA, ymax=(G2D_w0-wMin)/(wMax-wMin),color='r', linestyle='dashed', linewidth=2)
    plt.axhline(G2D_w0, xmax=(G2D_NA-NAmin)/(NAmax-NAmin),color='r', linestyle='dashed', linewidth=2)
    #plt.axhline(G1D_w0, color='b', linestyle='dashed', linewidth=2)
    #plt.axhline(G2D_w0, color='r', linestyle='dashed', linewidth=2)

    # print('height of bar ' + str(a) + ' is ' + str(G1D_w0))
    # plot error bars
    plt.bar(G1D_NA, G1D_w0-wMin, 2*G1D_NAErr, wMin, alpha=0.1,color='b')
    plt.barh(G1D_w0, G1D_NA-NAmin, 2*G1D_w0Err, NAmin, alpha=0.1, color='b')
    plt.bar(G2D_NA, G2D_w0-wMin, 2*G2D_NAErr, wMin, alpha=0.1,color='r')
    plt.barh(G2D_w0, G2D_NA-NAmin, 2*G2D_w0Err, NAmin, alpha=0.1, color='r')
    plt.bar(M_NA, M_w0-wMin, 2*M_NAErr, wMin, alpha=0.1,color='g')
    plt.barh(M_w0, M_NA-NAmin, 2*M_w0Err, NAmin, alpha=0.1, color='g')

    plt.text(0.5, 0.5,f"M_NA{axis}     = {M_NA:.5f} ({M_NAErr*1e5:.0f})\n"+        
             f"G1D_NA{axis} = {G1D_NA:.5f} ({G1D_NAErr*1e5:.0f})\n"+
             f"G2D_NA{axis} = {G2D_NA:.5f} ({G2D_NAErr*1e5:.0f})",
             transform=ax.transAxes, fontsize=12)

    # plt.text(0.5, 0.5,       
    #          f"G1D_NA{axis} = {G1D_NA:.5f} ({G1D_NAErr*1e5:.0f})\n"+
    #          f"G2D_NA{axis} = {G2D_NA:.5f} ({G2D_NAErr*1e5:.0f})",
    #          transform=ax.transAxes, fontsize=12)
    
    plt.xlabel("NA")
    plt.ylabel("waist $w_0$ in $mm$")
    plt.title(title+"\n"+subtitle)
    plt.legend()
    outputFolder = dataFolder + f'theory/a{a:.3f}/'
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    plt.savefig(outputFolder+f'{method}_NA{axis}plot.png')
    #plt.show()
    plt.close()

    return [float(G1D_NA), G1D_NAErr, float(G2D_NA), G2D_NAErr, float(M_NA), M_NAErr]

def NAPlot1D(w0ArrG1D, Gauss1DFit, wData, title, axis, method, data_1D_w, data_1D_NA):
    
    G1D_w0, G1D_w0Err = wData.loc['Gauss1D']['w0_mm'], wData.loc['Gauss1D']['w0Err_mm']
    G1D_NA = Gauss1DFit(G1D_w0)
    G1D_NAErr = abs(Gauss1DFit(G1D_w0+G1D_w0Err)-Gauss1DFit(G1D_w0-G1D_w0Err))/2.

    plt.plot(Gauss1DFit(w0ArrG1D), w0ArrG1D, color = "b", label="theory Gauss1D")
    plt.plot(data_1D_NA, data_1D_w, 'bo', markersize=3) # plot theory points
    
    subtitle = (f"a={a:.3f}: {method} $w_{axis}$ vs NA Plot")
    #subtitle = (f"non-linear pixel response n=5: {method} $w_{axis}$ vs NA Plot")
    
    ax = plt.gca()
    wMin, wMax = ax.get_ylim()
    NAmin, NAmax = ax.get_xlim()
    
    
    plt.axvline(G1D_NA, ymax=(G1D_w0-wMin)/(wMax-wMin),color='b', linestyle='dashed', linewidth=2)
    plt.axhline(G1D_w0, xmax=(G1D_NA-NAmin)/(NAmax-NAmin),color='b', linestyle='dashed', linewidth=2)

    # print('height of bar ' + str(a) + ' is ' + str(G1D_w0))
    # plot error bars
    plt.bar(G1D_NA, G1D_w0-wMin, 2*G1D_NAErr, wMin, alpha=0.1,color='b')
    plt.barh(G1D_w0, G1D_NA-NAmin, 2*G1D_w0Err, NAmin, alpha=0.1, color='b')

    plt.text(0.5, 0.5,   
             f"G1D_NA{axis} = {G1D_NA:.5f} ({G1D_NAErr*1e5:.0f})\n",
             transform=ax.transAxes, fontsize=12)
    
    plt.xlabel("NA")
    plt.ylabel("waist $w_0$ in $mm$")
    plt.title(title+"\n"+subtitle)
    plt.legend()
    outputFolder = dataFolder + f'theory/a{a:.3f}/'
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    plt.savefig(outputFolder+f'{method}_NA{axis}plot.png')
    #plt.show()
    plt.close()

    return [float(G1D_NA), G1D_NAErr]


def plotavsNA(dfNA, title, outputFolder):

    plt.errorbar(dfNA['a [um]'], dfNA['G1D_NA'], fmt='o', yerr=dfNA['G1D_NAErr'], label='Gauss1D', markersize=3)
    plt.errorbar(dfNA['a [um]'], dfNA['G2D_NA'], fmt='o', yerr=dfNA['G2D_NAErr'], label='Gauss2D', markersize=3)
    plt.errorbar(dfNA['a [um]'], dfNA['Moments_NA'], fmt='o', yerr=dfNA['Moments_NAErr'], label='Moments', markersize=3)

    plt.xlabel('Predicted core radius $a$ [um]')
    plt.ylabel('Fitted NA')

    plt.title(title + ' \n Theory comparison')
    plt.legend()
    
    if title.endswith('X Fit'):
        i = 'X'
    else:
        i = 'Y'
        
    plt.savefig(os.path.join(outputFolder, 'AvsNAPlot' + i + '.png'))
    #plt.show()
    plt.close()


    return

if __name__ == "__main__":
    dataFolder = sys.argv[1]+"/"
    title = dataFolder.strip("./")
    title = title.split("/")[-1]
    lam = 405e-9
    #a=1.5e-6
    theory = "./theory/"
    #fiberModeFolder = f'./theory/FiberMode/lam{lam*10**9:.0f}/a{a*10**6:.1f}/'
    fiberModeFolder = f'{theory}FiberMode/lam{lam*10**9:.0f}/'
    NAData_x = []
    NAData_y = []

    for folder in os.listdir(fiberModeFolder):
        if "a" in folder:
            try:
                a = float(folder.split("a")[1])
                NAresults_x, NAresults_y = compareToTheory1D(dataFolder, fiberModeFolder+folder+"/", lam, a, title, "FiberMode")
                
                # add a value to results and append to data collection
                NAresults_x.insert(0, a)
                NAresults_y.insert(0, a)
                NAData_x.append(NAresults_x)
                NAData_y.append(NAresults_y)

            except Exception as e:
                print("FiberMode", folder, "failed")
                print(e)
    
    if False:
        dfNA_x = pd.DataFrame(NAData_x, columns =['a [um]', 'G1D_NA', 'G1D_NAErr', 'G2D_NA', 'G2D_NAErr', 'Moments_NA', 'Moments_NAErr'])
        dfNA_y = pd.DataFrame(NAData_y, columns =['a [um]', 'G1D_NA', 'G1D_NAErr', 'G2D_NA', 'G2D_NAErr', 'Moments_NA', 'Moments_NAErr'])
        
        outputFolder = os.path.join(dataFolder, "theory")
        dfNA_x.to_csv(os.path.join(outputFolder, "NA_x.csv"))
        dfNA_y.to_csv(os.path.join(outputFolder, "NA_y.csv"))

        plotavsNA(dfNA_x, title + ' X Fit', outputFolder)
        plotavsNA(dfNA_y, title + ' Y Fit', outputFolder)
    else:
        dfNA_x = pd.DataFrame(NAData_x, columns =['a [um]', 'G1D_NA', 'G1D_NAErr'])
        dfNA_y = pd.DataFrame(NAData_y, columns =['a [um]', 'G1D_NA', 'G1D_NAErr'])
        
        outputFolder = os.path.join(dataFolder, "theory")
        dfNA_x.to_csv(os.path.join(outputFolder, "NA_x.csv"))
        dfNA_y.to_csv(os.path.join(outputFolder, "NA_y.csv"))

        # plotavsNA(dfNA_x, title + ' X Fit', outputFolder)
        # plotavsNA(dfNA_y, title + ' Y Fit', outputFolder)

    '''
    #GaussFolder = f'./theory/Gauss/lam{lam*10**9:.0f}/a{a*10**6:.1f}/'
    gaussFolder = f'{theory}Gauss/lam{lam*10**9:.0f}/'
    for folder in os.listdir(gaussFolder):
        if "a" in folder:
            try:
                a = float(folder.split("a")[1])
                compareToTheory(dataFolder, gaussFolder+folder+"/", lam, a, title, "Gauss")
            except:
                print("Gauss",folder,"failed")
    '''
    
    