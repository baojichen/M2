#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 13:45:46 2022

@author: Cotton
"""

import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from datetime import datetime



def applyNonlinearity(im, alpha, n):
    im = im * (1-alpha*np.power(im/np.max(im),n) )
    im = np.rint(im) # round to nearest integer
    return im

plt.figure(dpi=150)

n=5
x = np.linspace(0, 1, 50)
for a in [0, 0.02,0.08, 0.15]:
    y = x * (1-a*np.power(x,n))
    lab = 'a=' + str(a) + ', n=' + str(n)
    plt.plot(x,y,label=lab)

n=2
for a in [ 0.15]:
    y = x * (1-a*np.power(x,n))
    lab = 'a=' + str(a) + ', n=' + str(n)
    plt.plot(x,y,label=lab)
    
plt.legend()
plt.title('Nonlinearity with different alphas and n')
plt.xlabel('$I/I_{max}$')
plt.ylabel('$I_{eff}/I_{max}$')