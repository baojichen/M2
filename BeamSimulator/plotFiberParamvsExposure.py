#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 15:30:11 2022

@author: Cotton
"""
import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm

directory = '/Users/Cotton/Downloads/20220721'

FolderName = []
ExpTime = []
NA = []
NADiff = []
Radius = []

for directory in ['/Users/Cotton/Documents/fiberanalysis']
    # sort by last word
    folders = os.listdir(directory)
    folders.sort(key=lambda s: s.split('-')[-1])
    
    for folder in folders:
        if folder.startswith('20220713') and not folder.endswith('csv'): 
            
            FolderName.append(folder)
            
            metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')
    
            exptime = metadata['ExposureTime'][0] # get first exposure time
            ExpTime.append(exptime)
            
            df = pd.read_csv(os.path.join(directory, folder, 'theory/NA_y.csv'))
            
            diff = abs(df['G1D_NA'] - df['G2D_NA'])
            err = diff.min()
            index = diff.idxmin()
            
            NAavg = (df['G1D_NA'][index] + df['G2D_NA'][index])/2 # avg NA values
            rad = df['a [um]'][index]
            
            NA.append(NAavg)
            NADiff.append(err)
            Radius.append(rad)
            
            #plt.plot(metadata['StagePos'], metadata['ExposureTime'], label=label1, marker='x')
        
data = {'Folder Name': FolderName,
        'Exposure Time': ExpTime,
        'NA': NA,
        'NA Diff': NADiff,
        'Radius': Radius
        }
savedata = pd.DataFrame(data)

fig, ax = plt.subplots()

# plot error accurately with scale
ax.set_ylim([0.097, 0.109])

M = ax.transData.get_matrix()
yscale = M[1,1]
markersize = yscale*NADiff

# normalize and scale
#markersize = np.array(NADiff) / max(NADiff) * 40

#sc = plt.scatter(ExpTime, NA, s=markersize**2 + 100, c=Radius, alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.65)
sc = plt.scatter(ExpTime[:3], NA[:3], s=markersize[:3]**2, c=Radius[:3], alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.65)
sc1 = plt.scatter(ExpTime[-3:], NA[-3:], s=markersize[-3:]**2, c=Radius[-3:], marker='s', alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.65)

# plt.errorbar(ExpTime, Gauss1D, fmt='o', yerr=Gauss1Derr, label='Gauss1D')
# plt.errorbar(ExpTime, Gauss2D, fmt='o', yerr=Gauss2Derr, label='Gauss2D')
# plt.errorbar(ExpTime, Moments, fmt='o', yerr=Momentserr, label='Moments')
plt.colorbar(sc)
# plt.text(14.1, 0.0993, 'markersize = diff between NA \n square = misaligned, circle = aligned ')
plt.text(9.1, 0.098, 'markersize = diff between NA \n Fixed rotation 0.1')
plt.ylabel('Avg of closest Gauss1D and Gauss2D NA')
plt.xlabel('270 mm Exposure Time [ms]')

plt.title('Fiber Parameters vs Exposure Time \n 20220721-P5-405BPM-FC2-PCSide Y Fit')

plt.show()

#savedata.to_csv(os.path.join(directory, "20220721-0deg-y.csv"))















