#!/usr/bin/env python

#------------------------------------------------------------------------------
#                 PyuEye example - camera modul
#
# Copyright (c) 2017 by IDS Imaging Development Systems GmbH.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#------------------------------------------------------------------------------

from pyueye import ueye
from pyueye_utils import (uEyeException, Rect, get_bits_per_pixel,
                                  ImageBuffer, check)
import numpy as np

class Camera:

    def __init__(self, device_id=0):
        self.h_cam = ueye.HIDS(device_id)
        print(self.h_cam)
        self.img_buffers = []

    def __enter__(self):
        self.init()
        return self

    def __exit__(self, _type, value, traceback):
        self.exit()

    def handle(self):
        return self.h_cam

    def alloc(self, buffer_count=1):
        rect = self.get_aoi()
        bpp = get_bits_per_pixel(self.get_colormode())
        self.buff = ImageBuffer()
        ueye.is_AllocImageMem(self.h_cam,
                                  rect.width, rect.height, bpp,
                                 self.buff.mem_ptr, self.buff.mem_id)
        ueye.is_SetImageMem(self.h_cam,self.buff.mem_ptr, self.buff.mem_id)
        ueye.is_SetExternalTrigger(self.h_cam,ueye.IS_SET_TRIGGER_SOFTWARE)
        '''
        hw_gamma = ueye.DOUBLE()
        gamma = ueye.UINT()
        hw_gamma = ueye.is_SetHardwareGamma(self.h_cam,ueye.IS_GET_HW_SUPPORTED_GAMMA)
        ueye.is_Gamma(self.h_cam,ueye.IS_GAMMA_CMD_GET,gamma,ueye.sizeof(gamma))
        if hw_gamma == ueye.IS_SET_HW_GAMMA_OFF:
           print('hardware gamma is not supported')
        if hw_gamma == ueye.IS_SET_HW_GAMMA_ON:
           print('hardware gamma is not supported')
        print('hardware gamma is: ',hw_gamma)
        print('software gamma is: ',gamma.value)
        gain = ueye.is_SetGainBoost(self.h_cam,ueye.IS_GET_GAINBOOST)
        print('gain gamma is: ',gain)
        '''

    def init(self):

        ret = ueye.is_InitCamera(self.h_cam, None)
        if ret != ueye.IS_SUCCESS:
            self.h_cam = None
            raise uEyeException(ret)

        return ret

    def exit(self):

        ret = None
        if self.h_cam is not None:
            ret = ueye.is_ExitCamera(self.h_cam)
        if ret == ueye.IS_SUCCESS:
            self.h_cam = None

    def get_aoi(self):

        rect_aoi = ueye.IS_RECT()
        ueye.is_AOI(self.h_cam, ueye.IS_AOI_IMAGE_GET_AOI, rect_aoi, ueye.sizeof(rect_aoi))

        return Rect(rect_aoi.s32X.value,
                    rect_aoi.s32Y.value,
                    rect_aoi.s32Width.value,
                    rect_aoi.s32Height.value)

    def set_aoi(self, x, y, width, height):

        rect_aoi = ueye.IS_RECT()
        rect_aoi.s32X = ueye.int(x)
        rect_aoi.s32Y = ueye.int(y)
        rect_aoi.s32Width = ueye.int(width)
        rect_aoi.s32Height = ueye.int(height)

        return ueye.is_AOI(self.h_cam, ueye.IS_AOI_IMAGE_SET_AOI, rect_aoi, ueye.sizeof(rect_aoi))

    def set_imaging_mode(self,setting):

        if setting == 'fast':
            self.set_colormode(ueye.IS_CM_MONO8)
        if setting == 'quality':
            self.set_colormode(ueye.IS_CM_MONO12)
        else:
            self.set_colormode(ueye.IS_CM_MONO12)
        self.set_max_pixel_clock()
        min_ft = ueye.DOUBLE()
        max_ft = ueye.DOUBLE()
        intervall_ft = ueye.DOUBLE()
        ueye.is_GetFrameTimeRange(self.h_cam,min_ft,max_ft, intervall_ft)
        targetfps = min(1/min_ft.value, 30)
        realfps = ueye.DOUBLE()
        ueye.is_SetFrameRate(self.h_cam,ueye.DOUBLE(targetfps),realfps)

    def get_min_exposure_time(self):

        exposure_time_min = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MIN,exposure_time_min,ueye.sizeof(exposure_time_min))
        return exposure_time_min.value

    def get_max_exposure_time(self):

        exposure_time_max = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MAX,exposure_time_max,ueye.sizeof(exposure_time_max))
        return exposure_time_max.value

    def set_max_exposure_time(self):

        self.set_exposure_time(self.get_max_exposure_time())

    def get_exposure_time(self):

        exposure_time = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE,exposure_time,ueye.sizeof(exposure_time))
        return exposure_time.value

    def set_exposure_time(self, exp_time):

        if exp_time > self.get_max_exposure_time():
            exp_time = self.get_max_exposure_time()
        if exp_time < self.get_min_exposure_time():
            exp_time = self.get_min_exposure_time()
        exposure_time_inc = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_INC,exposure_time_inc,ueye.sizeof(exposure_time_inc))
        exposure_time = ueye.DOUBLE(exp_time)
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_SET_EXPOSURE, exposure_time,ueye.sizeof(exposure_time))

    def set_gain(self):
        print(ueye.is_SetAutoParameter(self.h_cam,0x8800,1,None))
        print(ueye.is_SetAutoParameter(self.h_cam,0x8802,0,None))
        print(ueye.is_SetAutoParameter(self.h_cam,0x8804,0,None))
        print(ueye.is_SetAutoParameter(self.h_cam,0x8801,0,None))
        print(ueye.is_SetHWGainFactor(self.h_cam,0x8004,100))
    def get_exposure_step(self):

        exposure_time_inc = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_INC,exposure_time_inc,ueye.sizeof(exposure_time_inc))
        return int(round(self.get_exposure_time()/exposure_time_inc))

    def set_exposure_step(self,steps):

        exposure_time_inc = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_INC,exposure_time_inc,ueye.sizeof(exposure_time_inc))
        self.set_exposure_time(exposure_time_inc*steps)

    def change_exposure_step(self, steps):

        exposure_time = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE,exposure_time,ueye.sizeof(exposure_time))
        exposure_time_min = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MIN,exposure_time_min,ueye.sizeof(exposure_time_min))
        exposure_time_inc = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_INC,exposure_time_inc,ueye.sizeof(exposure_time_inc))
        if 0.0 >= exposure_time.value+exposure_time_inc.value*steps:
            self.set_exposure_time(exposure_time_min.value)
        else:
            self.set_exposure_time(exposure_time.value+exposure_time_inc.value*steps)

    def get_possible_steps_exposure(self):

        exposure_time_max = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MAX,exposure_time_max,ueye.sizeof(exposure_time_max))
        exposure_time_min = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MIN,exposure_time_min,ueye.sizeof(exposure_time_min))
        exposure_time_inc = ueye.DOUBLE()
        ueye.is_Exposure(self.h_cam,ueye.IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_INC,exposure_time_inc,ueye.sizeof(exposure_time_inc))
        return int(round((exposure_time_max.value-exposure_time_min.value)/exposure_time_inc.value))

    def set_max_pixel_clock(self):

        max_clock_count = ueye.UINT()
        ueye.is_PixelClock(self.h_cam,ueye.IS_PIXELCLOCK_CMD_GET_NUMBER,max_clock_count,ueye.sizeof(max_clock_count))
        max_clock = (ueye.UINT*max_clock_count.value)()
        ueye.is_PixelClock(self.h_cam,ueye.IS_PIXELCLOCK_CMD_GET_LIST,max_clock,ueye.sizeof(max_clock))
        pixel_clock = ueye.UINT(max_clock[len(max_clock)-1])
        ret = ueye.is_PixelClock(self.h_cam,ueye.IS_PIXELCLOCK_CMD_SET,pixel_clock,ueye.sizeof(pixel_clock))
        print(f"set_max_pixel_clock to {int(pixel_clock)}: {ret}")
    def set_colormode(self, colormode):
        check(ueye.is_SetColorMode(self.h_cam, colormode))

    def get_colormode(self):
        ret = ueye.is_SetColorMode(self.h_cam, ueye.IS_GET_COLOR_MODE)
        return ret

    def get_format_list(self):

        count = ueye.UINT()
        check(ueye.is_ImageFormat(self.h_cam, ueye.IMGFRMT_CMD_GET_NUM_ENTRIES, count, ueye.sizeof(count)))
        format_list = ueye.IMAGE_FORMAT_LIST(ueye.IMAGE_FORMAT_INFO * count.value)
        format_list.nSizeOfListEntry = ueye.sizeof(ueye.IMAGE_FORMAT_INFO)
        format_list.nNumListElements = count.value
        check(ueye.is_ImageFormat(self.h_cam, ueye.IMGFRMT_CMD_GET_LIST,
                                  format_list, ueye.sizeof(format_list)))
        return format_list
