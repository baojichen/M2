import cv2
import numpy as np

import matplotlib.pyplot as plt

import os
import scipy.optimize
import scipy.interpolate
from math import tan,pi,sqrt,sin,cos, fmod
import time
import queue
import copy
import scipy

import threading

import logging


class Analyse(threading.Thread):

    def __init__(self, master=None):

        threading.Thread.__init__(self)
        self.master = master
        self.pixelSize = 2.4e-3 # in mm
        self.waveLength = 410e-6 #in mm
        self.shouldFitRes = False
        self.calcMoment = False
        self.fixM2Parameter = False
        self.updateM2cont = False
        self.saveDatacont = False
        self.gaussMajCut = False
        self.rotationFixed = False
        self.gaussMinCut = False
        self.finished = False
        self.q = queue.Queue()
        self.fitResults = {}
        self.running = True
        self.directory = None
        self.rotationAngle = 0
        self.fitResults = {}
        zList=[]
        rotAngleList=[]
        self.fitResults['rotAngleList'] = rotAngleList
        self.fitResults['zList'] = zList

    def stop(self):
        self.running = False
        self.q.put('done')

    def stopFit(self):
        if self.q.qsize() > 0:
           self.q.queue.clear()

    def run(self):
         while self.running:
             try:
                 res = self.q.get()
                 if res != 'done':
                     self.analyseFit(*res)
                     #fit is done, add data to M2
                     if self.saveDatacont:
                         self.saveData()
                     if self.updateM2cont:
                         self.updateM2()
             except Exception as exp:
                    logging.error(f'analysis.run error adding data: {exp} {exp.__traceback__}')
             try:
                 if self.q.qsize() == 0 and len(self.fitResults) > 4:
                     self.updateM2()
                     self.saveData()
                     self.finished = True
                 else:
                     self.finished = False
             except Exception as exp:
                    logging.error(f'analysis.run error updating M2: {exp}')
                    self.finished = True

      #signals to queue job is done
    def empty_M2_fit(self):
        self.fitResults = {}
        zList=[]
        rotAngleList=[]
        self.fitResults['rotAngleList'] = rotAngleList
        self.fitResults['zList'] = zList
        self.master.view.canvas.figure.clf()
        self.master.view.canvas.draw()

    def find_ellipses(self,im):
        # Otsu's threshbesting after Gaussian filtering
        blur = cv2.GaussianBlur(im,(5,5),0)
        ret,thresh = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        try:
            _,contours,hierarchy = cv2.findContours(thresh, 1, 2)
        except:
            contours,hierarchy = cv2.findContours(thresh, 1, 2)
        best_x, best_y, best_ma, best_MA, best_angle = 0, 0, 0 , 0 ,0
        if len(contours) != 0:
            for cont in contours:
                if len(cont) < 5:
                    continue
                (x,y),(ma,MA),angle = cv2.fitEllipse(cont)
                if ma > best_ma and MA > best_MA:
                    (best_x, best_y), (best_ma, best_MA), best_angle = (x,y),(ma,MA),angle

        if best_x == 0 and best_y == 0 and best_ma == 0 and best_MA == 0 and best_angle == 0:
            return None
        else:
            return (best_x, best_y), (best_ma, best_MA), best_angle

    def wz(self,z,z0,w0,M2):

        zR=pi*w0**2/(self.waveLength*M2)
        return w0*np.sqrt((1+((z-z0)/zR)**2))

    def fixedwz(self,z,z0,w0):

        zR=pi*w0**2/(self.waveLength)
        return w0*np.sqrt((1+((z-z0)/zR)**2))

    def gaussian(self, height, center_x, center_y, width_x, width_y, rotation, offset):
        """Returns a gaussian function with the given parameters"""
        width_x = float(width_x)
        width_y = float(width_y)
        rotation = np.deg2rad(rotation)
        center_x_tmp = center_x * np.cos(rotation) - center_y * np.sin(rotation)
        center_y_tmp = center_x * np.sin(rotation) + center_y * np.cos(rotation)

        center_x=center_x_tmp
        center_y=center_y_tmp

        def rotgauss(x,y):
            xp = x * np.cos(rotation) - y * np.sin(rotation)
            yp = x * np.sin(rotation) + y * np.cos(rotation)
            g = height*np.exp(
                -2*(((center_x-xp)/width_x)**2+
                  ((center_y-yp)/width_y)**2)) + offset
            return g
        return rotgauss

    def gaussianRotFixed(self, height, center_x, center_y, width_x, width_y, offset):
        return self.gaussian(height, center_x, center_y, width_x, width_y, self.rotationAngle, offset)

    def calcmoments(self,data,pos=None,comimg=None,noisesuppr=7, fixedAngle = None):
        # data = precut image
        # pos is position of
        # coming is complete image
        #deepcopy precut image
        im = copy.deepcopy(data)
        # "noise suppression"
        # because the laser does not fill up the whole image we can use the unused part of the image
        #to estimate the amount of noise on the image that we have
        noise_avg = 0
        noise_std = 0
        masked_img = 0
        masked_max = 0
        if pos is not None and comimg is not None:
            img = copy.deepcopy(comimg)
            mask1 = np.full(comimg.shape,False)
            mask1[pos[0]:pos[1],pos[2]:pos[3]] = True
            masked_img = np.ma.array(img,mask = mask1)
            noise_avg = np.ma.mean(masked_img)
            noise_std = np.ma.std(masked_img)
            masked_max = (masked_img.compressed()).max()
            print('Noise suppr data:',noise_avg,noise_std,noisesuppr,(abs(noise_avg)+noise_std*noisesuppr),masked_max)
            #im =cv2.subtract(im,(abs(noise_avg)+noise_std*noisesuppr))
            #im = im-abs(noise_avg)

        x_size = np.size(im,1)
        y_size = np.size(im,0)
        #selfconfwidth means that the cutout image that is used in the next iteration is 2x the size of the result
        selfconfwidth = 2
        # if the image is too small, reject it, D4sigma is unreliable threre
        if x_size < 20 or y_size < 20:
            print('rejecting image because size is too small and produces unreliable results')
            return [False]

        x, y, width_x, width_y = np.size(im,1)/2, np.size(im,0)/2, np.size(im,1)/(4*selfconfwidth), np.size(im,0)/(4*selfconfwidth)
        x_p, y_p, width_x_p,width_y_p,im2 = 0,0,0,0,0
        max_iterations= 100
        iterations = 0

        while not (x_p==x and y_p==y and width_x_p==width_x and width_y_p==width_y):
            print(x,y,width_x,width_y)
            x1lim = int(round(x-width_x/2*selfconfwidth,0))
            x2lim = int(round(x+width_x/2*selfconfwidth,0))
            y1lim = int(round(y-width_y/2*selfconfwidth,0))
            y2lim = int(round(y+width_y/2*selfconfwidth,0))
            if x1lim<0:
                x1lim=0
                print('warning: image cut is too small')
                break
            if y1lim<0:
                y1lim=0
                print('warning: image cut is too small')
                break
            if x2lim > x_size-1:
                x2lim=x_size-1
                print('warning: image cut is too big')
                break
            if y2lim > y_size-1:
                y2lim=y_size-1
                print('warning: image cut is too big')
                break

            im2 = im[y1lim:y2lim,x1lim:x2lim]
            x_p = copy.deepcopy(x)
            y_p = copy.deepcopy(y)
            width_x_p = copy.deepcopy(width_x)
            width_y_p = copy.deepcopy(width_y)
            x, y, width_x, width_y, phi = self.beam_size(copy.deepcopy(im2), fixedAngle)
            x += x1lim
            y += y1lim
            iterations += 1
            if iterations > max_iterations:
                break

        #This adjusts the beam size to fit our defintion of a transversal gaussian beam profile (1/e^2 width instead of 4 sigma width of a probability distribution)
        width_x = width_x/2
        width_y = width_y/2
        x1lim = int(x-width_x/2)
        x2lim = int(x+width_x/2)
        y1lim = int(y-width_y/2)
        y2lim = int(y+width_y/2)
        if x1lim<0 or y1lim<0 or x2lim > x_size-1 or y2lim > y_size-1:
            print('rejecting image because size is unusable',x,y,width_x,width_y)
            return [False]

        return np.asarray([im2.max(), x, y, width_x, width_y,phi,0])

    def fitgaussian(self, data):
        #Returns (height, x, y, width_x, width_y)
        #the gaussian parameters of a 2D distribution found by a fit
        params = self.calcmoments(data)
        errorfunction = lambda p: np.ravel(self.gaussian(*p)(*np.indices(data.shape)) - data)
        p, success = scipy.optimize.leastsq(errorfunction, params)
        return p

    def fitgaussianRotFixed(self, data):
        #Returns (height, x, y, width_x, width_y)
        #the gaussian parameters of a 2D distribution found by a fit
        params = self.calcmoments(data, fixedAngle = self.rotationAngle)
        params = params[0:6]
        errorfunction = lambda p: np.ravel(self.gaussianRotFixed(*p)(*np.indices(data.shape)) - data)
        p, success = scipy.optimize.leastsq(errorfunction, params)
        return p

    def gauss1d(self, r,r0,A,w,c):
        return A*np.exp(-2*(r-r0)**2/w**2)+c

    def reduceImageSizeToWidth(self, data):
        #reduce the image size that is used by the gaussian fit by doing a 1d gaussian fit of the image and estimating the size
        X = np.arange(0, data.shape[0])
        Y = np.arange(0, data.shape[1])
        xint = data.sum(axis=0)
        yint = data.sum(axis=1)
        x = np.sum(X*yint)/np.sum(yint)
        y = np.sum(Y*xint)/np.sum(yint)
        if int(y)<len(data):
            col = data[:, int(y)]
        else:
            col = data[:,len(data)-1]
        width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/abs(col.sum()))/2.2
        height = data.max()
        maxrow=col.argmax()
        maxcol=data[maxrow,:].argmax()
        xvals = np.arange(0,data.shape[1])
        popt, pcov = scipy.optimize.curve_fit(self.gauss1d, xvals, data[maxrow,:],(x,height,width_x,data[0][0]),maxfev=10000)
        width=popt[2]
        return maxrow, maxcol, np.abs(width)

    def cutImage(self, im, x, y, x_width, y_width):
        #function to cut the image
        im_xsize,im_ysize,maxVal = self.imageParams(im)
        x1lim = int(round(x-x_width))
        x2lim = int(round(x+x_width))
        y1lim = int(round(y-y_width))
        y2lim = int(round(y+y_width))
        #it does not throw and error/exception when the image is out of bounds, it instead just clips it
        if x1lim<0:
          x1lim=0
        if y1lim<0:
          y1lim=0
        if x2lim > im_xsize:
          x2lim=im_xsize-1
        if y2lim > im_ysize:
          y2lim=im_ysize-1
        ims = np.array(im[(y1lim):(y2lim),(x1lim):(x2lim)],copy = True)
        return ims

    def imageParams(self, im):

      return np.size(im,1),np.size(im,0),im.max()

    def updateM2(self):
        #Create the lists which contain all the necessary data
        zList = []
        wzXlist=[]
        wzYlist=[]
        for key, fitData in self.fitResults.items():
            try:
                zList.append(float(key))
            except:
                continue
            wx = fitData[3]
            wy = fitData[4]
            wzXlist.append(abs(wx))
            wzYlist.append(abs(wy))
        # if there are less points than the degrees of freedom for the fit then skip, otherwise it would crash
        if len(zList) < 3:
            return
        #create the figure
        self.figMFDfit= plt.figure()
        plt.figure(self.figMFDfit.number)
        plt.clf()
        # plot the x and y widths
        plt.plot(zList,np.array(wzXlist)*1000, marker=".", linestyle='None',markersize=5,color='blue')
        plt.plot(zList,np.array(wzYlist)*1000, marker=".", linestyle='None',markersize=5,color='red')
        plt.minorticks_on()
        xvals = np.arange(min(zList)-1, max(zList)+2, 1)

        #fit x widths of the M2 plot with M2 = 1 or freerunning M2
        if self.fixM2Parameter:
            popt, pcov = scipy.optimize.curve_fit(self.fixedwz, zList, wzXlist,(150,100e-3))
            wzXfit = lambda x: self.fixedwz(x, *popt)
        else:
            popt, pcov = scipy.optimize.curve_fit(self.wz, zList, wzXlist,(150,100e-3,1))
            wzXfit = lambda x: self.wz(x, *popt)
        # this is not a real reduced chi_sq per DOF but just the variance of Fit vs data
        chi_sq = (((wzXfit(zList)-wzXlist)**2)).sum()

        perr = np.sqrt(np.diag(pcov))
        w0x=popt[1]*1e3
        w0xErr=perr[1]*1e3
        z0x=popt[0]
        z0xErr=perr[0]
        chi_sq_x = chi_sq

        if self.fixM2Parameter:
            M2x=1
            M2xErr=0
        else:
            M2x=popt[2]
            M2xErr=perr[2]

        plt.plot(xvals,wzXfit(xvals)*1e3,color='blue',linewidth=2)
        plt.minorticks_on()
        wzXfitcopy = copy.deepcopy(wzXfit(xvals))
        #fit y widths of the M2 plot with M2 = 1 or freerunning M2

        if self.fixM2Parameter:
            popt, pcov = scipy.optimize.curve_fit(self.fixedwz, zList, wzYlist,(150,100e-3))
            wzYfit = lambda x: self.fixedwz(x, *popt)
        else:
            popt, pcov = scipy.optimize.curve_fit(self.wz, zList, wzYlist,(150,100e-3,1))
            wzYfit = lambda x: self.wz(x, *popt)
        # this is not a real reduced chi_sq per DOF but just the variance of Fit vs data
        chi_sq = (((wzYfit(zList)-wzYlist)**2)).sum()
        perr = np.sqrt(np.diag(pcov))
        w0y=popt[1]*1e3
        w0yErr=perr[1]*1e3
        z0y=popt[0]
        z0yErr=perr[0]
        chi_sq_y = chi_sq
        if self.fixM2Parameter:
            M2y=1
            M2yErr=0
        else:
            M2y=popt[2]
            M2yErr=perr[2]

        plt.plot(xvals,wzYfit(xvals)*1e3,color='red',linewidth=2)
        plt.minorticks_on()
        plt.xlabel('$z$ [mm]')
        plt.ylabel('Fitted width $w(z)=w_0 \sqrt{1+((z-z_0)/z_R)^2}$ [mm]')
        #german addition
        #plt.xlabel('z Position $(mm)$')
        #plt.ylabel('Strahlgröße $(\mu m)$')
        if self.calcMoment:
            plt.title('M² fit from 2nd moments, \n ' +self.mainTitle)
        else:
            plt.title('M² fit from 2D gaussian fit, \n ' +self.mainTitle)
        plt.legend(['$w(z)_x$', '$w(z)_y$']) #,'fit x', 'fit y'
        w0y = abs(w0y)
        w0x = abs(w0x)
        if(w0x<w0y):
            (w0x,w0y,w0xErr,w0yErr)=(w0y,w0x,w0yErr,w0xErr)

        zRy=abs(np.pi*(w0y*1e-3)**2/(self.waveLength*M2y))
        zRx=abs(np.pi*(w0x*1e-3)**2/(self.waveLength*M2x))
        ast = (z0y-z0x)/(0.5*(zRy+zRx))
        eps = max(sqrt(abs(zRx/zRy)),sqrt(abs(zRy/zRx)))
        M2 = np.sqrt(abs(M2x)*abs(M2y))
        M2Err = np.sqrt(abs(M2xErr)+abs(M2yErr))

        self.fitResults['M2'] = copy.deepcopy([w0x,w0xErr,w0y,w0yErr,z0x,z0xErr,z0y,z0yErr,eps,M2,M2Err,ast])
        self.figMFDfit.text(0.5, 0.55, '$w_{0,x}=%.1f (%.0f)  \,\mu m$ \n$w_{0,y}=%.1f (%.0f)  \,\mu m$\n$z_{0,x}=%.1f (%.0f) \,mm$ \n$z_{0,y}=%.1f (%.0f) \,mm$\nAst.: $a=%.3e$\nEllipticity: $\epsilon=%.3e$\n$M^2=%.3e (%.0f)$ \n$\chi^2_x=%.4e$\n$\chi^2_y=%.4e$' %(w0x,10*w0xErr,w0y,10*w0yErr,z0x,10*z0xErr,z0y,10*z0yErr,ast,eps,M2,M2Err,chi_sq_x,chi_sq_y),fontsize=14)

        plt.xlim([min(zList)-5, max(zList)+5])
        if self.calcMoment:
            plt.savefig(self.directory +os.path.sep+ 'MFD fit from 2nd Moments.pdf')
            plt.savefig(self.directory +os.path.sep+ 'MFD fit from 2nd Moments.png', dpi=200)
        else:
            plt.savefig(self.directory +os.path.sep+ 'MFD fit from 2D Gauss fit.pdf')
            plt.savefig(self.directory +os.path.sep+ 'MFD fit from 2D Gauss fit.png', dpi=200)
        plt.close()
        oldSize = self.master.view.canvas.figure.get_size_inches()
        self.figMFDfit.set_figheight(oldSize[1])
        self.figMFDfit.set_figwidth(oldSize[0])
        self.master.view.canvas.figure = self.figMFDfit
        self.master.view.canvas.draw()
        
        self.ratioFig = plt.figure()
        ratiosX = []
        ratiosY = []
        for i in range(len(wzXlist)):
            ratiosX.append(wzXlist[i]/wzYlist[i])
            ratiosY.append(wzYlist[i]/wzXlist[i])
        plt.plot(zList,ratiosX, marker="o", linestyle='None',markersize=2,color='b')
        plt.plot(zList,ratiosY, marker="o", linestyle='None',markersize=2,color='r')
        plt.plot(xvals,wzXfitcopy/wzYfit(xvals),color='b')
        plt.plot(xvals,wzYfit(xvals)/wzXfitcopy,color='r')
        plt.minorticks_on()
        plt.xlabel('$z$ [mm]')
        plt.ylabel('Ratio $ellipticity$')
        if self.calcMoment:
             plt.savefig(self.directory + os.path.sep+ 'MFD fit from Moments fit ratios.pdf')
             plt.savefig(self.directory + os.path.sep+ 'MFD fit from Moments fit ratios.png',dpi=200)
        else:
            plt.savefig(self.directory + os.path.sep+ 'MFD fit from Gaussian 2D fit ratios.pdf')
            plt.savefig(self.directory + os.path.sep+ 'MFD fit from Gaussian 2D fit ratios.png',dpi=200)
        plt.close()


        self.figResFit = plt.figure()
        plt.figure(self.figResFit.number)
        plt.clf()
        fitResX = np.polyfit(zList, wzXfit(zList)-wzXlist, 4)
        fitResY = np.polyfit(zList, wzYfit(zList)-wzYlist, 4)
        plt.plot(xvals,np.polyval(fitResX,xvals),color='b')
        plt.plot(zList,(wzXfit(zList)-wzXlist), marker="o", linestyle='None',markersize=2,color='b')
        plt.plot(xvals,np.polyval(fitResY,xvals),color='r')
        plt.plot(zList,(wzYfit(zList)-wzYlist), marker="o", linestyle='None',markersize=2,color='r')
        plt.minorticks_on()
        plt.legend(['$w(z)_x$', '$w(z)_y$'], bbox_to_anchor=(0.05, 0.98), loc=2, borderaxespad=0.) #,'fit x', 'fit y'
        plt.xlabel('$z$ [mm]')
        plt.ylabel('Fitted width $w(z)=w_0 \sqrt{1+((z-z_0)/z_R)^2}$')
        if self.calcMoment:
             plt.savefig(self.directory + os.path.sep+ 'MFD fit residuals from Moments.pdf')
             plt.savefig(self.directory + os.path.sep+ 'MFD fit residuals from Moments.png',dpi=200)
        else:
            plt.savefig(self.directory + os.path.sep+ 'MFD fit residuals from Gaussian 2D fit.pdf')
            plt.savefig(self.directory + os.path.sep+ 'MFD fit residuals from Gaussian 2D fit.png',dpi=200)
        plt.close()

    def saveData(self):

        rotAngleList = self.fitResults['rotAngleList']
        zList = self.fitResults['zList']
        if len(zList) == 0:
            return
        np.savetxt(self.directory+'rotAnglelist.txt', np.c_[zList,rotAngleList])
        fitRes = []
        for key, fitData in self.fitResults.items():
            try:
                float(key)
                fitRes.append(fitData)
            except:
                continue

        fileHeader = 'Filename, Amplitude (arb. u.), x0 (mm), y0 (mm), w0x (mm), w0y (mm), theta (deg), y0'
        np.savetxt(self.directory +'2D Gaussian Fit value.csv',np.hstack((np.reshape(zList, (len(zList), -1)), fitRes)), delimiter=',', header=fileHeader, fmt='%s')
        if 'M2' in self.fitResults:
            fileHeader = 'w0x,w0xErr,w0y,w0yErr,z0x,z0xErr,z0y,z0yErr,eps,M2,M2Err,ast'
            np.savetxt(self.directory +'M2Result.csv',((self.fitResults['M2'])), delimiter=',', header=fileHeader, fmt='%s')

    def analyseFit(self,im,pixelSize,stagePos,directory,rotationAngle = 0):
        #create an 8-bit version of the image for creating the precut images, CV2 cannot use 8+ bit images there
        im8 = im.astype(np.uint8)
        self.directory = directory
        self.pixelSize = pixelSize
        self.rotationAngle = rotationAngle

        self.mainTitle = os.path.basename(os.path.dirname(os.path.dirname(self.directory)))
        #NEVER EVER USE ":" in the filename, it will fail without a warning
        #final_x,final_y = 0,0
        title = self.mainTitle+'at Pos:'+str(stagePos)+' mm'

        #precut of the image to improve runtime
        (xx, yy), (best_ma, beam_width), best_angle = self.find_ellipses(im8)
        beam_width,xx,yy = int(beam_width*4),int(xx),int(yy)

        rotAngleList = self.fitResults['rotAngleList']
        zList = self.fitResults['zList']
        fitResult = 0
        x1lim = xx-beam_width
        x2lim = xx+beam_width
        y1lim = yy-beam_width
        y2lim = yy+beam_width
        if x1lim<0:
            x1lim=0
        if y1lim<0:
            y1lim=0
        if x2lim > np.size(im8,1)-1:
                x2lim=np.size(im8,1)-1
        if y2lim > np.size(im8,0)-1:
                y2lim=np.size(im8,0)-1
        momim = np.array(im,copy=True)
        im = self.cutImage(im,xx,yy,beam_width,beam_width)
        final_x,final_y = xx,yy
        #save the precut image just in case to check if there was any problem afterwards
        if not os.path.exists(directory+os.path.sep+'Precut'+os.path.sep):
                    os.makedirs(directory+os.path.sep+'Precut'+os.path.sep)
        cv2.imwrite(directory+os.path.sep+'Precut'+os.path.sep+str(stagePos)+"mm"+".tif", im)
        #calcuate the beamsize with either D4sigma or 2D gaussian fit

        if self.calcMoment:
            if self.rotationFixed: 
                fitResult = self.calcmoments(im,(y1lim,y2lim,x1lim,x2lim), momim, fixedAngle = self.rotationAngle)
            else:
                fitResult = self.calcmoments(im,(y1lim,y2lim,x1lim,x2lim),momim)
            if fitResult[0] == False:
                raise Exception('Fit moments failed')
        else:
            xx,yy,width = self.reduceImageSizeToWidth(copy.deepcopy(im))
            width = width*3
            im = self.cutImage(im,xx,yy,width,width)
            final_x += xx-width/2
            final_y += yy-width/2
            if not os.path.exists(directory+os.path.sep+'GaussianReduced'+os.path.sep):
                    os.makedirs(directory+os.path.sep+'GaussianReduced'+os.path.sep)
            cv2.imwrite(directory+os.path.sep+"GaussianReduced"+os.path.sep+str(stagePos)+"mm"+".tif", im)
            if self.rotationFixed: fitResult = self.fitgaussianRotFixed(copy.deepcopy(im))
            else: fitResult = self.fitgaussian(copy.deepcopy(im))

            cp = fitResult[3]
            fitResult[3] = fitResult[4]
            fitResult[4] = cp

        x = fitResult[1]
        y = fitResult[2]
        width_x = np.abs(fitResult[3])
        width_y = np.abs(fitResult[4])
        print((x,y,width_x,width_y))
        res = self.cutImage(im,x,y,width_x/2,width_y/2)
        if not os.path.exists(directory+os.path.sep+'fitImage'+os.path.sep):
                    os.makedirs(directory+os.path.sep+'fitImage'+os.path.sep)
        cv2.imwrite(directory+os.path.sep+'fitImage'+os.path.sep+str(stagePos)+"mm"+".tif", res)
        fitResultPixels = fitResult.copy()
        if self.shouldFitRes:
            if self.rotationFixed: fitRes = self.gaussianRotFixed(*fitResultPixels)
            else: fitRes = self.gaussian(*fitResultPixels)
        p = fitResultPixels
        x = np.arange(0,im.shape[0])
        y = np.arange(0,im.shape[1])

        A=p[0]
        x0=p[1]
        y0=p[2]
        w1=p[3]
        w2=p[4]

        if self.rotationFixed:
            alpha=self.rotationAngle

        else:
            signalpha=np.sign(p[5])
            alpha=p[5]

            '''
            w1cp = copy.deepcopy(p[3])
            p[3] = p[3]*cos(np.deg2rad(alpha))-p[4]*sin(np.deg2rad(alpha))
            p[4] = w1cp*sin(np.deg2rad(alpha))+p[4]*cos(np.deg2rad(alpha))
            p[5] = alpha
            '''
            '''
            if signalpha < 0:
                w1cp = copy.deepcopy(p[3])
                p[3] = copy.deepcopy(p[4])
                p[4] = w1cp
            '''

            print('Rot. angle: %.3f'%alpha)

        rotAngleList.append(alpha)
        z = im

        plt.close()
        cutFig=plt.figure()

        plt.subplot(311)
        plt.title(title + '\n X and Y 1D cuts', fontsize=14)
        if self.gaussMajCut or self.gaussMajCut:
            inter2d = scipy.interpolate.RectBivariateSpline(x,y,z)
            xy = np.meshgrid(y,x)
            z2 = inter2d.ev(xy[1],xy[0])
            # Should be the same as z
            # Major and minor 1D cuts'
            plt.imshow(z2)
        else:
            z2 = z
            plt.imshow(z)

        ## Major cut
        m=tan(alpha/360.0*2*pi)
        c=x0-m*y0
        plt.plot(x,m*x+c,color='y')

        plt.xlim([0,im.shape[1]])
        plt.ylim([0,im.shape[0]])

        if self.gaussMajCut:

            plt.subplot(312)
            xvals = abs(np.cos(alpha/360.0*2*pi)*np.arange(0,abs(int(im.shape[0]/np.cos(alpha/360.0*2*pi)))))
            yvals=m*xvals+c
            if m>1: mask=(yvals>0) & (yvals<im.shape[0])
            else: mask=(yvals>0) & (yvals<im.shape[1])
            cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*pi))
            cutXvals=cutXvals-cutXvals[0]
            cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])
            plt.plot(cutXvals,cutYvals)
            plt.minorticks_on()
            popt, pcov = scipy.optimize.curve_fit(self.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w2,0))
            peak1=popt[1]+popt[3]
            ax = cutFig.add_subplot(312)
            w1cut=popt[2]*self.pixelSize
            print("Width 1: %.5f mm"%w1cut)

            plt.text(0.05, 0.85, 'Width: %.3f pixels \n = %.3f mm' %(popt[2],w1cut), transform=ax.transAxes, fontsize = 16)
            plt.plot(cutXvals,self.gauss1d(cutXvals,*popt))
            plt.xlim([0,max(cutXvals)])
            plt.ylim([-0.05*max(cutYvals),1.05*max(cutYvals)])

            #1D fit for Ratio
            xmin=popt[0]-0.5*popt[2]
            xmax=popt[0]+0.5*popt[2]
            if xmin>xmax:
                cp=xmin
                xmin=xmax
                xmax=cp
            popt, pcov = scipy.optimize.curve_fit(self.gauss1d, cutXvals[int(xmin):int(xmax)], cutYvals[int(xmin):int(xmax)],(popt[0], popt[1],popt[2],popt[3]))
            print(abs(popt[0]*im.shape[0]/max(cutXvals)/np.cos(alpha/360.0*2*pi)))
            perr = np.sqrt(np.diag(pcov))
            gauss1dfitRat = lambda x: self.gauss1d(x, popt[0], popt[1],popt[2],popt[3])
            plt.plot(x,gauss1dfitRat(x),'c', linewidth=2.0)
            plt.minorticks_on()
            peak2=popt[1]+popt[3]
            plt.plot((xmin,xmin), (0, peak1), 'g-',linestyle='--')
            plt.plot((xmax,xmax), (0, peak1), 'g-',linestyle='--')
            plt.minorticks_on()
            ratio1cut=peak2/peak1
            print("Ratio 1: %.7f"%ratio1cut)
            plt.text(0.62, 0.93, 'Peak ratio: %.3f' %ratio1cut, transform=ax.transAxes, fontsize=14)

        ## Minor cut
        alpha=alpha+90
        m=tan(alpha/360.0*2*pi)
        c=x0-m*y0
        plt.subplot(311)
        plt.plot(x,m*x+c,color='y')
        plt.xlim([0,im.shape[1]])
        plt.ylim([0,im.shape[0]])
        if self.gaussMinCut:
            ## Minor cut
            plt.subplot(313)
            xvals = abs(np.cos(alpha/360.0*2*pi)*np.arange(0,abs(int(im.shape[1]/np.cos(alpha/360.0*2*pi)))))
            yvals=m*xvals+c
            if m>1: mask=(yvals>0) & (yvals<im.shape[0])
            else: mask=(yvals>0) & (yvals<im.shape[1])
            cutXvals=abs(xvals[mask]/np.cos(alpha/360.0*2*pi))
            cutXvals=cutXvals-cutXvals[0]
            cutYvals=inter2d.ev(m*xvals[mask]+c,xvals[mask])
            plt.plot(cutXvals,cutYvals)
            plt.minorticks_on()
            popt, pcov = scipy.optimize.curve_fit(self.gauss1d, cutXvals, cutYvals,(max(cutXvals)/2,A,w1,0))
            peak1=popt[1]+popt[3]
            ax = cutFig.add_subplot(313)
            w2cut=popt[2]*self.pixelSize
            print("Width 2: %.5f mm"%w2cut)
            plt.text(0.05, 0.85, 'Width: %.3f pixels \n = %.3f mm' %(popt[2],w2cut), transform=ax.transAxes, fontsize=14)
            plt.plot(cutXvals,self.gauss1d(cutXvals,*popt))
            plt.minorticks_on()
            plt.xlim([0,max(cutXvals)])
            plt.ylim([-0.05*max(cutYvals),1.05*max(cutYvals)])
        #1D fit for Ratio
            xmin=popt[0]-0.5*popt[2]
            xmax=popt[0]+0.5*popt[2]
            if xmin>xmax:
                cp=xmin
                xmin=xmax
                xmax=cp
            popt, pcov = scipy.optimize.curve_fit(self.gauss1d, cutXvals[int(xmin):int(xmax)], cutYvals[int(xmin):int(xmax)],(popt[0], popt[1],popt[2],popt[3])) #abs(popt[0]*im.shape[0]/max(cutXvals)/np.cos(alpha/360.0*2*pi))
            print(abs(popt[0]*im.shape[0]/max(cutXvals)/np.cos(alpha/360.0*2*pi)))
            perr = np.sqrt(np.diag(pcov))
            gauss1dfitRat = lambda x: self.gauss1d(x, popt[0], popt[1],popt[2],popt[3])
            plt.plot(x,gauss1dfitRat(x),'c', linewidth=2.0)
            peak2=popt[1]+popt[3]
            plt.plot((xmin,xmin), (0, peak1), 'g-',linestyle='--')
            plt.plot((xmax,xmax), (0, peak1), 'g-',linestyle='--')
            plt.minorticks_on()
            ratio2cut=peak2/peak1
            print("Ratio 2: %.7f"%ratio2cut)
            plt.text(0.62, 0.93, 'Peak ratio: %.3f' %ratio2cut, transform=ax.transAxes, fontsize=14)

        cutFig.set_figheight(13)
        cutFig.set_figwidth(6)
        # ppCut.savefig(cutFig)
        if not os.path.exists(directory+os.path.sep+'Plot'+os.path.sep):
            os.makedirs(directory+os.path.sep+'Plot'+os.path.sep)
        cutFig.savefig(directory+os.path.sep+'Plot'+os.path.sep+title.replace(":","")+'fit.pdf')
        #cutFig.show()
        plt.close()
        if self.shouldFitRes:
            # Plot residuals
            figRes = plt.figure()
            #plt.clf()
            res = fitRes(*np.indices(im.shape))-im
            plt.imshow(res, extent=[0,self.pixelSize*im.shape[1],0,self.pixelSize*im.shape[0]])
            plt.xlabel('x (mm)')
            plt.ylabel('y (mm)')
            plt.colorbar()
            plt.title(title + '\nFit residuals', fontsize=14)
            plt.tight_layout(pad=1, w_pad=2, h_pad=2)
            if not os.path.exists(directory+os.path.sep+'ResidualPlot'+os.path.sep):
                os.makedirs(directory+os.path.sep+'ResidualPlot'+os.path.sep)
            plt.savefig(directory + os.path.sep+'ResidualPlot'+os.path.sep+title.replace(":","")+ 'fit residual.pdf')
            plt.close()

        zList.append(stagePos)
        final_x += (p[1]-p[3]/2)
        final_y += (p[2]-p[4]/2)
        p[1] = final_x
        p[2] = final_y

        self.fitResults[str(stagePos)] = p*self.pixelSize
        print('done analyzing image at stage position',stagePos)

    def beam_size(self, image, fixedAngle = None):
        """
        Determines laser beam center, diameters, and tilt of beam according to ISO 11146 standard.

        Parameters
        ----------
        image : array_like
            should be a monochrome two-dimensional array

        Returns
        -------
        xc : int
            horizontal center of beam
        yc : int
            vertical center of beam
        dx : float
            horizontal diameter of beam
        dy : float
            vertical diameter of beam
        phi: float
            angle that beam is rotated (about center) from the horizontal axis
        """


        #vertical and horizontal dimensions of beam
        v,h = image.shape
        # total of all pixels
        p  = np.sum(image,dtype=np.float)     # float avoids integer overflow

        # find the centroid
        hh = np.arange(h,dtype=np.float)      # float avoids integer overflow
        vv = np.arange(v,dtype=np.float)      # ditto
        xc = np.sum(np.dot(image,hh))/p
        yc = np.sum(np.dot(image.T,vv))/p

        # find the variances
        hs = hh-xc
        vs = vv-yc
        xx = np.sum(np.dot(image,hs**2))/p
        xy = np.dot(np.dot(image.T,vs),hs)/p
        yy = np.sum(np.dot(image.T,vs**2))/p

        # the ISO measures
        diff = xx-yy
        summ = xx+yy
        if self.rotationFixed and not fixedAngle is None:
            rotAng = np.deg2rad(fixedAngle)
            xy = tan(2*rotAng)*diff/2
            print(xy)
            disc = diff
        else:

            if diff :
                disc = np.sign(diff)*np.sqrt(diff**2 + 4*xy**2)
            else :
                disc = np.abs(xy)

        dx = 2.0*np.sqrt(2)*np.sqrt(summ+disc)
        dy = 2.0*np.sqrt(2)*np.sqrt(summ-disc)

        phi = -0.5 * np.arctan2(2*xy,diff) # negative because top of matrix is zero
        return xc, yc, dx, dy, phi