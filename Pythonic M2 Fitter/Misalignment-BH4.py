# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 14:34:45 2018

@author: Vit
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import copy
from scipy.odr import *
import math
name = "2018-06-22 M2 Measurement BH4 Collimator\Align"
#name= "2018-07-02 750mmLens"+os.path.sep+"49-74 Modified"
#name = "2018-07-02 750mmLens"
#name = "2018-06-29 LensMisalign"
#name = "2018-06-29-LensAngle"

Dir = r"C:\Users\Balmer\HLab-M2"+os.path.sep+name
allFolders = os.listdir(Dir)
#Files 20180608-1044-BH4LensCol1-750mmLens-3880CPCam-Align04 -- 20180608-1115-BH4LensCol1-750mmLens-3880CPCam-Align25
#XalignVals=[+0.5,0,-0.5,-0.5,-1,-1.5,-0.5,-0.25,-0.75,-0.625,-0.5,-0.5,-0.5,-0.375,-0.375,-0.375,-0.375,-0.25,-0.125,-0.125,-0.5,-0.5]
#AstVals=[-0.230,-0.079,-0.039,-0.054,-0.087,-0.245,-0.060,-0.034,-0.066,-0.057,-0.077,-0.056,-0.065,-0.060,-0.051,-0.051,-0.054,-0.052,-0.028,-0.048,-0.037,-0.032]
#fitTypes = ["fit","fitMom","fit1zR","fitMom1zR"]
fitTypes = ["fit"]
def quad_func(p, x):
    a,b,c = p
    return a*x**2 +b*x+c 
for fitType in fitTypes:
    AstVals = []
    xVals = []
    XalignVals = []
    YalignVals = []
    angAlignVals = []
    plt.clf()
    for folder in allFolders:
        try:
          ast = float(np.genfromtxt(Dir+os.path.sep+folder+os.path.sep+fitType+os.path.sep+"M2Result.csv",skip_header=1)[-1])
          Align = np.genfromtxt(Dir+os.path.sep+folder+os.path.sep+"comment.txt",delimiter = ",",dtype=None,encoding=None)
        except Exception as exp:
          print(exp)
          continue
        X= float((Align[0]).split("=")[1])
        Y= float((Align[1]).split("=")[1])
        #ang = float((Align[()].split("="))[1])
        if abs(ast) > 0.5:
            print("Possible outlier at "+folder+" with Astigmatism"+ str(abs(ast)))
            continue
        print(X)
        if Y == 0.7:
            print("Exclude outlier")
            continue
        X = X*0.75
        Y = Y*0.75
        XalignVals.append(X)
        YalignVals.append(Y)
        AstVals.append(ast)
        #angAlignVals.append(ang)
    YalignVals = np.array(YalignVals)
    AstVals = np.array(AstVals)
    XalignVals = np.array(XalignVals)
    angAlignVals = np.array(angAlignVals)
    #x_err= 0.125
    
    x_err= 0.015625
    y_err= 0.005
    
    uniqueYalignVals, indices = np.unique(YalignVals,return_inverse=True)
    #print(uniqueYalignVals)
    #print(indices)
    for yy in uniqueYalignVals:
        plt.clf()
        XalignValsUse=np.ma.masked_where(indices!=(list(uniqueYalignVals).index(yy)),XalignVals)
        XalignValsUse = XalignValsUse.compressed()
        #AstValsUse=AstVals[indices[np.where(uniqueYalignVals==y)]]
        if len(np.unique(XalignValsUse)) <= 2:
            continue
        AstValsUse=np.ma.masked_where(indices!=(list(uniqueYalignVals).index(yy)),AstVals)
       # print(AstValsUse)
        AstValsUse = AstValsUse.compressed()
        
        compressedXalign, indi = np.unique(XalignValsUse,return_inverse=True)
        AstValsUseTemp = []
        XerrUse = []
        YerrUse = []
        #print(compressedXalign)
        for xxx in compressedXalign:
          compressedTemp=np.ma.masked_where(indi!=(list(compressedXalign).index(xxx)),AstValsUse)
          compressedTemp.compressed()
          XerrUse.append(x_err)
          YerrUse.append(y_err)
          AstValsUseTemp.append(compressedTemp.mean())
        AstValsUse=np.array(AstValsUseTemp)
        XalignValsUse =compressedXalign
        
        
        fit=np.poly1d(np.polyfit(XalignValsUse,AstValsUse, 2,w=1/np.array(YerrUse)))
        print(fit)
       
        xvals=np.arange(min(XalignValsUse)-0.25*abs(min(XalignValsUse)),max(XalignValsUse)+0.25*abs(max(XalignValsUse)),0.01)
        
        plt.xlim(xvals.min(),xvals.max())
        plt.errorbar(XalignValsUse,AstValsUse,marker='',linestyle='None',xerr=XerrUse,yerr=YerrUse,color='black',capsize=3,barsabove=True,label='Messwerte')
        plt.plot(xvals,fit(xvals),color='red',label='Fit')
        #plt.xlabel('X alignment in mm')
        #plt.ylabel('Astigmatism')
        
        plt.xlabel('X Position (mm)')
        plt.ylabel('Astigmatismus')
        plt.minorticks_on()
        plt.legend()
        dof = len(AstValsUse) - 3
        red_chi_sq = np.sum(((AstValsUse-fit(XalignValsUse))/YerrUse)**2)/dof
        print(red_chi_sq)
        #plt.title('Aligning in X, Y='+str(yy)+" \n , Red. ChiSq: "+str(red_chi_sq)) #\n Files 20180608-1044-BH4LensCol1-750mmLens-3880CPCam-Align04 \n -- 20180608-1115-BH4LensCol1-750mmLens-3880CPCam-Align25',fontsize=9)
        plt.tight_layout()
        #plt.savefig(Dir+os.path.sep+fitType+"-"+'Align in X='+', Y='+str(yy)+'chisq'+str(red_chi_sq)+'.pdf')
        plt.savefig(Dir+os.path.sep+fitType+"-"+'Align in X, Y='+str(yy)+'chisq'+str(red_chi_sq)+'.svg')
        plt.savefig(Dir+os.path.sep+fitType+"-"+'Align in X, Y='+str(yy)+'chisq'+str(red_chi_sq)+'.png',dpi=300)
        
        
        
        
    uniqueXalignVals, indices = np.unique(XalignVals,return_inverse=True)    
    for xx in uniqueXalignVals:
        plt.clf()
        YalignValsUse=np.ma.masked_where(indices!=(list(uniqueXalignVals).index(xx)),YalignVals)
        YalignValsUse = YalignValsUse.compressed()
        if len(np.unique(YalignValsUse)) <= 3:
            continue
        #AstValsUse=AstVals[indices[np.where(uniqueYalignVals==y)]]
        AstValsUse=np.ma.masked_where(indices!=(list(uniqueXalignVals).index(xx)),AstVals)
       # print(AstValsUse)
        AstValsUse = AstValsUse.compressed()
        x_err= 0.015625
        y_err= 0.005
        compressedYalign, indi = np.unique(YalignValsUse,return_inverse=True)
        AstValsUseTemp = []
        XerrUse = []
        YerrUse = []
        #print(compressedXalign)
        for yyy in compressedYalign:
          compressedTemp=np.ma.masked_where(indi!=(list(compressedYalign).index(yyy)),AstValsUse)
          compressedTemp.compressed()
          XerrUse.append(x_err)
          YerrUse.append(y_err)
          AstValsUseTemp.append(compressedTemp.mean())
        AstValsUse=np.array(AstValsUseTemp)
        YalignValsUse =compressedYalign
        
        
        fit=np.poly1d(np.polyfit(YalignValsUse,AstValsUse, 2,w=1/np.array(YerrUse)))
        print(fit)
       
        xvals=np.arange(min(YalignValsUse)-0.25*abs(min(YalignValsUse)),max(YalignValsUse)+0.25*abs(max(YalignValsUse)),0.01)
        
        plt.xlim(xvals.min(),xvals.max())
        plt.errorbar(YalignValsUse,AstValsUse,marker='',linestyle='None',xerr=XerrUse,yerr=YerrUse,color='black',capsize=3,barsabove=True,label='Messwerte')
        plt.plot(xvals,fit(xvals),color='red',label='Fit')
        #plt.xlim(x_fit.min(),x_fit.max())
        #plt.xlabel('X alignment in mm')
        #plt.ylabel('Astigmatism')
        
        plt.xlabel('Y Position (mm)')
        plt.ylabel('Astigmatismus')
        plt.minorticks_on()
        
        dof = len(AstValsUse) - 3
        red_chi_sq = math.sqrt(np.sum((AstValsUse-fit(YalignValsUse))/YerrUse)**2)/dof
        print('reduced',red_chi_sq)
        plt.legend()
        #plt.title('Aligning in X, Y='+str(yy)+" \n , Red. ChiSq: "+str(red_chi_sq)) #\n Files 20180608-1044-BH4LensCol1-750mmLens-3880CPCam-Align04 \n -- 20180608-1115-BH4LensCol1-750mmLens-3880CPCam-Align25',fontsize=9)
        plt.tight_layout()
        #plt.savefig(Dir+os.path.sep+fitType+"-"+'Align in X='+', Y='+str(yy)+'chisq'+str(red_chi_sq)+'.pdf')
        plt.savefig(Dir+os.path.sep+fitType+"-"+'Align in Y, X='+str(xx)+'chisq'+str(red_chi_sq)+'.svg')
        plt.savefig(Dir+os.path.sep+fitType+"-"+'Align in Y, X='+str(xx)+'chisq'+str(red_chi_sq)+'.png',dpi=300)
    '''
    compressedAngalign, indi = np.unique(angAlignVals,return_inverse=True)
    AstValsUseTemp = []
    XerrUse = []
    YerrUse = []
    for yyy in compressedAngalign:
          compressedTemp=np.ma.masked_where(indi!=(list(compressedAngalign).index(yyy)),AstVals)
          compressedTemp.compressed()
          XerrUse.append(x_err)
          YerrUse.append(y_err)
          AstValsUseTemp.append(compressedTemp.mean())
    AstVals=np.array(AstValsUseTemp)
    angAlignVals =compressedAngalign
    plt.clf()
    fit=np.poly1d(np.polyfit(angAlignVals,AstVals, 2))
    
    print(fit)
    xvals=np.arange(min(angAlignVals)-0.1*abs(min(angAlignVals)),max(angAlignVals)+0.1*abs(max(angAlignVals)),0.01)
    plt.xlim(xvals.min(),xvals.max())
    #plt.plot(xvals,fit(xvals),color='orange', label='fit')
    plt.errorbar(angAlignVals,AstVals,marker='o',linestyle='None',xerr=x_err,yerr=y_err,color='black',capsize=3)
    plt.xlabel('Winkel&Translation justage (Drehungen, + im Uhrzeigersinn)')
    plt.ylabel('Astigmatismus')
    plt.legend()
    plt.minorticks_on()
   # plt.title('Lens misalignment by Translation+Angle on New Focus Rotation stage='+str(ang)) #\n Files 20180608-1044-BH4LensCol1-750mmLens-3880CPCam-Align04 \n -- 20180608-1115-BH4LensCol1-750mmLens-3880CPCam-Align25',fontsize=9)
    plt.tight_layout()
    plt.savefig(Dir+os.path.sep+fitType+"-"+'Misalign in Ang='+str(ang)+'.pdf')
    plt.savefig(Dir+os.path.sep+fitType+"-"+'Misalign in Ang='+str(ang)+'.svg')
    plt.savefig(Dir+os.path.sep+fitType+"-"+'Misalign in Ang='+str(ang)+'.png',dpi=300)
    '''
