# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 14:34:45 2018

@author: Vit
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import copy
from scipy.odr import *
from scipy.stats import *
import math
name = "Simulations\Astigmatic&Elliptic Gauss Noise"

Dir = r"C:\Users\Balmer\HLab-M2"+os.path.sep+name
allFolders = os.listdir(Dir)
#Files 20180608-1044-BH4LensCol1-750mmLens-3880CPCam-Align04 -- 20180608-1115-BH4LensCol1-750mmLens-3880CPCam-Align25
#XalignVals=[+0.5,0,-0.5,-0.5,-1,-1.5,-0.5,-0.25,-0.75,-0.625,-0.5,-0.5,-0.5,-0.375,-0.375,-0.375,-0.375,-0.25,-0.125,-0.125,-0.5,-0.5]
#AstVals=[-0.230,-0.079,-0.039,-0.054,-0.087,-0.245,-0.060,-0.034,-0.066,-0.057,-0.077,-0.056,-0.065,-0.060,-0.051,-0.051,-0.054,-0.052,-0.028,-0.048,-0.037,-0.032]
#fitTypes = ["fit","fitMom","fit1zR","fitMom1zR"]
theorNoise= -0.4011
fitTypes = ["fit"]
for fitType in fitTypes:
    AstVals = []
    xVals = []
    XalignVals = []
    YalignVals = []
    noiseVals = []
    plt.clf()
    for folder in allFolders:
        try:
          ast = float(np.genfromtxt(Dir+os.path.sep+folder+os.path.sep+fitType+os.path.sep+"M2Result.csv",skip_header=1)[-1])
        except Exception as exp:
          print(exp)
          continue
        noise = float(folder)*100
        if noise == 10:
          continue
        AstVals.append(ast)
        noiseVals.append(noise)
    noiseVals = np.array(noiseVals)
    AstVals = np.array(AstVals)

    plt.clf()
    plt.xscale('log')
    #fit=np.poly1d(np.polyfit(angAlignVals,AstVals, 0))
    #print(fit)
    xvals=np.arange(min(noiseVals)-0.1*abs(min(noiseVals)),max(noiseVals)+0.1*abs(max(noiseVals)),0.01)
    plt.hlines(xmin= min(noiseVals)-0.1*abs(min(noiseVals)),xmax = max(noiseVals)+0.1*abs(max(noiseVals)),
            y= theorNoise,
            linewidth=1, label = "Theorie",linestyle='--', color ='black'
            )
    plt.xlim(xvals.min(),xvals.max())
    plt.plot(noiseVals,AstVals,marker='x',linestyle='None',label='Simulation & D4$\sigma$')
    plt.xlabel('Rauschniveau in %')
    plt.ylabel('Astigmatismus')
    plt.legend()
    plt.minorticks_on()
   # plt.title('Lens misalignment by Translation+Angle on New Focus Rotation stage='+str(ang)) #\n Files 20180608-1044-BH4LensCol1-750mmLens-3880CPCam-Align04 \n -- 20180608-1115-BH4LensCol1-750mmLens-3880CPCam-Align25',fontsize=9)
    plt.tight_layout()
    plt.savefig(Dir+os.path.sep+fitType+"-"+'Noise'+'.pdf')
    #plt.savefig(Dir+os.path.sep+fitType+"-"+'Misalign in Ang='+str(ang)+'.svg')
    #plt.savefig(Dir+os.path.sep+fitType+"-"+'Misalign in Ang='+str(ang)+'.png',dpi=300)
